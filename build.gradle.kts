import java.text.SimpleDateFormat
import java.util.Date
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

object Globals {
    object Versions {
        const val main = "0.99"
        const val xstream = "1.4.16"
        const val apacheCommons = "2.0.1"
        const val mvel = "2.4.12.Final"
        const val objenesis = "2.1"
        const val flatlaf = "1.0"
		const val kotest = "4.6.1"
		const val slf4j = "1.7.31"
		const val logback = "1.2.3"
		const val kotlinLogging = "1.12.5"
		const val jackson = "2.12.4"
    }

    object Config {
        const val group = "fetishmaster"
        const val mainClasspath = "$group.FetishMaster"
        val jvmArgs = listOf(
                "--illegal-access=permit"
        )
    }
}

val isWindows = System.getProperty("os.name").toLowerCase().startsWith("windows")
val dateStamp = SimpleDateFormat("yyyymmdd").format(Date())

plugins {
    application
	jacoco
    id("org.beryx.runtime") version "1.12.4"
    id("com.github.johnrengelman.shadow") version "7.0.0"
	id("org.sonarqube") version "3.3"
	kotlin("jvm") version "1.5.21"
}

group = Globals.Config.group
version = Globals.Versions.main

repositories {
    // Use Maven Central for resolving dependencies.
    mavenCentral()
}

dependencies {
    implementation(files("libraries/AbsoluteLayout.jar"))
    implementation("com.thoughtworks.xstream", "xstream", Globals.Versions.xstream)
    implementation("commons-io", "commons-io", Globals.Versions.apacheCommons)
    implementation("org.mvel", "mvel2", Globals.Versions.mvel)
    implementation("org.objenesis", "objenesis", Globals.Versions.objenesis)
	implementation("org.slf4j", "slf4j-api", Globals.Versions.slf4j)
	implementation("ch.qos.logback", "logback-classic", Globals.Versions.logback)

	implementation("com.fasterxml.jackson.core", "jackson-core", Globals.Versions.jackson)
	implementation("com.fasterxml.jackson.core", "jackson-annotations", Globals.Versions.jackson)
	implementation("com.fasterxml.jackson.core", "jackson-databind", Globals.Versions.jackson)
	implementation("com.fasterxml.jackson.module", "jackson-module-kotlin", Globals.Versions.jackson)
	implementation("com.fasterxml.jackson.dataformat", "jackson-dataformat-xml", Globals.Versions.jackson)

    implementation("com.formdev", "flatlaf", Globals.Versions.flatlaf)

	implementation(kotlin("stdlib-jdk8"))
	implementation(kotlin("reflect"))
	implementation(kotlin("script-runtime"))
	implementation("io.github.microutils", "kotlin-logging", Globals.Versions.kotlinLogging)

    // Use JUnit Jupiter API for testing.
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.7.1")
	testImplementation("io.kotest", "kotest-runner-junit5", Globals.Versions.kotest)
	testImplementation("io.kotest", "kotest-assertions-core", Globals.Versions.kotest)
	testImplementation("io.kotest", "kotest-property", Globals.Versions.kotest)

    // Use JUnit Jupiter Engine for testing.
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
}

java {
	toolchain {
		//16 until I can get rid of xstream
		languageVersion.set(JavaLanguageVersion.of(16))
	}
}

application {
    // Define the main class for the application.
    mainClass.set(Globals.Config.mainClasspath)
    applicationDefaultJvmArgs = Globals.Config.jvmArgs
}

//disable start script generation because custom ones are used
tasks.startScripts.get().enabled = false
//Disable baseline distribution because only shaded and runtime dists are used
tasks.distZip.get().enabled = false
tasks.installDist.get().enabled = false


//Disable tar builds
tasks.withType<Tar> {
	enabled = false
}

//Normalize jar output to have reproducable builds
tasks.withType<AbstractArchiveTask> {
	isPreserveFileTimestamps = false
	isReproducibleFileOrder = true
}

runtime {
    jpackage {
        if(isWindows) {
            imageOptions = listOf("--win-console")
        }
        skipInstaller = true
    }
}

//Insert jar names and jvm args into custom scripts
tasks.register<Sync>("preprocessScripts") {
	from(layout.projectDirectory.dir("libraries/scripts"))
	filter {
		it
				.replace("<JVM_OPTS>", Globals.Config.jvmArgs.joinToString(" "))
				.replace("<JARNAME>", tasks.shadowJar.get().archiveFileName.get())
	}
	into("${buildDir}/processedScripts")

}

//Make sure preprocessed scripts are crated before creating distributions
tasks.withType<Zip> {
	dependsOn("preprocessScripts")
}

tasks.named<Sync>("installShadowDist") {
	dependsOn("preprocessScripts")
  doLast {
    delete("$buildDir/install/neofm-shadow/bin")
  }
}

distributions {
    all {
        contents {
            from(layout.projectDirectory) {
                include("gamedata/**")
                include("LICENSE.md")
                exclude("gamedata/img/**")
            }
        }
    }

    named("shadow") {
        contents {
        	from(layout.buildDirectory.dir("processedScripts/combo"))
        }
    }

    create("runtimePackage") {
        contents {
            from(layout.buildDirectory.dir("jpackage/neofm"))

            val isWindows = System.getProperty("os.name").toLowerCase().startsWith("windows")
            if (isWindows) {
            	from(layout.buildDirectory.dir("processedScripts/windows"))
            } else {
				from(layout.buildDirectory.dir("processedScripts/linux"))
            }
        }
    }
}

//Remove "shadow" from archive base name
tasks.shadowDistZip {
	archiveBaseName.set(project.name)
	eachFile {
		if(path.contains("bin")) {
			exclude()
		}
	}
}

//Rename runtime package to be for platform
tasks["installRuntimePackageDist"].dependsOn(tasks.jpackageImage)
tasks.getByName<Zip>("runtimePackageDistZip") {
    archiveBaseName.set("${project.name}-runtime-${if (isWindows) "win" else "nix"}")
    dependsOn(tasks.jpackageImage)
}

tasks.register<Zip>("makeImagePack") {
    from(layout.projectDirectory.dir("gamedata/img"))
    archiveFileName.set("imgpack.zip")
    group = "distribution"
}

tasks.withType<JavaExec> {
	args = listOf("gamedata")
}

tasks.register<JavaExec>("runDevMode") {
    main = Globals.Config.mainClasspath
    group = "application"
    classpath = sourceSets.main.get().runtimeClasspath
    jvmArgs = Globals.Config.jvmArgs
    args = listOf("--devmode")
}

tasks.register<JavaExec>("runFullDevMode") {
    main = Globals.Config.mainClasspath
    group = "application"
    classpath = sourceSets.main.get().runtimeClasspath
    jvmArgs = Globals.Config.jvmArgs
    args = listOf("--devmode", "--fulledit")
}

tasks.test {
    // Use junit platform for unit tests.
    useJUnitPlatform()
}

sonarqube {
	properties {
		property("sonar.projectKey", "Plebian-Softworks_NeoFM")
		property("sonar.organization", "plebian-softworks")
		property("sonar.host.url", "https://sonarcloud.io")
		property("sonar.exclusions", "display/**/*")
	}
}
val compileKotlin: KotlinCompile by tasks
compileKotlin.kotlinOptions {
	freeCompilerArgs = listOf("-Xjvm-default=all")
	jvmTarget = "16"
}
val compileTestKotlin: KotlinCompile by tasks
compileTestKotlin.kotlinOptions {
	freeCompilerArgs = listOf("-Xjvm-default=all")
	jvmTarget = "16"
}

tasks.test {
	finalizedBy(tasks.jacocoTestReport)
}

tasks.jacocoTestReport {
	reports {
		xml.required.set(true)
	}
}
