# NeoFM
![GitHub Workflow Status](https://img.shields.io/github/workflow/status/Plebian-Softworks/NeoFM/Build?style=for-the-badge)
![Discord](https://img.shields.io/discord/845109181664985138?style=for-the-badge)

[![forthebadge](https://forthebadge.com/images/badges/ages-18.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/built-by-neckbeards.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/made-with-out-pants.svg)](https://forthebadge.com)

An updated, optimized, and cleaned fork of Fetish Master, originally forked from jimbobvii's fork.

### Goals of this fork:
- Preserve full mod compatibility with mods targeting 0.985e of the original game
- Clean the code and make it easier to contribute to
- Optimize the game and improve performance
- Improve the UI of the game
- Fix bugs  
- Fix grammar and spelling errors
- Provide new optional features for mod authors

# Contributing

Contributions are welcome!

Follow these simple rules:
- Keep pull requests as small as possible, changing as little and being as self contained as possible
- Ensure you comply with the goals of the fork

Pull requests are open and anything that meets code quality and these requirements will likely be merged.

# Building
Due to the usage of Gradle Wrapper, no preinstalled tooling besides java is needed.

If you don't have JDK 16, it can be gotten from AdoptOpenJDK from [this link](https://github.com/AdoptOpenJDK/openjdk16-binaries/releases/download/jdk-16.0.1%2B9/OpenJDK16U-jdk_x64_windows_hotspot_16.0.1_9.msi).

You can run the game or build the project via provided batch and shell scripts:
- build.bat/sh builds distributions and outputs to `build/distributions`
- start.bat/sh runs the game normally
- start_dev.bat/sh runs the game in developer mode

All scripts will run the needed build steps as needed, as well as bootstrap their own gradle and JDK install if needed.
They will  automatically detect changes upon start and will trigger rebuilds on the next run after source changes.

Gradle is well integrated into many IDEs. IntelliJ, NetBeans, and Eclipse all have robust Gradle plugins that should
be able to automatically detect all tasks and be able to run them as needed.
Tasks follow standard gradle convention, with some exceptions:
- `build` - Build the source and bundle distributions
- `run` - Run Fetish Faster
- `runDevMode` - Run Fetish Master in Developer Mode
- `check` - Run unit tests
