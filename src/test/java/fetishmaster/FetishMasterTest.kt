package fetishmaster

import java.nio.file.Paths

object FetishMasterTest {

    const val testFilesDir = "src/test/resources"
    const val testOutputDir = "$testFilesDir/testOutputDir/"

    fun clearTestOutput() {
        val testOutputDir = Paths.get(testOutputDir).toFile()
        val fileOutput = testOutputDir.listFiles() ?: return
        for (file in fileOutput) {
            if (file.name == ".githold") continue
            file.delete()
        }
    }
}
