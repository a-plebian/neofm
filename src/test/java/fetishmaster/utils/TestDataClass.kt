package fetishmaster.utils

data class TestDataClass(
    var firstField: Int = 2,
    var secondField: String = "two",
): ICopiable<TestDataClass> {
	override fun copy(): TestDataClass {
		return TestDataClass(
			firstField,
			secondField,
		)
	}
}

