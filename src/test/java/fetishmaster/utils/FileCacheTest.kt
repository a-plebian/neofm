package fetishmaster.utils

import fetishmaster.FetishMasterTest
import io.kotest.core.spec.style.ShouldSpec
import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.collections.shouldContainExactlyInAnyOrder
import io.kotest.matchers.collections.shouldHaveSingleElement
import io.kotest.matchers.shouldBe
import java.nio.file.Path

const val testFilesPath = "${FetishMasterTest.testFilesDir}/testFiles"
const val path1 = "$testFilesPath/Test1.txt"
const val path2 = "$testFilesPath/Test2.txt"

class FileCacheTests : ShouldSpec({
	beforeSpec {
		FetishMasterTest.clearTestOutput()
	}

	afterSpec {
		FetishMasterTest.clearTestOutput()
	}

	beforeEach {
		FileCache.clearCache()
	}

	context("file cache") {
		should("load files and cache them") {
			val result = FileCache.getFile(path1)
			result shouldBe "Example Text"
			val result2 = FileCache.getFile(path2)
			result2 shouldBe "Example Text 2"
			FileCache.cacheLength() shouldBe 2
		}
		should("accurately report cache length") {
			FileCache.cacheLength() shouldBe 0
			FileCache.getFile(path1)
			FileCache.cacheLength() shouldBe 1
			FileCache.getFile(path2)
			FileCache.cacheLength() shouldBe 2
			FileCache.getFile(path2)
			FileCache.cacheLength() shouldBe 2
		}
		should("clear cache after clear is called") {
			FileCache.cacheLength() shouldBe 0
			FileCache.getFile(path1)
			FileCache.getFile(path2)
			FileCache.cacheLength() shouldBe 2
			FileCache.clearCache()
			FileCache.cacheLength() shouldBe 0
		}
		should("write and then deserialize XML object") {
			val filePath = FetishMasterTest.testOutputDir + "testOut.txt"
			val firstObject = TestDataClass()
			FileCache.writeXMLObject(filePath, firstObject)
			val readBack: TestDataClass = FileCache.getXMLObject(Path.of(filePath))
			firstObject shouldBe readBack
		}
		should("read entire folder of XML elements") {
			val objList: List<TestDataClass> = FileCache.getDirXMLObjects(FetishMasterTest.testFilesDir + "/testXML")
			objList shouldContainExactlyInAnyOrder listOf(
				TestDataClass(),
				TestDataClass(4, "four"),
				TestDataClass(8, "eight"),
			)
		}
		should("recurse into folders when loading folders") {
			val objList: List<TestDataClass> = FileCache.getDirXMLObjects(FetishMasterTest.testFilesDir + "/testXMLRecursive")
			objList shouldContainExactlyInAnyOrder listOf(
					TestDataClass(),
					TestDataClass(4, "four"),
					TestDataClass(8, "eight"),
					TestDataClass(20, "twenty"),
					TestDataClass(40, "forty"),
					TestDataClass(80, "eighty"),
					TestDataClass(200, "two hundred"),
					TestDataClass(400, "four hundred"),
					TestDataClass(800, "eight hundred"),
			)
		}
		should("mask files when loading dirs with a mask") {
			val objList: List<TestDataClass> = FileCache.getDirXMLObjects(
					FetishMasterTest.testFilesDir + "/testXML", "*.snd"
			)
			objList shouldHaveSingleElement TestDataClass(4, "four")
		}
		should("mask files when loading recursively") {
			val objList: List<TestDataClass> = FileCache.getDirXMLObjects(FetishMasterTest.testFilesDir + "/testXMLRecursive", "*.snd")
			objList shouldContainExactlyInAnyOrder listOf(
					TestDataClass(4, "four"),
					TestDataClass(40, "forty"),
					TestDataClass(400, "four hundred"),
			)
		}
		should("mask directories when loading recursively") {
			val objList: List<TestDataClass> = FileCache.getDirXMLObjects(FetishMasterTest.testFilesDir + "/testXMLRecursive", "first/*.fst")
			objList shouldHaveSingleElement TestDataClass()
		}
	}
})
