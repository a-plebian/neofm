/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.display.models;

import fetishmaster.items.Item;
import fetishmaster.items.ItemShop;

import javax.swing.*;

/**
 * @author H.Coder
 */
public class ItemShopListModel extends AbstractListModel {

	protected ItemShop shop;

	public ItemShopListModel(ItemShop shop) {
		this.shop = shop;
	}

	@Override
	public int getSize() {

		return shop.getItemCount();
		//throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public Object getElementAt(int index) {
		Item it = shop.lookAtPos(index);

		if (it == null)
			return null;

		return it.getName() + " " + it.getValue() + "c.";

		//throw new UnsupportedOperationException("Not supported yet.");
	}

}
