/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.display.models;

import fetishmaster.bio.Creature;
import fetishmaster.contracts.EmployAgency;
import fetishmaster.contracts.WorkerContract;

import javax.swing.*;

/**
 * @author H.Coder
 */
public class ContractListModel extends AbstractListModel {
	EmployAgency ea;

	public ContractListModel(EmployAgency ea) {
		this.ea = ea;
	}

	@Override
	public int getSize() {
		return ea.contracts.getContractCount();
	}

	@Override
	public Object getElementAt(int index) {
		WorkerContract wc = ea.contracts.getContract(index);
		Creature c = wc.getWorker();
		return c.getName() + " " + (c.getAge() / (365 * 24)) + " y.o.";
	}

}
