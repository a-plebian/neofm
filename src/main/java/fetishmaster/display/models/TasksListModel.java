/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.display.models;

import fetishmaster.engine.ManagmentTask;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author H.Coder
 */
public class TasksListModel extends AbstractListModel {

	private List<ManagmentTask> data = new ArrayList<>();


	public void setData(List<ManagmentTask> al) {
		data = al;
	}

	@Override
	public int getSize() {
		return data.size();
	}

	@Override
	public ManagmentTask getElementAt(int index) {
		return data.get(index);
	}

}
