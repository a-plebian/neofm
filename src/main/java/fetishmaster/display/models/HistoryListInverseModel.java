/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.display.models;

import fetishmaster.bio.Creature;
import fetishmaster.engine.HistoryEvent;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author H.Coder
 */
public class HistoryListInverseModel extends AbstractListModel {

	List<HistoryEvent> events;

	public HistoryListInverseModel(Creature c) {
		events = c.getHistory();
	}

	@Override
	public int getSize() {
		return events.size();
	}

	@Override
	public Object getElementAt(int index) {
		int pos = getSize() - index - 1;

		HistoryEvent hs = null;
		if (pos < events.size())
			hs = (HistoryEvent) events.get(pos);

		return hs;
	}

}
