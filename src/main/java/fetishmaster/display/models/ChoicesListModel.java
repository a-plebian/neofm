/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.display.models;

import fetishmaster.engine.TextTemplate;
import fetishmaster.engine.WalkChoice;
import fetishmaster.engine.WalkEvent;

import javax.swing.*;

/**
 * @author H.Coder
 */
public class ChoicesListModel extends AbstractListModel {
	WalkEvent we;
	TextTemplate tpl;

	public ChoicesListModel(WalkEvent we) {
		this.we = we;
	}

	public ChoicesListModel(TextTemplate tpl) {
		this.tpl = tpl;
	}

	@Override
	public int getSize() {
		if (tpl == null)
			return we.getChoicesCount();
		else
			return tpl.getChoicesCount();
	}

	@Override
	public Object getElementAt(int index) {
		WalkChoice res;

		if (tpl == null)
			res = we.getChoice(index);
		else
			res = tpl.getChoice(index);

		return res;
	}


}
