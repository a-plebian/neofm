/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.display.models;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author H.Coder
 */
public class jListWorkersModel extends AbstractListModel {
	List c;
	int size = 0;

	public jListWorkersModel(List c) {
		this.c = c;

	}

	@Override
	public int getSize() {
		Collections.sort(c);
		recheckSize();
		return c.size();
	}

	@Override
	public Object getElementAt(int index) {
		//recheckSize();
		//fireContentsChanged(this, 0, size);
		try {
			return c.get(index);
		} catch (IndexOutOfBoundsException e) {
			return c.get(c.size());
		}

	}

	public void recheckSize() {
		if (c.size() != size || c.isEmpty()) {
			size = c.size();

			fireContentsChanged(this, 0, size);
		}
	}

}
