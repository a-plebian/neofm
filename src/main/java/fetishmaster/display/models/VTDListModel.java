/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.display.models;

import fetishmaster.components.ValueTextDescriptor;

import javax.swing.*;

/**
 * @author H.Coder
 */
public class VTDListModel extends AbstractListModel {

	private final ValueTextDescriptor vtd;

	public VTDListModel(ValueTextDescriptor vtd) {
		this.vtd = vtd;
	}

	@Override
	public int getSize() {
		return vtd.getSize();
		//throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public Object getElementAt(int index) {
		String res = vtd.getMin(index) + " - " + vtd.getMax(index);

		return res;
		//throw new UnsupportedOperationException("Not supported yet.");
	}

}
