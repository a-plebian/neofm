/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * JPanelInventory.java
 *
 * Created on 11.05.2012, 0:39:36
 */
package fetishmaster.display;

import fetishmaster.bio.Creature;
import fetishmaster.display.models.ItemListModel;
import fetishmaster.engine.GameEngine;
import fetishmaster.engine.WalkEngine;
import fetishmaster.items.Item;
import fetishmaster.items.ItemBag;
import fetishmaster.items.ItemProcessor;

import javax.swing.*;

/**
 * @author H.Coder
 */
public class JPanelInventory extends javax.swing.JPanel {

	protected ItemBag ib;
	protected Creature c;
	protected javax.swing.JButton jButton_useItem;
	protected javax.swing.JList jList_itemList;
	// Variables declaration - do not modify//GEN-BEGIN:variables
	private javax.swing.JButton jButton_dropItem;
	private javax.swing.JLabel jLabel_value;
	private javax.swing.JLabel jLabel_weight;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JScrollPane jScrollPane2;
	private javax.swing.JTextPane jTextPane_itemDescr;

	public JPanelInventory() {
		initComponents();

	}

	public void clearVisuals() {
		jLabel_value.setText("");
		jLabel_weight.setText("");
		jList_itemList.setModel(new DefaultListModel());
		jTextPane_itemDescr.setText("");
	}

	public void setNewCreature(Creature c) {

		this.c = c;
		ib = c.inventory;

		ItemListModel ilm = new ItemListModel(ib);

		jList_itemList.setModel(ilm);
	}

	protected void usePressed() {
		int pos = jList_itemList.getSelectedIndex();
		if (pos == -1)
			return;

		Item it = c.inventory.lookAtItem(pos);

		String res = ItemProcessor.useItem(c, it.getName());

		if (!res.equals(""))
			WalkEngine.addWalkText("\n" + res + "\n");

		if (GameEngine.InInteractionMode()) {
			if (GameEngine.inventoryWindow != null) {
				GameEngine.inventoryWindow.dispose();
				GameEngine.inventoryWindow = null;
				return;
			}
		}

		setNewCreature(c);

		jList_itemList.setSelectedIndex(pos);
		GameEngine.activeMasterWindow.refreshWindow();
		GameEngine.activeMasterWindow.nextHourUpdate();

	}

	/**
	 * This method is called from within the constructor to
	 * initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is
	 * always regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
	// <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
	private void initComponents() {

		jScrollPane1 = new javax.swing.JScrollPane();
		jList_itemList = new javax.swing.JList();
		jScrollPane2 = new javax.swing.JScrollPane();
		jTextPane_itemDescr = new javax.swing.JTextPane();
		jButton_useItem = new javax.swing.JButton();
		jButton_dropItem = new javax.swing.JButton();
		jLabel_weight = new javax.swing.JLabel();
		jLabel_value = new javax.swing.JLabel();

		jList_itemList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
		jList_itemList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
			public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
				jList_itemListValueChanged(evt);
			}
		});
		jScrollPane1.setViewportView(jList_itemList);

		jTextPane_itemDescr.setEditable(false);
		jScrollPane2.setViewportView(jTextPane_itemDescr);

		jButton_useItem.setText("Use Item");
		jButton_useItem.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton_useItemActionPerformed(evt);
			}
		});

		jButton_dropItem.setText("Drop Item");
		jButton_dropItem.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton_dropItemActionPerformed(evt);
			}
		});

		jLabel_weight.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
		jLabel_weight.setText(" ");

		jLabel_value.setText(" ");

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(layout.createSequentialGroup()
								.addContainerGap()
								.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
										.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
												.addComponent(jLabel_value, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
												.addGap(18, 18, 18)
												.addComponent(jLabel_weight, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
										.addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE))
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
										.addGroup(layout.createSequentialGroup()
												.addComponent(jButton_useItem)
												.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
												.addComponent(jButton_dropItem))
										.addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 287, javax.swing.GroupLayout.PREFERRED_SIZE))
								.addContainerGap())
		);
		layout.setVerticalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(layout.createSequentialGroup()
								.addContainerGap()
								.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
										.addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING)
										.addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 346, Short.MAX_VALUE))
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
										.addComponent(jLabel_value)
										.addComponent(jLabel_weight)
										.addComponent(jButton_useItem)
										.addComponent(jButton_dropItem))
								.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
	}// </editor-fold>//GEN-END:initComponents

	private void jList_itemListValueChanged(javax.swing.event.ListSelectionEvent evt)//GEN-FIRST:event_jList_itemListValueChanged
	{//GEN-HEADEREND:event_jList_itemListValueChanged
		if (jList_itemList.isSelectionEmpty()) {
			jTextPane_itemDescr.setText("");
			jLabel_value.setText("");
			jLabel_weight.setText("");
			return;
		}

		Item it = ib.lookAtItem(jList_itemList.getSelectedIndex());

		jTextPane_itemDescr.setText(it.getDescr());

		jLabel_value.setText("Value: " + it.getSellValue());
		jLabel_weight.setText(String.format("%.1f", it.getWeight()) + " kg.");
	}//GEN-LAST:event_jList_itemListValueChanged

	private void jButton_useItemActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButton_useItemActionPerformed
	{//GEN-HEADEREND:event_jButton_useItemActionPerformed
		usePressed();

	}//GEN-LAST:event_jButton_useItemActionPerformed

	private void jButton_dropItemActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_jButton_dropItemActionPerformed
	{//GEN-HEADEREND:event_jButton_dropItemActionPerformed
		int pos = jList_itemList.getSelectedIndex();
		if (pos == -1)
			return;

		Item it = c.inventory.lookAtItem(pos);
		if (it != null) {
			c.inventory.removeItem(pos);
		}

		setNewCreature(c);

		jList_itemList.setSelectedIndex(pos);
	}//GEN-LAST:event_jButton_dropItemActionPerformed
	// End of variables declaration//GEN-END:variables
}
