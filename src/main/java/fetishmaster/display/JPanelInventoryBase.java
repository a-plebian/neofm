/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.display;

import fetishmaster.bio.Creature;
import fetishmaster.display.models.ItemListModel;
import fetishmaster.engine.GameEngine;
import fetishmaster.items.Item;
import fetishmaster.items.ItemProcessor;

/**
 * @author H.Coder
 */
public class JPanelInventoryBase extends JPanelInventory {

	protected Creature target;

	public JPanelInventoryBase() {
		super();
		this.jButton_useItem.setText("Use from storage");
	}

	public void setNewTarget(Creature tc) {
		this.target = tc;
	}

	public void initStorage() {
		ItemListModel ilm;
		ilm = new ItemListModel(GameEngine.activeWorld.playerAvatar.inventory);
		jList_itemList.setModel(ilm);
		c = GameEngine.activeWorld.playerAvatar;
		ib = c.inventory;

	}

	@Override
	public void clearVisuals() {

	}

	@Override
	public void setNewCreature(Creature c) {
		initStorage();
	}

	@Override
	protected void usePressed() {

		if (c == null || target == null)
			return;

		int pos = jList_itemList.getSelectedIndex();
		if (pos == -1)
			return;

		Item it = c.inventory.lookAtItem(pos);

		String res = ItemProcessor.useItem(c, target, it.getName());

		jList_itemList.setSelectedIndex(pos);
		GameEngine.activeMasterWindow.refreshWindow();
		GameEngine.activeMasterWindow.nextHourUpdate();

	}
}
