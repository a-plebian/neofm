/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.display;

import javax.swing.*;
import java.awt.*;

/**
 * @author H.Coder
 */
public class JFrameAligned extends JFrame {
	public JFrameAligned() {

	}


	public void AlignCenter(JFrame base) {
		Dimension al = this.getSize();
		Dimension bas;
		int x, y, w, h;

		if (base != null) {
			bas = base.getSize();
			x = base.getX() + (bas.width - al.width) / 2;
			y = base.getY() + (bas.height - al.height) / 2;
		} else {
			bas = Toolkit.getDefaultToolkit().getScreenSize();
			x = (bas.width - al.width) / 2;
			y = (bas.height - al.height) / 2;
		}


		w = al.width;
		h = al.height;

		this.setBounds(x, y, w, h);

		this.setResizable(false);

	}

}
