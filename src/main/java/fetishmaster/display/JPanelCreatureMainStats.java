/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * JPanelCreatureMainStats.java
 *
 * Created on 20.04.2012, 17:10:10
 */
package fetishmaster.display;

import fetishmaster.bio.Creature;
import fetishmaster.bio.RNAGene;
import fetishmaster.bio.RNAGenePool;

/**
 * @author H.Coder
 */
public class JPanelCreatureMainStats extends javax.swing.JPanel {

	// Variables declaration - do not modify//GEN-BEGIN:variables
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JLabel jLabel3;
	private javax.swing.JLabel jLabel4;
	private javax.swing.JLabel jLabel5;
	private javax.swing.JLabel jLabel6;
	private javax.swing.JProgressBar jProgressBar_CHA;
	private javax.swing.JProgressBar jProgressBar_DEX;
	private javax.swing.JProgressBar jProgressBar_INT;
	private javax.swing.JProgressBar jProgressBar_SPD;
	private javax.swing.JProgressBar jProgressBar_STA;
	private javax.swing.JProgressBar jProgressBar_STR;
	/**
	 * Creates new form JPanelCreatureMainStats
	 */
	public JPanelCreatureMainStats() {
		initComponents();
		this.setSize(230, 180);
	}

	public void UpdatePanel(Creature c) {
		RNAGenePool rna = c.getRna();
		RNAGene r;
		Integer val;

		r = rna.getGene("generic.str");
		if (r != null) {
			val = (int) r.getValue();
			jProgressBar_STR.setValue(val);
			jProgressBar_STR.setString(val.toString());
		}

		r = rna.getGene("generic.dex");
		if (r != null) {
			val = (int) r.getValue();
			jProgressBar_DEX.setValue(val);
			jProgressBar_DEX.setString(val.toString());
		}


		r = rna.getGene("generic.spd");
		if (r != null) {
			val = (int) r.getValue();
			jProgressBar_SPD.setValue(val);
			jProgressBar_SPD.setString(val.toString());
		}

		r = rna.getGene("generic.sta");
		if (r != null) {
			val = (int) r.getValue();
			jProgressBar_STA.setValue(val);
			jProgressBar_STA.setString(val.toString());
		}

		r = rna.getGene("generic.int");
		if (r != null) {
			val = (int) r.getValue();
			jProgressBar_INT.setValue(val);
			jProgressBar_INT.setString(val.toString());
		}

		r = rna.getGene("generic.cha");
		if (r != null) {
			val = (int) r.getValue();
			jProgressBar_CHA.setValue(val);
			jProgressBar_CHA.setString(val.toString());
		}

	}

	/**
	 * This method is called from within the constructor to
	 * initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is
	 * always regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
	// <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
	private void initComponents() {

		jProgressBar_STR = new javax.swing.JProgressBar();
		jProgressBar_DEX = new javax.swing.JProgressBar();
		jProgressBar_SPD = new javax.swing.JProgressBar();
		jProgressBar_INT = new javax.swing.JProgressBar();
		jProgressBar_CHA = new javax.swing.JProgressBar();
		jLabel1 = new javax.swing.JLabel();
		jLabel2 = new javax.swing.JLabel();
		jLabel3 = new javax.swing.JLabel();
		jLabel4 = new javax.swing.JLabel();
		jLabel5 = new javax.swing.JLabel();
		jLabel6 = new javax.swing.JLabel();
		jProgressBar_STA = new javax.swing.JProgressBar();

		setPreferredSize(new java.awt.Dimension(230, 160));

		jProgressBar_STR.setString("0");
		jProgressBar_STR.setStringPainted(true);

		jProgressBar_DEX.setString("0");
		jProgressBar_DEX.setStringPainted(true);

		jProgressBar_SPD.setString("0");
		jProgressBar_SPD.setStringPainted(true);

		jProgressBar_INT.setString("0");
		jProgressBar_INT.setStringPainted(true);

		jProgressBar_CHA.setString("0");
		jProgressBar_CHA.setStringPainted(true);

		jLabel1.setText("STR");

		jLabel2.setText("DEX");

		jLabel3.setText("SPD");

		jLabel4.setText("INT");

		jLabel5.setText("CHA");

		jLabel6.setText("END");

		jProgressBar_STA.setString("0");
		jProgressBar_STA.setStringPainted(true);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(layout.createSequentialGroup()
								.addContainerGap()
								.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
										.addComponent(jLabel4)
										.addComponent(jLabel6)
										.addComponent(jLabel1)
										.addComponent(jLabel2)
										.addComponent(jLabel5)
										.addComponent(jLabel3))
								.addGap(18, 18, 18)
								.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
												.addComponent(jProgressBar_DEX, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
												.addComponent(jProgressBar_SPD, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
												.addComponent(jProgressBar_STR, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
										.addComponent(jProgressBar_INT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
										.addComponent(jProgressBar_STA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
										.addComponent(jProgressBar_CHA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
								.addContainerGap(35, Short.MAX_VALUE))
		);
		layout.setVerticalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(layout.createSequentialGroup()
								.addContainerGap()
								.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
										.addComponent(jProgressBar_STR, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
										.addComponent(jLabel1))
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
								.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addGroup(layout.createSequentialGroup()
												.addComponent(jProgressBar_DEX, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
												.addGap(11, 11, 11)
												.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
														.addComponent(jProgressBar_SPD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)))
										.addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
								.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
										.addComponent(jLabel6)
										.addComponent(jProgressBar_STA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
								.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addComponent(jLabel4)
										.addComponent(jProgressBar_INT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
								.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addComponent(jLabel5)
										.addComponent(jProgressBar_CHA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
								.addContainerGap(13, Short.MAX_VALUE))
		);
	}// </editor-fold>//GEN-END:initComponents
	// End of variables declaration//GEN-END:variables
}
