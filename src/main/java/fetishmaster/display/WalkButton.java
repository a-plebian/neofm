/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.display;

import fetishmaster.engine.WalkEngine;
import fetishmaster.engine.WalkEvent;

import javax.swing.*;

/**
 * @author H.Coder
 */
public class WalkButton extends JButton {
	public int hours = 1;
	//public String value;
	private WalkEvent event;
	private Object scriptObject = null;

	public WalkButton(String name, WalkEvent event) {
		this.setText(name);
		this.event = event;
		//      this.value = value;
		this.setSize(100, 20); //To do - dinamic size
		this.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				pressed(evt);
			}
		});
	}

	public WalkButton(String name, WalkEvent event, int hours) {
		this.hours = hours;
		this.setText(name);
		//     this.value = value;
		this.event = event;
		this.setSize(100, 20); //To do - dinamic size
		this.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				pressed(evt);
			}
		});
	}

	public void pressed(java.awt.event.ActionEvent evt) {
		WalkEngine.walkButtonPressed(this);
	}

	/**
	 * @return the scriptValue
	 */
	public Object getScriptObject() {
		return scriptObject;
	}

	/**
	 * @param scriptValue the scriptValue to set
	 */
	public void setScriptObject(Object scriptObject) {
		this.scriptObject = scriptObject;
	}

	/**
	 * @return the event
	 */
	public WalkEvent getEvent() {
		return event;
	}

	/**
	 * @param event the event to set
	 */
	public void setEvent(WalkEvent event) {
		this.event = event;
	}

}
