/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.items;

import fetishmaster.bio.Creature;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;

/**
 * @author H.Coder
 */
public class ItemBag {

	protected List<Item> items = new ArrayList<>();
	protected ArrayList counts;

	public int getNamesCount() {
		return items.size();
	}

	public Item lookAtItem(int pos) {
		return items.get(pos);
	}

	public int itemsCountAtPos(int pos) {
		Item it = lookAtItem(pos);
		return it.getCount();
	}

	public int itemsCount(String itemName) {
		int res = 0;

		Item it = getItem(itemName);
		if (it != null)
			res = it.getCount();

		return res;
	}

	public Item getItem(int pos) {
		if (items.size() < pos)
			return null;

		return items.get(pos);
	}

	public Item getItem(String itemName) {
		Predicate<Item> con1 = item -> item.getName().equals(itemName);
		Predicate<Item> con2 = item -> item.getFilename().equals(itemName);

		return items.stream()
				.filter(con1.or(con2))
				.findFirst()
				.orElse(null);
	}

	public boolean removeItem(int pos) {
		if (items.size() < pos)
			return false;

		Item it = getItem(pos);
		int cnt = it.getCount();

		if (cnt < 0)
			return false;

		if (cnt > 1) {
			cnt--;
			it.setCount(cnt);
		} else {
			items.remove(pos);
//            counts.remove(pos);
		}

		return true;
	}

	public Item takeItem(int pos) {
		Item it, ret;

		it = getItem(pos);
		if (it == null)
			return null;

		if (it.isCanBeStacked())
			ret = ItemProcessor.loadItem(it.getFilename());
		else
			ret = it;

		removeItem(pos);

		return ret;
	}

	public void addItem(Item item) {
		if (item == null)
			return;

		Item it;
		Item itm;
		int i, cnt;

		if (item.isCanBeStacked()) {
			if (!hasItem(item)) {
				items.add(item);
//            counts.add(1);
			} else {
				i = posOfItem(item.getName());
				itm = lookAtItem(i);
				cnt = itm.getCount();
				cnt++;
				itm.setCount(cnt);

			}
		} else {
			items.add(item);
		}
	}

	public boolean hasItem(String itemName) {
		Item it = getItem(itemName);
//        int i;
		boolean res = it != null;

		//        for (i = 0; i<items.size(); i++)
//        {
//            it = (Item) items.get(i);
//            if (it.getName().equals(itemName))
//                res = true;
//        }

		return res;
	}

	public boolean hasItem(Item item) {
		return hasItem(item.getFilename());
	}

	public int posOfItem(String itemName) {
		Item it;
		int i;

		for (i = 0; i < items.size(); i++) {
			it = (Item) items.get(i);
			if (it.getName().equals(itemName))
				return i;
		}

		for (i = 0; i < items.size(); i++) {
			it = (Item) items.get(i);
			if (it.getFilename().equals(itemName))
				return i;
		}

		return -1;
	}

	public int posCount() {
		return this.items.size();
	}

	public double getWeight() {
		double res = 0;
		int i;
		Item it;

		for (i = 0; i < this.items.size(); i++) {
			it = lookAtItem(i);
			res += it.getWeight() * itemsCountAtPos(i);
		}

		return res;
	}

	public void addMoney(int money) {
		Item coins = null;
		Iterator it = items.iterator();

		while (it.hasNext()) {
			coins = (Item) it.next();
			if (coins.getName().equals("Coin") || coins.getName().equals("Coins")) {
				break;
			} else {
				coins = null;
			}
		}

		if (coins == null) {
			coins = ItemProcessor.loadItem("coin");
			money--;
			items.add(coins);
		}

		coins.setCount(money + coins.getCount());
	}

	public boolean removeMoney(int money) {
		Item coins = null;
		Iterator it = items.iterator();

		while (it.hasNext()) {
			coins = (Item) it.next();
			if (coins.getName().equals("Coin") || coins.getName().equals("Coins")) {
				break;
			} else {
				coins = null;
			}
		}

		if (coins == null) {
			return false;
		}

		if (money < coins.getCount()) {
			coins.setCount(coins.getCount() - money);
			return true;
		}

		if (money == coins.getCount()) {
			this.items.remove(coins);
			return true;
		}

		return false;


	}

	public int moneyCount() {
		Item coins = null;
		Iterator it = items.iterator();

		while (it.hasNext()) {
			coins = (Item) it.next();
			if (coins.getName().equals("Coin") || coins.getName().equals("Coins")) {
				break;
			} else {
				coins = null;
			}
		}

		if (coins == null) {
			return 0;
		}

		return coins.getCount();
	}

	public void nextHour(Creature c) {
		if (c == null)
			return;

		for (int i = items.size() - 1; i > 0; i--) {
			Item itm = this.getItem(i);
			itm.nextHour(c);
		}
	}

}
