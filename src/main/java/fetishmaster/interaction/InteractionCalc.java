/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.interaction;

import fetishmaster.bio.Creature;
import fetishmaster.bio.DNAGene;
import fetishmaster.bio.RNAGene;
import fetishmaster.engine.GameEngine;
import fetishmaster.utils.Debug;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author H.Coder
 */
public class InteractionCalc {
	private static final Logger logger = LoggerFactory.getLogger(InteractionCalc.class);

	public static int AttackChance(Creature atk, Creature def) {
		double astr, dstr, adex, ddex;
		astr = atk.getRNAValue(DNAGene.STR);
		dstr = def.getRNAValue(DNAGene.STR);
		adex = atk.getRNAValue(DNAGene.DEX);
		ddex = def.getRNAValue(DNAGene.DEX);

		double chance = 100 - ((ddex - adex / 2) + (dstr - astr / 2));
		if (chance < 5)
			chance = 5;

		if (chance > 95)
			chance = 95;

		return (int) chance;
	}

	public static int AttackDamage(Creature atk, Creature def) {
		double res, at, df;

		at = atk.getRNAValue("generic.attack");
		df = def.getRNAValue("generic.defence");
		res = Math.random() * (at - df);

		if (GameEngine.devMode) {
			System.out.println("Attack:" + at + "  Defence:" + df + "  Result:" + res);
		}

		if (res < 1)
			res = 1;

		return (int) res;
	}

	public static int RunChance(Creature run, Creature cathing) {
		double rspd, cspd;

		rspd = run.getRNAValue(DNAGene.SPD);
		cspd = cathing.getRNAValue(DNAGene.SPD);

		double chance = 90 - (cspd - rspd);
		if (chance < 5)
			chance = 5;

		if (chance > 95)
			chance = 95;

		if (GameEngine.devMode)
			System.out.println("Chance to run: " + chance);

		return (int) chance;
	}

	public static double ShameCheck(Creature c, double force) {
		double l = c.getRNAValue("generic.lewdness");
		double res;

		res = force - l;

		if (res < 0)
			return 0;

		return res;
	}

	public static double Attraction(Creature c, Creature t) {
		double res;
		double bonus = 0;
		double base_to_t = 0;

		if (t.isNeuter())
			base_to_t = c.getStat("psy.to_neuter");
		if (t.isMale())
			base_to_t = c.getStat("psy.to_male");
		if (t.isFemale())
			base_to_t = c.getStat("psy.to_female");
		if (t.isFuta())
			base_to_t = c.getStat("psy.to_futa");

		res = base_to_t + t.getStat("generic.cha") / 2;

		logger.debug("Character \"{}\" attraction to \"{}\" is now \"{}\"", c.getName(), t.getName(), res);

		return res;
	}

}
