/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster;

import fetishmaster.bio.Creature;
import fetishmaster.bio.DNAGene;
import fetishmaster.bio.RNAGene;
import fetishmaster.components.GameClock;
import fetishmaster.contracts.ContractList;
import fetishmaster.contracts.EmployAgency;
import fetishmaster.contracts.WorkerContract;
import fetishmaster.engine.*;
import fetishmaster.engine.backgrounds.AddWorldHoursBG;
import fetishmaster.engine.backgrounds.BackgroundsManager;
import fetishmaster.engine.scripts.ScriptEngine;
import fetishmaster.engine.scripts.VarContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.*;

/**
 * @author H.Coder
 */
public class World implements Serializable {
	private static final Logger logger = LoggerFactory.getLogger(World.class);

	private final List<Creature> creatures = new ArrayList<>();
	private final List<Creature> workers = new ArrayList<>();
	private final List<Creature> returners = new ArrayList<>();
	public double content_base = 0;
	public double core_version = 0;
	public GameClock clock;
	public Creature playerAvatar;
	public Creature activePartner;
	public Creature selectedCreature;
	public Creature interactionCreature;
	//public Creature scriptSelfCreature;
	public Creature currentEnemy;
	public String lastEvent = "in_the_base";
	//public Room activeRoom;
	public String startedInVersion;
	//public ArrayList roomsSet = new ArrayList();
	public EventsStack eventsOnHold = new EventsStack();
	public ContractList contracts = new ContractList();
	public EmployAgency agency = new EmployAgency();
	public Map<String, Integer> flags = new HashMap<>();
	public Map<String, String> textFlags = new HashMap<>();
	public Map objects = Collections.synchronizedMap(new HashMap());
	public boolean inInteractionMode = false;
	public boolean inSexMode = false;
	public String notes = "";

	public World() {
		clock = new GameClock();
		playerAvatar = new Creature("Player");
		startedInVersion = GameEngine.version;
	}

	public List<Creature> getCreatures() {
		return creatures;
	}

	public List<Creature> getWorkers() {
		return workers;
	}

	public List<Creature> getReturners() {
		return returners;
	}

	public synchronized void addCreature(Creature c) {
		if (creatures.contains(c))
			return;

		creatures.add(c);
	}

	public synchronized boolean isCreatureExists(Creature c) {
		return creatures.contains(c);
	}

	public synchronized Creature getCreature(int i) {
		return creatures.get(i);
	}

	public void removeCreature(Creature c) {
		creatures.remove(c);
	}

	public synchronized void addWorker(Creature c) {
		if (workers.contains(c))
			return;

		workers.add(c);
	}

	public synchronized boolean isWorkerExists(Creature c) {
		return workers.contains(c);
	}

	public synchronized Creature getWorker(int i) {
		return workers.get(i);
	}

	public void removeWorker(Creature c) {
		workers.remove(c);
	}

	public synchronized void addReturner(Creature c) {
		if (returners.contains(c))
			return;

		returners.add(c);
	}

	public synchronized boolean isReturnerExists(Creature c) {
		return returners.contains(c);
	}

	public synchronized Creature getReturner(int i) {
		return returners.get(i);
	}

	public void removeReturner(Creature c) {
		workers.remove(c);
	}

	public boolean nextHour(boolean timeSkipping) {
		// copy needed to avoid concurent ArrayList modification error in rare cases.
		ArrayList<Creature> ccopy = new ArrayList<Creature>(creatures);
		Iterator<Creature> it = ccopy.iterator();
		Creature creature;
		boolean emergency = false;
		String s;
		TextTemplate t;

		//WalkEngine.ReturnPointsCount = 0;

		while (it.hasNext()) {

			creature = it.next();
			if (creature.nextHour(timeSkipping)) {
				emergency = true;
			}
			//Script integration for critical conditions.
			if (creature != null) {
				VarContext vars = new VarContext();
				ScriptEngine.loadVars(vars, creature, null);
				WalkFrame wf = new WalkFrame();
				wf.setVarsContext(vars);
				synchronized (BackgroundsManager.notifyer) {
					WalkEngine.setSpecialIncludeFrame(wf);
					WalkEngine.processInclude("system/next_hour", wf);//= ScriptEngine.parseForScripts(t.getText(), wf.getVarsContext());
					WalkEngine.clearSpecialIncludeFrame();
				}
			} else {
				logger.error(
						"Null pointer in the characters list!\nOriginal List:\n{}\nCopy List:\n{}\n=======================",
						creatures.toString(),
						ccopy.toString()
				);
			}

		}

		clock.addHours(1);

		agency.nextHour();

		if (!GameEngine.nowWalking) {
			contractorsCheck();
		}

		returnersCheck();

		VarContext vars = new VarContext();
		ScriptEngine.loadVars(vars, GameEngine.activeWorld.playerAvatar, null);
		WalkFrame wf = new WalkFrame();
		wf.setVarsContext(vars);
		synchronized (BackgroundsManager.notifyer) {
			WalkEngine.setSpecialIncludeFrame(wf);
			WalkEngine.processInclude("system/world_next_hour", wf);
			WalkEngine.clearSpecialIncludeFrame();
		}
		return emergency;
	}

	public int addHours(int hours) {
		int i;
		boolean warn;
		for (i = 0; i < hours; i++) {
			warn = nextHour(false);
			if (warn) {
				return hours - i;
			}
		}

		GameEngine.activeMasterWindow.nextHourUpdate();
		return 0;
	}

	public void addHoursBG(int hours) {
		BackgroundsManager.addBackgroundTask(new AddWorldHoursBG(hours));
	}

	private void returnersCheck() {
		int i;
		Creature c;
		RNAGene r;

		for (i = 0; i < returners.size(); i++) {
			c = returners.get(i);
			r = c.getRNAGene(DNAGene.HEALTH);
			if (!r.hasEffect("time_to_return")) {
				GameEngine.activeWorld.workers.add(c);
				GameEngine.activeWorld.returners.remove(c);
			}
		}

	}

	private void contractorsCheck() {
		int i;
		WorkerContract wc;
		String failed;

		for (i = 0; i < contracts.getContractCount(); i++) {
			wc = contracts.getContract(i);

			failed = wc.testConditions();

			if (failed != null && wc.isCustom() == false) {
				GameEngine.voidContract(wc);
				GameEngine.alertWindow(failed);
				break;
			} else if (failed != null && wc.isCustom()) {
				GameEngine.alertWindow(failed);
				break;
			}
		}
	}
}
