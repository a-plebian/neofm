    /*
	 * To change this template, choose Tools | Templates
	 * and open the template in the editor.
	 */
	package fetishmaster.bio;

	import fetishmaster.bio.organs.Organ;
	import fetishmaster.components.StatEffect;
	import fetishmaster.contracts.WorkerContract;
	import fetishmaster.engine.GameEngine;
	import fetishmaster.engine.HistoryEvent;
	import fetishmaster.engine.ManagementEngine;
	import fetishmaster.engine.TextProcessor;
	import fetishmaster.interaction.InteractionCalc;
	import fetishmaster.items.Item;
	import fetishmaster.items.ItemBag;
	import fetishmaster.items.ItemProcessor;
	import fetishmaster.utils.Debug;
	import fetishmaster.utils.FileCache;
	import org.slf4j.Logger;
	import org.slf4j.LoggerFactory;

	import java.io.Serializable;
	import java.util.*;

	/**
	 * @author H.Coder
	 */
	public class Creature implements Serializable, Comparable<Creature> {
		private static final Logger logger = LoggerFactory.getLogger(Creature.class);

		public static final int NONE = 0;
		public static final int MALE = 1;
		public static final int FEMALE = 2;
		public static final int FUTA = 3;
		private final String UID;
		public CreatureShedule shedule = new CreatureShedule();
		public ItemBag inventory = new ItemBag();
		public Map<String, Integer> flags = new HashMap<>();
		//    private HashMap stats = new HashMap();
		private Map<String, Organ> organs = new LinkedHashMap<>();
		private DNAGenePool dna = new DNAGenePool();
		private RNAGenePool rna = new RNAGenePool();
		private List<HistoryEvent> history = new ArrayList<>();
		private String name;
		private String nickname;
		private int age = 0;
		private String personalNotes = "";
		private Map<String, Object> objects = Collections.synchronizedMap(new HashMap<>());

		public Creature(String name) {
			this.name = name;
			UID = java.util.UUID.randomUUID().toString();
			initOrgans();
		}

		private void initOrgans() {
		}

		public void addOrgan(Organ organ) {
			organs.put(organ.getName(), organ);

			organ.onAdd(this);
		}

		public void addOrgan(String name) {
			loadOrgan(name);
		}

		public void removeOrgan(String name) {
			Organ o;
			if (this.getOrgans().containsKey(name)) {
				o = (Organ) this.getOrgans().get(name);
				this.getOrgans().remove(name);
				o.onRemove();
			}

			//there need to be genetic changes - setting "passive" to RNAgenes of this organ;

		}

		public boolean hasOrgan(String organName) {
			return this.getOrgans().containsKey(organName);
		}

		public Organ getOrgan(String name) {
			if (this.getOrgans().containsKey(name)) {
				return this.getOrgans().get(name);
			}
			return null;
		}

		public void loadOrgan(String filename) {
			Organ o = FileCache.getGamedata("organs/" + filename + ".organ");
			logger.trace("Loaded organ: \"{}/organs/{}.organ\"", GameEngine.gameDataPath, filename);
			if (o == null)
				return;

			this.addOrgan(o);
		}

		public void loadDNA(String filename) {
			DNAGenePool d = FileCache.getGamedata("dna/" + filename + ".dna");
			logger.trace("Loaded DNA: \"{}/dna/{}.dna\"", GameEngine.gameDataPath, filename);
			if (d == null)
				return;

			this.getDNA().mergeGenes(d);
		}

		public void loadRNA(String filename) {
			DNAGenePool d = FileCache.getGamedata("dna/" + filename + ".dna");
			logger.trace("Loaded RNA: \"{}/dna/{}.dna\"", GameEngine.gameDataPath, filename);
			if (d == null)
				return;

			RNAGenePool r = GeneProcessor.DNAtoRNA(d);

			this.rna.mergeGenes(r);
		}

		public String getName() {
			return name;
		}

		/**
		 * @param name the name to set
		 */
		public void setName(String name) {
			this.name = name;
		}

		public int getAge() {
			return age;
		}

		public void setAge(int age) {
			this.age = age;
		}

		public boolean nextHour() {
			this.nextHour(false);
			return false;
		}

		//return false if no emergency in creature state after next hour
		//return true if it is. for example - dieing or birth...
		public boolean nextHour(boolean timeSkipping) {
			Collection tmp;
			Iterator it;
			Organ o;

			Boolean emergencyState = false;

			//this.addStat("generic.health", this.getStat("generic.regen_rate"));

			//Genes nextHour
			//GameEngine.activeWorld.scriptSelfCreature = this;

			rna.nextHour(this, timeSkipping);

			//Organs nextHour
			if (getOrgans().size() > 0) {
				tmp = organs.values();
				it = tmp.iterator();

				while (it.hasNext()) {
					o = (Organ) it.next();
					emergencyState = o.nextHour();
				}

			}

			// random release to imitate normal sex life :)
//        if (this.isWorker() == false )
//        {
//            if (this.getFlag("chastity") < 1 )
//            {
//                int oh = this.getFlag("next_auto_orgasm_hours");
//                if (oh <= 0)
//                {
//                    this.doAction("orgasm");
//                    this.setStat("generic.tiredness", 0);
//                    this.setFlag("next_auto_orgasm_hours", (int)Calc.random(200));
//                }
//                else
//                {
//                    this.setFlag("next_auto_orgasm_hours", oh-1);
//                }
//
//            }
//        }

			if (this.isWorker() && (!this.isActivePartner())) {
				CreatureProcessor.workerWorkOnShedule(this);
			}

			CreatureProcessor.mainStatsRecheck(this);

			if (!this.isActivePartner() && GameEngine.gameStarted) {
//            WalkFrame wf = new WalkFrame();
//            wf.setActor(this);
//            ScriptEngine.loadVars(wf);
//            WalkEngine.processInclude("system/proxy_walking_check", wf);
				CreatureProcessor.criticalConditionsCheck(this);
			}

			age++;

			//items nextHour code
			this.inventory.nextHour(this);


			return emergencyState;
		}

//    /**
//     * @return the stats
//     */
//    public synchronized HashMap getStats()
//    {
//        return stats;
//    }
//
//    /**
//     * @param stats the stats to set
//     */
//    public synchronized void setStats(HashMap stats)
//    {
//        this.stats = stats;
//    }

		//metod to fast ageeing for genetic changes. Not for regular game. States and organs may not work correctly!!!
		public synchronized boolean addHours(int hours) {

			//one month is in real time;
			int month = (int) (24 * (20 + Math.random() * 20));
			int fasttime = hours - month;

			if (fasttime > 0) {
				hours -= fasttime;
			}

			//Collection tmp;
			//Iterator it;
			//Organ o;

			Boolean emergencyState = false;

			//Genes nextHour
			//GameEngine.activeWorld.scriptSelfCreature = this;

			//math part
			if (fasttime > 0) {

				rna.addHours(fasttime, this);
				age += fasttime;
			}
			//.nextHour part
			int i;
			for (i = 0; i < hours; i++) {
				nextHour(false);
			}

			return false;
		}

		/**
		 * @return the organs
		 */
		public Map<String, Organ> getOrgans() {
			return organs;
		}

		/**
		 * @param organs the organs to set
		 */
		public void setOrgans(Map<String, Organ> organs) {
			this.organs = organs;
		}

		/**
		 * @return the genes
		 */
		public DNAGenePool getGenes() {
			return getDNA();
		}

		/**
		 * @param genes the genes to set
		 */
		public void setGenes(DNAGenePool genes) {
			this.setDna(genes);
		}

		/**
		 * @return the dna
		 */
		public DNAGenePool getDNA() {
			return dna;
		}

		/**
		 * @param dna the dna to set
		 */
		public void setDna(DNAGenePool dna) {
			this.dna = dna;
		}

		/**
		 * @return the rna
		 */
		public RNAGenePool getRna() {
			return rna;
		}

		/**
		 * @param rna the rna to set
		 */
		public void setRna(RNAGenePool rna) {
			this.rna = rna;
		}

		public DNAGene getDNAGene(String gene) {
			DNAGene g;
			g = dna.getGene(gene);
			return g;
		}

		public RNAGene getRNAGene(String gene) {
			RNAGene g;
			g = rna.getGene(gene);
			return g;
		}

		public synchronized double getRNAValue(String gene) {
			RNAGene g = getRNAGene(gene);
			if (g == null) {
				return 0;
			}

			return g.getValue();
		}

		public synchronized double getCleanRNAValue(String gene) {
			RNAGene g = getRNAGene(gene);
			if (g == null) {
				return 0;
			}

			return g.getCleanValue();
		}

		public double getCleanStat(String gene) {
			return getCleanRNAValue(gene);
		}

		public synchronized String getRNAText(String gene) {
			RNAGene g = getRNAGene(gene);
			if (g == null) {
				return "";
			}

			String t = g.getTextValue();
			if (t == null)
				return "";

			return t;
		}

		@Override
		public String toString() {
			if (getNickname().equals(""))
				return this.getName() + ", age " + age / (365 * 24) + "y.o.";
			else
				return this.getName() + " (" + nickname + "), age " + age / (365 * 24) + "y.o.";
		}

		public boolean isWorker() {

			if (GameEngine.activeWorld == null) {
				return false;
			}

//        if (GameEngine.activeWorld.workers == null)
//        {
//            return false;
//        }

			return GameEngine.activeWorld.isWorkerExists(this) || GameEngine.activeWorld.isReturnerExists(this);
//        Creature c;
//        int i;
//
//        for (i = 0; i < GameEngine.activeWorld.workers.size(); i++)
//        {
//            
//            c = (Creature) GameEngine.activeWorld.workers.get(i);
//            if (this == c)
//            {
//                return true;
//            }
//        }
		}

		public boolean isActivePartner() {
			return this == GameEngine.activeWorld.activePartner;
		}

		public void addHistory(String name, String descr) {
			addHistory(name, descr, "");
		}

		public void addHistory(String name, String descr, String img) {
			HistoryEvent he = new HistoryEvent(name, descr);
			he.setImage(img);
			history.add(he);
			if (history.size() > 50) {
				history.remove(0);
			}
		}

		/**
		 * @return the history
		 */
		public List<HistoryEvent> getHistory() {
			return history;
		}

		public void clearHistory() {
			history = new ArrayList<>();
		}

		public BodyResponse doAction(String action) {
			return doAction(action, new HashMap());
		}

		public BodyResponse doAction(String action, Map args) {
			//GameEngine.activeWorld.scriptSelfCreature = this;

			BodyResponse res = new BodyResponse();
			int i;
			Organ o;
			Collection or = organs.values();
			Iterator it = or.iterator();
			while (it.hasNext()) {
				o = (Organ) it.next();
				res.put(o.getName(), o.doAction(action, args));
			}

			return res;
		}

		public void addToWorkers() {
			if (this.isWorker()) {
				return;
			}

			GameEngine.activeWorld.addWorker(this);
			this.shedule.day = ManagementEngine.getDefaultTask();
			this.shedule.evening = ManagementEngine.getDefaultTask();
			this.shedule.morning = ManagementEngine.getDefaultTask();
			this.shedule.night = ManagementEngine.getDefaultTask();

		}

		public void addToWorkers(WorkerContract cont) {
			if (this.isWorker() || cont == null) {
				return;
			}
			cont.setWorker(this);
			GameEngine.activeWorld.contracts.addContract(cont, 0);
			this.addToWorkers();
		}

		public void removeFromWorkers() {
			if (this.isWorker()) {
				GameEngine.activeWorld.removeWorker(this);
				GameEngine.activeWorld.removeReturner(this);
				GameEngine.activeWorld.contracts.removeContract(this);
				this.shedule.day = null;
				this.shedule.evening = null;
				this.shedule.morning = null;
				this.shedule.night = null;
			}
		}

		public void die() {
			this.removeFromWorkers();
			GameEngine.activeWorld.removeCreature(this);
		}

		// here functions mainly for the MVEL integration =======================================================
		public void addStat(String statName, double value) {
			RNAGene r = rna.getGene(statName);
			if (r == null) {
				return;
			}

			r.setValue(r.getCleanValue() + value);
		}

		public void subStat(String statName, double value) {
			addStat(statName, -value);
		}

		public double getStat(String gene) {
			return getRNAValue(gene);
		}

		public String getStatText(String gene) {
			return getRNAText(gene);
		}

		public void setStat(String statName, double value) {
			RNAGene r = rna.getGene(statName);
			if (r == null) {
				return;
			}

			r.setValue(value);
		}

		public void setRNAText(String gene, String text) {
			RNAGene r = rna.getGene(gene);
			if (r == null) {
				return;
			}
			r.setTextValue(text);

		}

		public void setStatText(String gene, String text) {
			setRNAText(gene, text);
		}

		public void addEffect(String statName, String effectName, double effectValue) {
			RNAGene r = rna.getGene(statName);
			if (r == null) {
				return;
			}

			StatEffect eff = new StatEffect(effectName, effectValue);
			r.addEffect(eff);
		}

		public void addEffect(String statName, String effectName, double effectValue, String endText) {
			RNAGene r = rna.getGene(statName);
			if (r == null) {
				return;
			}

			StatEffect eff = new StatEffect(effectName, effectValue, endText);
			r.addEffect(eff);
		}

		public void addEffect(double rForce, String statName, String effectName, double effectValue) {
			RNAGene r = rna.getGene(statName);
			if (r == null) {
				return;
			}

			StatEffect eff = new StatEffect(rForce, effectName, effectValue);
			r.addEffect(eff);
		}

		public void updateEffect(String statName, String effectName, double effectValue) {
			RNAGene r = rna.getGene(statName);
			if (r == null) {
				return;
			}

			r.updateEffect(effectName, effectValue);
		}

		public void addEffect(String statName, String effectName, double effectValue, int hours, String endText) {
			RNAGene r = rna.getGene(statName);
			if (r == null) {
				return;
			}

			StatEffect eff = new StatEffect(effectName, effectValue, hours, endText);
			r.addEffect(eff);
		}

		public void addEffect(String statName, String effectName, double effectValue, int hours) {
			RNAGene r = rna.getGene(statName);
			if (r == null) {
				return;
			}

			StatEffect eff = new StatEffect(effectName, effectValue, hours);
			r.addEffect(eff);
		}

		public double getEffectValue(String statName, String effectName) {
			StatEffect eff;
			RNAGene r = rna.getGene(statName);
			if (r == null) {
				return 0.0;
			}

			eff = r.getEffect(effectName);

			if (eff == null)
				return 0;

			return eff.getValue();
		}

		public void updateEffect(String statName, String effectName, double effectValue, int hours) {
			RNAGene r = rna.getGene(statName);
			if (r == null) {
				return;
			}

			r.updateEffect(effectName, effectValue, hours);
		}

		public void updateEffect(String statName, String effectName, double effectValue, int hours, String text) {
			RNAGene r = rna.getGene(statName);
			if (r == null) {
				return;
			}

			r.updateEffect(effectName, effectValue, hours, text);
		}

		public void loadEffect(String statName, String effectFile, Double value) {
			RNAGene r = rna.getGene(statName);
			if (r == null) {
				return;
			}

			StatEffect eff = FileCache.getGamedata("effects/" + effectFile + ".effect");
			if (eff == null)
				return;

			eff.setFilename(effectFile);

			while (r.hasEffect(eff.getName())) {
				r.removeEffect(eff.getName());
			}

			eff.setHost(this);
			eff.setValue(value);
			r.addEffect(eff);
		}

		public void loadEffect(String statName, String effectFile) {
			loadEffect(statName, effectFile, 0.0);
		}

		public void removeEffect(String statName, String effectName) {
			RNAGene r = rna.getGene(statName);
			if (r == null) {
				return;
			}

			r.removeEffect(effectName);
		}

		public boolean hasEffect(String geneName, String effectName) {

			if (!this.rna.hasGene(geneName))
				return false;
			RNAGene r = getRNAGene(geneName);
			if (r == null)
				return false;

			return r.hasEffect(effectName);
		}

		public void addEffectAR(String statName, String effectName, double effectValue, double rForce) {
			RNAGene r = rna.getGene(statName);
			if (r == null) {
				return;
			}

			StatEffect eff = new StatEffect(rForce, effectName, effectValue);
			r.addEffect(eff);
		}

		public void updateEffectAR(String statName, String effectName, double effectValue, double rForce) {
			RNAGene r = rna.getGene(statName);
			if (r == null) {
				return;
			}

			r.updateEffect(rForce, effectName, effectValue);
		}

		public boolean isRNAactive(String name) {
			return getRNAGene(name).isActive();
		}

		public void setRNAactive(String name, boolean active) {
			getRNAGene(name).setActive(active);
		}

		public void setFlag(String flag, int value) {
			flags.put(flag, value);
		}

		public boolean hasFlag(String flag) {
			return flags.containsKey(flag);
		}

		public int getFlag(String flag) {
			Integer res;
			res = (Integer) flags.get(flag);

			if (res == null)
				return 0;

			return res;
		}

		public double ShameCheck(double force) {
			double res = InteractionCalc.ShameCheck(this, force);

			logger.debug("Creature \"{}\" has resulting shame level from shame check: {}", name, res);

			return res;
		}

		public void RelaxShame(double force) {
			this.addStat("generic.lewdness", force);
		}

		public void MoodChange(double val) {
			this.addStat("generic.mood", -val);
		}

		public void MoodShame(double force) {
			double slev = ShameCheck(force);

			MoodChange(slev);

			if (slev == 0) {
				force = 100 / force / 100;
			} else {
				force = force / 100;
			}
			RelaxShame(force);
		}

		public void LustShame(double lust, double shame) {

			double libido = this.getStat("generic.libido");
			double shm = this.ShameCheck(shame);
			double res = ((libido) / 10) * (lust - shm);

			this.addStat("generic.arousal", res);
			this.MoodShame(shame);
			//TODO: Figure out what this is intended to mean
			logger.debug("Character: {}; Lust Change: {}", getName(), res);
		}

		public String getOrganDescr(String organ, String param) {
			Organ o = this.getOrgan(organ);
			if (o == null) {
				return "";
			}

			return o.getDescr(param);
		}

		public String get3dPerson() {
			return TextProcessor.HimHer(this);
		}

		public String HimHer() {
			return TextProcessor.HimHer(this);
		}

		public String HisHer() {
			return TextProcessor.HisHer(this);
		}

		public String get2ndPerson() {
			return TextProcessor.HeShe(this);
		}

		public String HeShe() {
			return TextProcessor.HeShe(this);
		}

		public boolean isMale() {
			return getRNAValue("generic.sex") == 1;
		}

		public boolean isFemale() {
			return getRNAValue("generic.sex") == 2;
		}

		public boolean isFuta() {
			return getRNAValue("generic.sex") == 3;
		}

		public boolean isNeuter() {
			return getRNAValue("generic.sex") == 0;
		}

		public void setGeneMin(String gene, double value) {
			RNAGene r = rna.getGene(gene);
			r.setMinValue(value);
		}

		public void setGeneMax(String gene, double value) {
			RNAGene r = rna.getGene(gene);
			r.setMaxValue(value);
		}

		public void setGeneRChecks(String gene, boolean flag) {
			RNAGene r = rna.getGene(gene);
			r.setCheckRange(flag);
		}

		public void addItem(Item itm) {
			this.inventory.addItem(itm);
		}

		public void addItem(String name) {
			ItemProcessor.injectItemsFromFile(inventory, name, 1);
		}

		/**
		 * @param name
		 * @param count
		 */
		public void addItem(String name, int count) {
			ItemProcessor.injectItemsFromFile(inventory, name, count);
		}

		public boolean hasItem(String name) {
			return this.inventory.hasItem(name);
		}

		public void removeItem(String name) {
			int pos = this.inventory.posOfItem(name);
			if (pos == -1) {
				return;
			}
			this.inventory.removeItem(pos);
		}

		public String useItem(String name) {
			if (!this.hasItem(name))
				return "";

			String res;

			Item it = this.inventory.takeItem(this.inventory.posOfItem(name));
			res = it.consumeItem(this);

			return res;
		}

		/**
		 * @return the UID
		 */
		public String getUID() {
			return UID;
		}

		public Object getObject(String key) {
			if (objects == null) {
				objects = Collections.synchronizedMap(new HashMap());
			}
			return objects.get(key);
		}

		public Object addObject(String key, Object obj) {
			if (objects == null) {
				objects = Collections.synchronizedMap(new HashMap());
			}
			return objects.put(key, obj);
		}

		public Object removeObject(String key) {
			if (objects == null) {
				objects = Collections.synchronizedMap(new HashMap());
			}
			return objects.remove(key);
		}

		public boolean isObjectExists(String key) {
			if (objects == null) {
				objects = Collections.synchronizedMap(new HashMap());
			}
			return objects.containsKey(key);
		}

		@Override
		public int compareTo(Creature o) {
			return this.name.compareTo(o.getName());
		}

		/**
		 * @return the personalNotes
		 */
		public String getPersonalNotes() {
			return personalNotes;
		}

		/**
		 * @param personalNotes the personalNotes to set
		 */
		public void setPersonalNotes(String personalNotes) {
			this.personalNotes = personalNotes;
		}

		/**
		 * @return the nickname
		 */
		public String getNickname() {
			if (nickname == null)
				return "";

			return nickname;
		}

		/**
		 * @param nickname the nickname to set
		 */
		public void setNickname(String nickname) {
			this.nickname = nickname;
		}
	}
