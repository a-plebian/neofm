/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.bio;

import fetishmaster.bio.organs.OrganHook;
import fetishmaster.bio.organs.StateMap;
import fetishmaster.engine.scripts.ScriptEngine;
import fetishmaster.utils.Calc;

/**
 * @author H.Coder
 */
public class Foetus extends OrganHook {
	private int age;
	private int birthready;
	private DNAGenePool dna;
	private double volume = 0;
	private double growrate = 0;
	private double liveage = 0;

	public Foetus() {
		setName("foetus");
	}

	@Override
	public boolean nextHour(Creature c) {
		age++;
		volume += growrate;

		for (int i = 0; i < dna.count(); i++) {
			DNAGene g = dna.getGene(i);
			if (g.isScriptRunOnlyAsActive() && g.isActive()) {
				if (g.isRunAsDNA()) {
					String s = g.getScript();
					if (s != null && s.length() > 2)
						ScriptEngine.processCreatureScript(c, s, this);
				}
			} else if (!g.isScriptRunOnlyAsActive()) {
				if (g.isRunAsDNA()) {
					String s = g.getScript();
					if (s != null && s.length() > 2)
						ScriptEngine.processCreatureScript(c, s, this);
				}
			}

		}

		return false;
	}

	@Override
	public boolean isAlert() {
		return false;
	}

	@Override
	public StateMap getState() {
		StateMap state = new StateMap();

		state.addState("age", getAge());
		if (getLiveAge() < getAge())
			state.addState("is_live", 1);
		if (getBirthReady() < getAge())
			state.addState("is_ready", 1);

		state.addState("volume", 0); // need to recalc

		return state;
	}

	public DNAGenePool getDNA() {
		return getDna();
	}

	public void setDNA(DNAGenePool dna) {
		this.setDna(dna);
	}

	/**
	 * @return the dna
	 */
	public DNAGenePool getDna() {
		return dna;
	}

	/**
	 * @param dna the dna to set
	 */
	public void setDna(DNAGenePool dna) {
		this.dna = dna;
	}

	/**
	 * @return the volume
	 */
	@Override
	public double getVolume() {
		return volume;
	}

	/**
	 * @param volume the volume to set
	 */
	public void setVolume(double volume) {
		this.volume = volume;
	}

	/**
	 * @return the growrate
	 */
	public double getGrowrate() {
		return growrate;
	}

	/**
	 * @param growrate the growrate to set
	 */
	public void setGrowRate(double growrate) {
		this.growrate = growrate;
	}

	void setReadyAge(double value) {
		this.setBirthReady((int) value);
		this.setLiveAge(Calc.procent(value, 70));
	}

	/**
	 * @return the liveage
	 */
	public double getLiveAge() {
		return liveage;
	}

	/**
	 * @param liveage the liveage to set
	 */
	public void setLiveAge(double liveage) {
		this.liveage = liveage;
	}

	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}

	/**
	 * @return the birthReady
	 */
	public int getBirthReady() {
		return birthready;
	}

	/**
	 * @param birthReady the birthReady to set
	 */
	public void setBirthReady(int birthReady) {
		this.birthready = birthReady;
	}

}
