/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.bio;

import fetishmaster.utils.ICopiable;

import java.util.*;
import java.util.regex.Pattern;

/**
 * @author H.Coder
 */
public class DNAGroupDB implements ICopiable<DNAGroupDB> {
	private final Map<String, Map<String, String>> db = new LinkedHashMap<>();

	@Override
	public DNAGroupDB copy() {
		var out = new DNAGroupDB();

		db.forEach((key, map) -> out.db.put(key, new LinkedHashMap<>(map)));

		return null;
	}

	public void addGroup(String key) {
		db.put(key, new LinkedHashMap<>());
	}

	public void addGeneName(String group, String gene) {
		var groupDB = db.putIfAbsent(group, new LinkedHashMap<>());

		groupDB.put(gene, gene);
	}

	public boolean isGeneNamePresent(String gene) {
		for (Map<String, String> entry : db.values()) {
			if (entry.containsKey(gene)) {
				return true;
			}
		}

		return false;
	}

	public boolean isMemberOfGroup(String group, String gene) {
		Map<String, String> l = db.get(group);
		if (l != null) {
			return l.containsKey(gene);
		}

		return false;
	}

	public void addToDB(DNAGenePool dna) {
		for(DNAGene gene : dna.pool.values()) {
			if(gene.getOrganName().equals("group")) {
				addGroup(gene.getStatName());
				for(String name : parseTextForNames(gene.getTextValue())) {
					addGeneName(gene.getStatName(), name);
				}
			}
		}
	}

	private List<String> parseTextForNames(String t) {
		return Arrays.asList(t.split("&"));
	}

	public Set<String> getGroup(String group) {
		return db.get(group).keySet();
	}

	public Set<String> getGroups() {
		return db.keySet();
	}

	public int getGroupsCount() {
		return db.size();
	}

}
