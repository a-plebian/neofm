/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.bio;


import fetishmaster.utils.ICopiable;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author H.Coder
 */
public class DNAGroup implements ICopiable<DNAGroup> {

	DNAGenePool parent1;
	DNAGenePool parent2;
	DNAGenePool td1, td2;
	String groupName;
	private DNAGroupDB db;
	private DNAGene g1, g2;

	@Override
	public DNAGroup copy() {
		var out = new DNAGroup();

		out.parent1 = parent1.copy();
		out.parent2 = parent2.copy();
		out.td1 = td1.copy();
		out.td2 = td2.copy();
		out.groupName = groupName;
		out.db = db.copy();
		out.g1 = g1.copy();
		out.g2 = g2.copy();

		return out;
	}

	private void newGroupGenes() {
		DNAGene g = new DNAGene();
		g.setGeneForce(0);
		g.setSexTraits(0);
	}

	public void setParents(DNAGenePool p1, DNAGenePool p2) {
		db = new DNAGroupDB();
		this.parent1 = p1;
		this.parent2 = p2;
		db.addToDB(p1);
		db.addToDB(p2);
	}

	private void calcGene(DNAGene g, DNAGenePool dna) {
		for (int i = 0; i < dna.count(); i++) {
			DNAGene tg = dna.getGene(i);
			if (db.isGeneNamePresent(tg.getFCName())) {
				g.setGeneForce(g.getGeneForce() + tg.getGeneForce());
				g.setSexTraits(g.getSexTraits() + tg.getSexTraits());
			}
		}
	}

	private void calcGenes() {
		g1 = new DNAGene();
		g2 = new DNAGene();
		calcGene(g1, td1);
		calcGene(g2, td2);
	}

	public int groupCount() {
		return db.getGroupsCount();
	}

	private void setupCalcForGroup(String group) {
		int i;
		Set<String> l = db.getGroup(group);
		DNAGene d1;
		DNAGene d2;
		td1 = new DNAGenePool();
		td2 = new DNAGenePool();
		for (String string : l) {
			d1 = parent1.getGene(string);
			d2 = parent2.getGene(string);

			if (d1 != null)
				td1.addGene(d1);
			if (d2 != null)
				td2.addGene(d2);
		}
	}

	public Set<String> getGroups() {
		return db.getGroups();
	}

	public DNAGenePool getGroupGenome(String group) {

		DNAGenePool dna = null;

		setupCalcForGroup(group);
		calcGenes();

		DNAGene g = GeneProcessor.SelectGeneFromForce(g1, g2, GeneProcessor.sexTraitsBalance(g1, g2));
		if (g == g1) {
			dna = selectGroupGenes(group, td1);
		} else if (g == g2) {
			dna = selectGroupGenes(group, td2);
		}
		return dna;
	}

	private DNAGenePool selectGroupGenes(String group, DNAGenePool dna) {
		DNAGenePool d = new DNAGenePool();
		Set<String> l = db.getGroup(group);
		DNAGene td;

		for (String entry : l) {
			td = dna.getGene(entry);
			if (td != null) {
				d.addGene(td.copy());
			}
		}

		return d;
	}

	public boolean isGroupedGene(String name) {
		return db.isGeneNamePresent(name);
	}

	public boolean isGroupedGene(DNAGene g) {
		if (g == null) {
			return false;
		}
		return isGroupedGene(g.getFCName());
	}


}
