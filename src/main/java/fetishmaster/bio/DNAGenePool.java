/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.bio;

import fetishmaster.utils.ICopiable;

import java.io.Serializable;
import java.util.*;


/**
 * @author H.Coder
 */
public class DNAGenePool implements Serializable, ICopiable<DNAGenePool> {
	//protected LinkedHashMap pool = new LinkedHashMap();
	protected Map<String, DNAGene> pool = Collections.synchronizedMap(new LinkedHashMap<>());

	@Override
	public DNAGenePool copy() {
		var out = new DNAGenePool();

		pool.forEach((string, gene) -> out.pool.put(string, gene.copy()));

		return out;
	}

	public void addGene(DNAGene gen) {

		pool.put(gen.getFCName(), gen);

	}

	public void mergeGenes(DNAGenePool genes) {
		int i;
		DNAGene g;
		for (i = 0; i < genes.count(); i++) {
			g = genes.getGene(i);
			pool.put(g.getFCName(), g);
		}
	}

	public int count() {
		return pool.size();
	}

	public DNAGene getGene(String FCName) {
		if (pool.containsKey(FCName))
			return pool.get(FCName);

		return null;
	}

	public void removeGene(String FCName) {
		pool.remove(FCName);
	}


	public DNAGene getGene(int number) {
		if (number > pool.size())
			return null;

		Set s = pool.keySet();
		Iterator it = s.iterator();
		String str = "";

		DNAGene g = null;
		int i = 0;
		while ((i <= number) && it.hasNext()) {
			str = (String) it.next();
			i++;
		}

		g = (DNAGene) pool.get(str);
		return g;
	}

	public void removeGene(int number) {
		if (number > pool.size())
			return;

		Set s = pool.keySet();
		Iterator it = s.iterator();
		String str = "";

		DNAGene g = null;
		int i = 0;
		while ((i <= number) && it.hasNext()) {
			str = (String) it.next();
			i++;
		}
		pool.remove(str);
	}

	public boolean hasGene(String FCName) {
		return pool.containsKey(FCName);
	}

	public void updateKeys() {
		Map<String, DNAGene> gp = Collections.synchronizedMap(new LinkedHashMap<>());
		DNAGene g;

		while (!pool.isEmpty()) {
			g = getGene(0);
			gp.put(g.getFCName(), g);
			removeGene(0);
		}
		pool = gp;
	}

	public double getGeneValue(String name) {
		DNAGene g = this.getGene(name);
		if (g == null)
			return 0;

		return g.getValue();
	}

	private void clearAll() {
		this.pool.clear();
	}

	public void organize() {
		TreeMap<String, DNAGene> temp = new TreeMap<>(pool);
		this.pool.clear();
		this.pool.putAll(temp);
	}
}
