/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.bio;

import fetishmaster.utils.ICopiable;

import java.util.ArrayList;
import java.util.List;

/**
 * @author H.Coder
 */
public class TemplateDNA implements ICopiable<TemplateDNA> {
	private final List<String> pools = new ArrayList<>();

	private String name;

	@Override
	public TemplateDNA copy() {
		var out = new TemplateDNA();

		out.name = name;

		out.pools.addAll(pools);

		return out;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	public String getPool(int index) {
		return pools.get(index);

	}

	public void addPool(String name) {
		pools.add(name);
	}

	public void removePool(int index) {
		if (index > pools.size())
			return;
		pools.remove(index);
	}

	public void updatePool(String pool, int index) {
		if (index > pools.size())
			return;

		pools.set(index, pool);
	}

	public int count() {
		return pools.size();
	}
}
