/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.bio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

/**
 * @author H.Coder
 */
public class RNAGenePool extends DNAGenePool implements Serializable {

	public RNAGenePool() {
		super();
	}

	public RNAGenePool(DNAGenePool dnaGenePool) {
		dnaGenePool.pool.forEach((key, gene) -> {
			pool.put(key, gene.copy());
		});
	}

	@Override
	public DNAGenePool copy() {
		return new RNAGenePool(super.copy());
	}

	public void nextHour(Creature c, boolean timeSkipping) {
//        synchronized (pool) {
		Set rna = pool.keySet();
		RNAGene r;
		ArrayList genes = new ArrayList();

		Iterator it = rna.iterator();
		while (it.hasNext()) {
			r = getGene(it.next().toString());
			genes.add(r);
		}

		// this needed to avoid concurent modification in the iterator in some cases.
		for (int i = 0; i < genes.size(); i++) {
			r = (RNAGene) genes.get(i);
			r.nextHour(c, timeSkipping);
		}
//        }

	}

	public void addHours(int hours, Creature c) {
		//      synchronized (pool) {
		RNAGene r;

		Set rna = pool.keySet();
		Iterator it = rna.iterator();

		while (it.hasNext()) {
			r = getGene(it.next().toString());
			r.addAge(hours, c);
		}
//        }

	}

	@Override
	public RNAGene getGene(String FCName) {
		RNAGene r = (RNAGene) super.getGene(FCName);
		if (r == null) {
			String[] s = FCName.split("\\.");
			r = new RNAGene();
			r.setOrganName(s[0]);
			r.setStatName(s[1]);
			addGene(r); //Do we really need new gene in rna on empty request?
//            if (GameEngine.devMode)
//                System.out.println("Not found rna gene "+r.getFCName()+" adding...");
		}

		return r;
	}

	@Override
	public double getGeneValue(String name) {
		RNAGene g = this.getGene(name);
		if (g == null)
			return 0;

		return g.getValue();
	}


}
