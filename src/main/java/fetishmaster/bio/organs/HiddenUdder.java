/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.bio.organs;

import fetishmaster.bio.BodyResponse;
import fetishmaster.bio.Creature;
import fetishmaster.components.GameClock;
import fetishmaster.engine.GameEngine;
import fetishmaster.engine.scripts.Status;
import fetishmaster.utils.Calc;
import fetishmaster.utils.GeometryCalc;

import java.util.Map;

/**
 * @author H.Coder
 */
public class HiddenUdder extends Organ {

	public HiddenUdder() {
		super();
		this.name = "hidden_udder";
	}

	@Override
	public boolean nextHour() {
		super.nextHour();
		Creature self = host;

		if (self.isRNAactive("breasts.lact_rate")) {
			self.doAction("udder_lactate");
		} else {
			self.setStat("udder.milk", 0);
			self.removeEffect("udder.size", "engorged");
		}

		self.doAction("udder_recalc");

		return false;
	}

	@Override
	public OrganResponse doAction(String action, Map agrs) {
		OrganResponse res = new OrganResponse();

		Creature self = host;
		GameClock clock = GameEngine.activeWorld.clock;
		GeometryCalc geometry = new GeometryCalc();
		Status status = new Status();

		if (action.equals("orgasm")) {
			if (self.hasEffect("udder.size", "engorged")) {
				double m = self.getStat("udder.milk");
				double e = Calc.procent(m, 10);
				res.put("milk", e);
				self.subStat("udder.milk", e);
			}

		}

		if (action.equals("milking")) {
			BodyResponse rp = self.doAction("udder_milking");

			res.put("milk", rp.get("udder", "milk"));
			res.put("milk_volume", rp.get("udder", "milk"));
		}

		if (action.equals("udder_lactate")) {
			if (self.isRNAactive("breasts.lact_rate")) {
				self.addStat("udder.milk", self.getStat("breasts.lact_rate"));
			}

			self.doAction("udder_balancemilk");

			self.doAction("udder_engorgement");

			self.doAction("udder_excesscheck");

			self.doAction("udder_growcheck");

			self.doAction("udder_dryup");

		}

		if (action.equals("udder_growcheck")) {
			if (self.hasEffect("udder.size", "engorged") && Calc.chance(5)) {
				double cursize = self.getCleanRNAValue("udder.max_size");
				double psize = Calc.procent(cursize, 5);
				self.addStat("udder.max_size", psize);
				self.addHistory("Udder", "Udder grow a little, to hold better all this milk...");
			}

		}

		if (action.equals("udder_engorgement")) {
			if (self.hasEffect("udder.size", "engorged")) {
				if (Calc.procent(self.getCleanRNAValue("udder.maxvol"), 50) > self.getStat("udder.milk")) {
					self.removeEffect("udder.size", "engorged");
				}
			}

			if (self.getCleanRNAValue("udder.maxvol") < self.getStat("udder.milk") && self.hasEffect("udder.size", "engorged") == false) {
				double addsize = Calc.procent(self.getCleanRNAValue("udder.size"), 20);
				self.updateEffect("udder.size", "engorged", addsize);
				self.addHistory("Lactating", "Udder engorged with milk");
			}
		}

		if (action.equals("udder_recalc")) {
			// udder swell if lactating.
			if ((self.isRNAactive("breasts.lact_rate")) && (self.getStat("udder.size") < self.getStat("udder.max_size")) && self.hasEffect("udder.size", "engorged")) {
				self.addStat("udder.size", Calc.percent(self.getStat("udder.max_size"), 5));
			}

// udder retract if not lactating.
			if ((self.isRNAactive("breasts.lact_rate") == false) && (self.getStat("udder.size") > 0)) {
				self.removeEffect("udder.size", "engorged");
				self.subStat("udder.size", Calc.percent(self.getStat("udder.max_size"), 5));
			}

			self.setStat("udder.weight", self.getCleanRNAValue("udder.size") * 1.5);
			self.updateEffect("udder.weight", "milk", self.getStat("udder.milk"));

//recalc udder volume;
			self.setStat("udder.maxvol", GeometryCalc.SphereDiameterToMillilites(10 + self.getStat("udder.size")) / 2);

			self.updateEffect("generic.weight", "udder", self.getStat("udder.weight"));

//penalties
			double str = self.getStat("generic.str");
			double pen = self.getStat("udder.weight");

			self.updateEffect("generic.dex", "udder_size", 0 - pen / (str * 10));
			self.updateEffect("generic.spd", "udder_size", 0 - pen / (str * 10));
		}

		if (action.equals("udder_excesschek")) {
			double excess = self.getStat("udder.milk") - self.getStat("udder.maxvol");
			if (excess > 0) {
				self.setStat("udder.milk", self.getStat("udder.maxvol"));
				self.addHistory("Milk", "Milk leaked from the udder");
			}

		}

		if (action.equals("udder_milking")) {
			double milk = self.getStat("udder.milk");

			res.put("milk", milk);
			res.put("milk_volume", milk);

			self.setStat("udder.milk", 0);
		}

		if (action.equals("udder_balancemilk")) {
			double sv = Calc.percent(self.getStat("breasts.max_volume"), 50);

//if breasts is full and uddert is not
			if (self.getStat("breasts.milk_volume") > Calc.percent(self.getStat("breasts.max_volume"), 60) && self.getStat("udder.milk") < self.getStat("udder.maxvol") - sv - 10) {
				self.subStat("breasts.milk_volume", sv);
				self.addStat("udder.milk", sv);
			}

//reverse, if breasts is empty and udder is not
			if (self.getStat("breasts.milk_volume") < Calc.percent(self.getStat("breasts.max_volume"), 10) && self.getStat("udder.milk") > sv - 10) {
				self.addStat("breasts.milk_volume", sv);
				self.subStat("udder.milk", sv);
			}
		}

		return res;
	}
}
