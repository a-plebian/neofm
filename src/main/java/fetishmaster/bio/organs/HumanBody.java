/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.bio.organs;

import fetishmaster.bio.Creature;

import java.util.Map;

/**
 * @author H.Coder
 */
public class HumanBody extends Organ {

	public HumanBody() {
		super();
		this.name = "human_body";
	}

	@Override
	public boolean nextHour() {
		super.nextHour();
		Creature self = host;
//recalcualting base weight.
		double chest = self.getStat("generic.chest");
		double height = self.getStat("generic.height");
		double weight = height * chest / 240; // based on Borngardt formula
		self.setStat("generic.weight", weight * 1000); // formula result in kg, game need gramms.

//fat in hips
		double fat_vol = self.getStat("generic.fat") / 100 * self.getStat("fat.hips");
		self.setStat("hips.fat", fat_vol);
//self.updateEffect("abdomen.volume", "fat", fat_vol);
		self.updateEffect("generic.fat_in_organs", "hips", fat_vol, 2);
		self.updateEffect("generic.hips", "hipfat", (self.getStat("hips.fat") / 1150));

//recalculating fat percent
		double fwg = self.getStat("generic.weight");
		double fat = self.getStat("generic.fat") - self.getStat("generic.fat_in_organs");
		double fat_percent = 100.0 / (fwg / (fat * 0.9));
		self.setStat("generic.fat_percent", fat_percent);

		return false;
	}

	@Override
	public OrganResponse doAction(String action, Map agrs) {
		OrganResponse res = new OrganResponse();

		return res;
	}

}

