/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.bio.organs;

import fetishmaster.utils.ICopiable;

import java.util.HashMap;
import java.util.Map;

/**
 * @author H.Coder
 */
public class OrganResponse implements ICopiable<OrganResponse> {
	//TODO: use some kind of union type or something here
	private final Map<String, Object> results = new HashMap<>();

	@Override
	public OrganResponse copy() {
		var out = new OrganResponse();

		out.results.putAll(results);

		return out;
	}

	public void put(String key, int value) {
		results.put(key, (double) value);
	}

	public void put(String key, double value) {
		results.put(key, value);
	}

	public void put(String key, Object value) {
		results.put(key, value);
	}

	public double get(String key) {
		if (results.containsKey(key)) {
			return (Double) results.get(key);
		}

		return 0;
	}

	public String getText(String key) {
		if (results.containsKey(key)) {
			return (String) results.get(key);
		}

		return "";
	}
}
