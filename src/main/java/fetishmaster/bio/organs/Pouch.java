/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.bio.organs;

import fetishmaster.bio.Creature;
import fetishmaster.components.GameClock;
import fetishmaster.engine.GameEngine;
import fetishmaster.engine.scripts.Status;
import fetishmaster.utils.Calc;
import fetishmaster.utils.GeometryCalc;

import java.util.Map;

/**
 * @author H.Coder
 */
public class Pouch extends Organ {

	public Pouch() {
		super();
		this.name = "pouch";
	}

	@Override
	public boolean nextHour() {
		super.nextHour();
		Creature self = host;

		self.doAction("pouch_recalc");

		return false;
	}

	@Override
	public OrganResponse doAction(String action, Map agrs) {
		OrganResponse res = new OrganResponse();

		Creature self = host;
		GameClock clock = GameEngine.activeWorld.clock;
		GeometryCalc geometry = new GeometryCalc();
		Status status = new Status();

		if (action.equals("pouch_recalc")) {
			//sperm calc
			double cumvol = self.getStat("pouch.sperm_volume");
			double maxvol = self.getStat("pouch.max_volume");

//excess sperm is lost
			if (cumvol > maxvol) {
				self.setStat("pouch.sperm_volume", maxvol);
				cumvol = maxvol;
			}

//calc size and weight;

			self.updateEffect("pouch.volume", "sperm", cumvol); //sperm as part volume

			double volume = self.getStat("pouch.volume"); //full volume is...

			double weight = volume + 70; //weight of pouch is...

			double size = GeometryCalc.MillilitersToSphereDiameter(volume + 50); //size is...

			self.setStat("pouch.size", size);
			self.setStat("pouch.weight", weight);

			self.updateEffect("abdomen.weight", "pouch", weight); //adding weight to abdomen.
			self.updateEffect("abdomen.volume", "pouch", volume / 2 + 50); // adding volume to abdomen.

		}

		return res;
	}
}
