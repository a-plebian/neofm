/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.bio.organs;

import fetishmaster.bio.Creature;
import fetishmaster.bio.DNAGenePool;
import fetishmaster.bio.RNAGenePool;
import fetishmaster.bio.Semen;

import java.util.HashMap;

/**
 * @author H.Coder
 */
public class Ova extends OrganHook {

	private final HashMap semenlist = new HashMap();
	private double fertility = 100;
	private double age = 0;
	private double maxage = 72;
	private DNAGenePool dna;
	private RNAGenePool rna;

	public Ova() {
		setName("ova");
	}

	@Override
	public boolean nextHour(Creature c) {
		setAge(getAge() + 1);
		if (getAge() > getMaxage())
			unHook();
		return false;
	}

	@Override
	public boolean isAlert() {
		return false;
	}

	@Override
	public StateMap getState() {
		StateMap state = new StateMap();

		state.addState("age", getAge());
		state.addState("fertility", getFertility() - getAge());

		return state;
	}

	/**
	 * @return the fertility
	 */
	public double getFertility() {
		return fertility;
	}

	/**
	 * @param fertility the fertility to set
	 */
	public void setFertility(double fertility) {
		this.fertility = fertility;
	}

	/**
	 * @return the age
	 */
	public double getAge() {
		return age;
	}

	/**
	 * @param age the age to set
	 */
	public void setAge(double age) {
		this.age = age;
	}

	/**
	 * @return the maxage
	 */
	public double getMaxage() {
		return maxage;
	}

	/**
	 * @param maxage the maxage to set
	 */
	public void setMaxage(double maxage) {
		this.maxage = maxage;
	}

	/**
	 * @return the dna
	 */
	public DNAGenePool getDna() {
		return dna;
	}

	/**
	 * @param dna the dna to set
	 */
	public void setDna(DNAGenePool dna) {
		this.dna = dna;
	}

	public void addToSemenList(Semen s) {
		semenlist.put(s, s);
	}

	public boolean isSemenUsed(Semen s) {
		return semenlist.containsKey(s);
	}

	/**
	 * @return the rna
	 */
	public RNAGenePool getRna() {
		return rna;
	}

	/**
	 * @param rna the rna to set
	 */
	public void setRna(RNAGenePool rna) {
		this.rna = rna;
	}
}
