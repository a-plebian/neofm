/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.bio.organs;

import fetishmaster.bio.Creature;

/**
 * @author H.Coder
 */
public abstract class OrganHook {
	private Organ hooked;
	private String name = "";

	abstract public boolean nextHour(Creature c);

	abstract public boolean isAlert();

	abstract public StateMap getState();

	public int getHookNumber(Organ o) {
		int res = -1;
		int i;
		for (i = 0; i < o.hooksCount(); i++) {
			if (o.getHook(i).equals(this))
				res = i;
		}

		return res;
	}

	/**
	 * @return the hooked
	 */
	public Organ getHooked() {
		return hooked;
	}

	/**
	 * @param hooked the hooked to set
	 */
	public void setHooked(Organ hooked) {
		this.hooked = hooked;
	}

	public void unHook() {
		hooked.removeHookedObject(this);
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	public double getVolume() {
		return 0;
	}

	public double getWeight() {
		return getVolume();
	}
}
