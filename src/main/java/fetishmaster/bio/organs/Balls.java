/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.bio.organs;

import fetishmaster.bio.Creature;
import fetishmaster.components.GameClock;
import fetishmaster.engine.GameEngine;
import fetishmaster.utils.GeometryCalc;

import java.util.Map;

/**
 * @author H.Coder
 */
public class Balls extends Organ {

	public Balls() {
		super();
		this.name = "balls";
	}


	@Override
	public boolean nextHour() {
		super.nextHour();
		Creature self = this.host;

		self.doAction("balls_production");

		self.doAction("balls_recalc");


		return false;
	}

	@Override
	public OrganResponse doAction(String action, Map agrs) {
		OrganResponse res = new OrganResponse();

		Creature self = host;
		GameClock clock = GameEngine.activeWorld.clock;

		if (action.equals("orgasm")) {
			self.setStat("balls.last_orgasm", clock.getAHours());
		}

		if (action.equals("balls_production")) {
			self.addStat("balls.sperm_volume", self.getStat("balls.prod_rate"));

			double excess = 0;

			if (self.getStat("balls.max_volume") < self.getStat("balls.sperm_volume")) {
				excess = self.getStat("balls.sperm_volume") - self.getStat("balls.max_volume");
				self.subStat("balls.sperm_volume", excess);
			}

			if (self.hasOrgan("pouch") == true) {
				self.addStat("pouch.sperm_volume", excess);
			}

		}

		if (action.equals("balls_recalc")) {
			double x = GeometryCalc.SphereDiameterToMillilites(self.getCleanStat("balls.size") - 3);

			x = x + 10;

			self.setStat("balls.max_volume", x);

			self.setStat("balls.hours_from_orgasm", clock.getAHours() - self.getStat("balls.last_orgasm"));

			// maybe need fix later
			if (self.hasOrgan("pouch") == false) {
				double ballsfilling = self.getStat("balls.sperm_volume") / self.getStat("balls.max_volume");
				self.updateEffect("balls.size", "balls_filling", ballsfilling * self.getCleanStat("balls.size"));
			}
		}
		return res;
	}
}
