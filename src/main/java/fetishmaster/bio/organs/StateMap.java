/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.bio.organs;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author H.Coder
 */
public class StateMap {
	private final Map<String, Double> states = new LinkedHashMap<>();

	public void addState(String key, double val) {
		states.put(key, val);
	}

	public double getState(String key) {
		if (states.containsKey(key))
			return states.get(key);

		return 0;
	}

}
