/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.bio.organs;

import fetishmaster.bio.Creature;
import fetishmaster.bio.RNAGene;
import fetishmaster.engine.GameEngine;
import fetishmaster.engine.TextProcessor;

import java.util.*;

/**
 * @author H.Coder
 */

//generic class for the organ. Actual organs 
public class Organ {
	protected Creature host;
	protected String name;
	private List<OrganHook> hooked = Collections.synchronizedList(new ArrayList<>());
	private Map stats = Collections.synchronizedMap(new HashMap());

	public Organ(Creature hostCreature) {
		this.host = hostCreature;
	}

	public Organ() {
		this.host = null;
	}

	public void onAdd(Creature host) {
		setParent(host);

	}

	public void onRemove() {

	}

	public boolean nextHour() {
		if (getHooked() == null)
			setHooked(Collections.synchronizedList(new ArrayList<OrganHook>()));

		int i;
		for (i = 0; i < getHooked().size(); i++) {
			getHooked().get(i).nextHour(host);
		}

		return false;
	}

	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	public Creature getParent() {
		return getHost();
	}

	public void setParent(Creature host) {
		this.setHost(host);
	}

	/**
	 * @return the host
	 */
	public Creature getHost() {
		return host;
	}

	/**
	 * @param host the host to set
	 */
	public void setHost(Creature host) {
		this.host = host;
	}

	public OrganResponse doAction(String action) {
		return doAction(action, new HashMap());
	}

	public OrganResponse doAction(String action, Map agrs) {
		if (action.equals("test")) {
			if (GameEngine.devMode)
				System.out.println(name + " testing");
		}
		return new OrganResponse();
	}

	//getting gene, but only with this organ first name part.
	protected RNAGene getGene(String name) {
		if (host == null)
			return null;

		RNAGene g = host.getRNAGene(this.name + "." + name);

		if (g == null) {
			g = new RNAGene();
			g.setActive(false);
			g.setOrganName(this.name);
			g.setStatName(name);
		}
		host.getRna().addGene(g);

		return g;
	}

	protected RNAGene getGene(String organName, String geneName) {
		if (host == null)
			return null;

		RNAGene g = host.getRNAGene(organName + "." + geneName);

		if (g == null) {
			g = new RNAGene();
			g.setActive(false);
			g.setOrganName(organName);
			g.setStatName(geneName);
		}
		host.getRna().addGene(g);

		return g;
	}

	public String getDescr(String param) {
		RNAGene r = getGene(param);
		double value = r.getValue();
		String res = TextProcessor.getDescr(this.name + param, value);

		return res;
	}

	@Override
	public String toString() {
		return getName();
	}

	public void addHookedObject(OrganHook oh) {
		if (getHooked() == null)
			setHooked(Collections.synchronizedList(new ArrayList<>()));
		this.getHooked().add(oh);
		oh.setHooked(this);
	}

	public void removeHookedObject(OrganHook oh) {
		if (getHooked() == null)
			setHooked(Collections.synchronizedList(new ArrayList<>()));
		this.getHooked().remove(oh);
		oh.setHooked(null);
	}

	public void removeHookedObject(int i) {
		if (getHooked() == null)
			setHooked(Collections.synchronizedList(new ArrayList<>()));
		OrganHook oh = getHooked().get(i);
		this.getHooked().remove(i);
		if (oh != null)
			oh.setHooked(null);

	}

	public OrganHook getHook(int i) {
		if (getHooked() == null)
			setHooked(Collections.synchronizedList(new ArrayList<>()));
		return this.getHooked().get(i);
	}

	public int hooksCount() {
		if (getHooked() == null)
			setHooked(Collections.synchronizedList(new ArrayList<>()));
		return getHooked().size();
	}

	public List<OrganHook> selectHooksByName(String name) {
		return getHooked().stream()
				.filter(hook -> hook.getName().equals(name))
				.toList();
	}

	/**
	 * @return the hooked
	 */
	public List<OrganHook> getHooked() {
		return hooked;
	}

	/**
	 * @param hooked the hooked to set
	 */
	public void setHooked(List<OrganHook> hooked) {
		this.hooked = hooked;
	}

	/**
	 * @return the stats
	 */
	public Map getStats() {
		return stats;
	}

	/**
	 * @param stats the stats to set
	 */
	public void setStats(Map stats) {
		this.stats = stats;
	}
}
