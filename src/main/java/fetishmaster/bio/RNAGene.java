/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.bio;

import fetishmaster.components.StatEffect;
import fetishmaster.engine.WalkEngine;
import fetishmaster.engine.scripts.ScriptEngine;
import fetishmaster.utils.Calc;
import fetishmaster.utils.LinkedMapList;

import java.io.Serializable;
import java.util.*;

/**
 * @author H.Coder
 */

//RNA is used as real stat of the creture.
public class RNAGene extends DNAGene implements Serializable {

	private LinkedMapList<String, StatEffect> effects = new LinkedMapList<>();
	private double naturalValue;

	public RNAGene() { super(); }

	public RNAGene(DNAGene dnaGene) {
		organname = dnaGene.organname;
		statname = dnaGene.statname;
		value = dnaGene.value;
		changerate = dnaGene.changerate;
		pubertyage = dnaGene.pubertyage;
		maturetime = dnaGene.maturetime;
		mutationrate = dnaGene.mutationrate;
		oneTimeMutation = dnaGene.oneTimeMutation;
		active = dnaGene.active;
		geneForce = dnaGene.geneForce;
		sexTraits = dnaGene.sexTraits;
		script = dnaGene.script;
		passOnFastTime = dnaGene.passOnFastTime;
		runAsDNA = dnaGene.runAsDNA;
		runAsRNA = dnaGene.runAsRNA;
		checkRange = dnaGene.checkRange;
		minValue = dnaGene.minValue;
		maxValue = dnaGene.maxValue;
		returnToNatural = dnaGene.returnToNatural;
		backforce = dnaGene.backforce;
		backforceRangeMult = dnaGene.backforceRangeMult;
		chrate = dnaGene.chrate;
		setTextValue(dnaGene.getTextValue());
		setScriptRunOnlyAsActive(dnaGene.isScriptRunOnlyAsActive());
	}

	@Override
	public DNAGene copy() {
		var out = new RNAGene(super.copy());

		out.effects = effects.copy();
		out.naturalValue = naturalValue;

		return out;
	}

	public void addAge(int hours, Creature c) {
		if (!active)
			return;
		//if grow time in past then just exit
		if (pubertyage + maturetime < c.getAge())
			return;

		int stopgrow = (int) (pubertyage + maturetime);
		int beforepub;
		int excess;
		int leftage;
		int chours;

		beforepub = (int) (pubertyage - c.getAge());
		if (beforepub < 0)
			beforepub = 0;

		leftage = stopgrow - c.getAge() - beforepub;
		if (leftage < 0)
			leftage = 0;

		excess = (c.getAge() + hours) - stopgrow;
		if (excess < 0)
			excess = 0;

		chours = hours - excess - beforepub;
		if (chours > leftage)
			chours = leftage;

		if (chours < 0)
			chours = 0;

		//how match value need to be added?
		double add;
		//if(statname.equals("max_lactation"))
//        {
//            add = Calc.debugPlusMinusXProcent(chrate*chours, mutationrate);
//            System.out.println(organname+"."+statname+" adding: "+add+ "  chrate:"+chrate+"  chours:"+chours+"  mrate:"+mutationrate);
//        }
//        else
		add = Calc.PlusMinusXProcent(chrate * chours, mutationrate);


		if (add > 0) {
			value += add;
			setNaturalValue(getNaturalValue() + add);
		}
	}

	public void nextHour(Creature c, boolean timeSkipping) {
		//nextHour(timeSkipping);
		List<StatEffect> expired = new ArrayList<>();

		effects.values().forEach(effect -> {
			effect.nextHour();
			if (effect.isExpired()) {
				expired.add(effect);
			}
		});

		String ref;
		for (StatEffect effect : expired) {
			ref = removeEffect(effect);
			if (ref != null && !ref.isEmpty()) {
				WalkEngine.addWalkText(ref);
				c.addHistory("Effect", ref);
			}
		}

		//return to base without training mechanic
		if (returnToNatural) {
			if (value > getNaturalValue())
				value -= backforce * (Math.abs(value - getNaturalValue()) * backforceRangeMult);
			else if (value < getNaturalValue())
				value += backforce * (Math.abs(value - getNaturalValue()) * backforceRangeMult);
		}

		//newer part;

		//if (isPassOnFastTime())
		//    return;

		if ((c.getAge() >= pubertyage) && (c.getAge() < (pubertyage + maturetime))) {
			if (active) {
				value += chrate;
				setNaturalValue(getNaturalValue() + chrate);
			}
			if (script != null && runAsRNA && script.length() > 2) {
				if (isScriptRunOnlyAsActive() && isActive()) {
					ScriptEngine.processCreatureScript(c, script, this);
				}
				if (!isScriptRunOnlyAsActive()) {
					ScriptEngine.processCreatureScript(c, script, this);
				}

			}
		}

	}

	//Effects managment
	public void addEffect(StatEffect effect) {
		effects.put(effect.getName(), effect);
	}

	public StatEffect getEffect(String name) {
		if (effects.containsKey(name)) {
			return effects.get(name);
		}

		return null;

	}

	public String removeEffect(String name) {
		String res = null;
		StatEffect e;
		if (effects.containsKey(name)) {
			e = effects.get(name);
			effects.remove(name);
			if (!effects.containsKey(name))
				res = e.getEndText();
		}

		return res;
	}

	public String removeEffect(StatEffect eff) {
		return removeEffect(eff.getName());
	}

	public boolean hasEffect(String name) {
		return effects.containsKey(name);
	}

	public int effectCount() {
		return effects.size();
	}

	public void updateEffect(String effect, double newValue) {
		if (hasEffect(effect)) {
			getEffect(effect).setValue(newValue);
			return;
		}
		addEffect(new StatEffect(effect, newValue));
	}

	public void updateEffect(double rForce, String effect, double newValue) {
		if (hasEffect(effect)) {
			getEffect(effect).setValue(newValue);
			getEffect(effect).setRForce(rForce);
			return;
		}
		addEffect(new StatEffect(rForce, effect, newValue));
	}

	public void updateEffect(String effect, double newValue, int newTimer) {

		if (hasEffect(effect)) {
			getEffect(effect).setValue(newValue);
			getEffect(effect).setTimer(newTimer);
		} else {
			addEffect(new StatEffect(effect, newValue, newTimer));
		}
	}

	public void updateEffect(String effect, double newValue, int newTimer, String text) {

		if (hasEffect(effect)) {
			getEffect(effect).setValue(newValue);
			getEffect(effect).setTimer(newTimer);
			getEffect(effect).setEndText(text);
		} else {
			addEffect(new StatEffect(effect, newValue, newTimer, text));
		}
	}

	public void updateEffectTimer(String effect, int newTimer) {
		if (hasEffect(effect)) {
			StatEffect st = getEffect(effect);
			st.addTimer(newTimer);
		}

	}

	public StatEffect getEffect(int number) {
		return effects.getAt(number);
	}

	@Override
	public double getValue() {
		double effs = 0;
		Collection<String> e = effects.keySet();
		Iterator<String> it = e.iterator();
		StatEffect ef;

		while (it.hasNext()) {
			ef = effects.get(it.next());
			effs += ef.getValue();
		}

		return value + effs;
	}

	@Override
	public void setValue(double val) {
		value = val;
//        if (returnToNatural)
//        {
//            naturalValue = val;
//        }

	}

	public double getCleanValue() {
		return value;
	}

	/**
	 * @return the naturalValue
	 */
	public double getNaturalValue() {
		return naturalValue;
	}

	/**
	 * @param naturalValue the naturalValue to set
	 */
	public void setNaturalValue(double naturalValue) {
		this.naturalValue = naturalValue;
	}

	@Override
	public RNAGene clone() {
		RNAGene rnaGene = new RNAGene();

		rnaGene.setActive(active);
		rnaGene.setBackforce(backforce);
		rnaGene.setBackforceRangeMult(backforceRangeMult);
		rnaGene.setChangeRate(changerate);
		rnaGene.setCheckRange(checkRange);
		rnaGene.setData(getData());
		rnaGene.setGeneForce(geneForce);
		rnaGene.setMatureTime(maturetime);
		rnaGene.setMaxValue(maxValue);
		rnaGene.setMinValue(minValue);
		rnaGene.setMutationRate(mutationrate);
		rnaGene.setNaturalValue(naturalValue);
		rnaGene.setOneTimeMutation(oneTimeMutation);
		rnaGene.setOrganName(organname);
		rnaGene.setPassOnFastTime(passOnFastTime);
		rnaGene.setPubertyAge(pubertyage);
		rnaGene.setReturnToNatural(returnToNatural);
		rnaGene.setRunAsDNA(runAsDNA);
		rnaGene.setRunAsRNA(runAsRNA);
		rnaGene.setScript(script);
		rnaGene.setScriptRunOnlyAsActive(isScriptRunOnlyAsActive());
		rnaGene.setSexTraits(sexTraits);
		rnaGene.setStatName(statname);
		rnaGene.setTextValue(getTextValue());
		rnaGene.setValue(value);

		return rnaGene;
	}
}
