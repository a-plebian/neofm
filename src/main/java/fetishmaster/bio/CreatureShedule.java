/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.bio;

import fetishmaster.engine.ManagmentTask;
import fetishmaster.utils.ICopiable;

import java.io.Serializable;

/**
 * @author H.Coder
 */
public class CreatureShedule implements Serializable, ICopiable<CreatureShedule> {
	public ManagmentTask morning = null;
	public ManagmentTask day = null;
	public ManagmentTask evening = null;
	public ManagmentTask night = null;

	@Override
	public CreatureShedule copy() {
		var out = new CreatureShedule();

		out.morning = morning.copy();
		out.day = day.copy();
		out.evening = evening.copy();
		out.night = night.copy();

		return out;
	}
}
