/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.bio;

import fetishmaster.bio.organs.OrganResponse;
import fetishmaster.utils.ICopiable;

import java.util.HashMap;
import java.util.Map;

/**
 * @author H.Coder
 */
public class BodyResponse implements ICopiable<BodyResponse> {
	private final Map<String, OrganResponse> responses = new HashMap<>();

	@Override
	public BodyResponse copy() {
		var out = new BodyResponse();

		responses.forEach((string, response) -> {
			out.responses.put(string, response.copy());
		});

		return out;
	}

	public OrganResponse get(String organKey) {
		if (responses.containsKey(organKey))
			return responses.get(organKey);

		return new OrganResponse();
	}

	public double get(String organKey, String responseKey) {
		return get(organKey).get(responseKey);
	}

	public void put(String organKey, OrganResponse or) {
		responses.put(organKey, or);
	}


}
