/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.bio;

import fetishmaster.bio.organs.*;
import fetishmaster.components.StatEffect;
import fetishmaster.engine.*;
import fetishmaster.engine.backgrounds.BackgroundsManager;
import fetishmaster.engine.backgrounds.GrowInBG;
import fetishmaster.engine.scripts.ScriptEngine;
import fetishmaster.utils.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.nio.file.Path;
import java.util.*;

/**
 * @author H.Coder
 */
public class CreatureProcessor {
	private static final Logger logger = LoggerFactory.getLogger(CreatureProcessor.class);

	public static Map<String, String> organsLib = new HashMap<>();

	public static void init() {
		organsLib.put("breasts", "fetishmaster.bio.organs.Breasts");
		organsLib.put("balls", "fetishmaster.bio.organs.Balls");
		organsLib.put("uterus", "fetishmaster.bio.organs.Uterus");
		organsLib.put("pouch", "fetishmaster.bio.organs.Pouch");
		organsLib.put("abdomen", "fetishmaster.bio.organs.Abdomen");
		organsLib.put("human_body", "fetishmaster.bio.organs.HumanBody");
		organsLib.put("ass", "fetishmaster.bio.organs.Ass");
		organsLib.put("penis", "fetishmaster.bio.organs.Penis");
		organsLib.put("vagina", "fetishmaster.bio.organs.Vagina");
		organsLib.put("udder", "fetishmaster.bio.organs.Udder");
		organsLib.put("hidden_udder", "fetishmaster.bio.organs.HiddenUdder");


	}

	public static void Birth(Creature c) {
//        checkGenericRNA(c, RNAGene.AROUSAL, GameEngine.gameDataPath+"/genes/generic/arousal.gene");
//        checkGenericRNA(c, RNAGene.CHA, GameEngine.gameDataPath+"/genes/generic/cha.gene");
//        checkGenericRNA(c, RNAGene.DEX, GameEngine.gameDataPath+"/genes/generic/dex.gene");
//        checkGenericRNA(c, RNAGene.END, GameEngine.gameDataPath+"/genes/generic/end.gene");
//        checkGenericRNA(c, RNAGene.HEALTH, GameEngine.gameDataPath+"/genes/generic/health.gene");
//        checkGenericRNA(c, RNAGene.INT, GameEngine.gameDataPath+"/genes/generic/int.gene");
//        //checkGenericRNA(c, RNAGene.MAXAROUSAL, GameEngine.gameDataPath+"/genes/generic/maxarousal.gene");
//        checkGenericRNA(c, RNAGene.MAXHEALTH, GameEngine.gameDataPath+"/genes/generic/maxhealth.gene");
//        checkGenericRNA(c, RNAGene.MAXTIREDNESS, GameEngine.gameDataPath+"/genes/generic/maxtiredness.gene");
//        checkGenericRNA(c, RNAGene.SPD, GameEngine.gameDataPath+"/genes/generic/spd.gene");
//        checkGenericRNA(c, RNAGene.STA, GameEngine.gameDataPath+"/genes/generic/sta.gene");
//        checkGenericRNA(c, RNAGene.STR, GameEngine.gameDataPath+"/genes/generic/str.gene");
//        checkGenericRNA(c, RNAGene.TIREDNESS, GameEngine.gameDataPath+"/genes/generic/tiredness.gene");
		c.setStat(DNAGene.HEALTH, c.getRNAValue(DNAGene.MAXHEALTH));
		c.setStat("generic.mood", 100);

		mainStatsRecheck(c);

		DNAGenePool dna = c.getDNA();
		if (dna.hasGene("generic.birthage")) {
			DNAGene birthage = dna.getGene("generic.birthage");
			agePassHours(c, (int) birthage.getValue(), true);
			dna.removeGene("generic.birthage");
		}
		mainStatsRecheck(c);
		c.setStat(DNAGene.HEALTH, c.getRNAValue(DNAGene.MAXHEALTH));

		//sexual prefference:
		if (c.isNeuter()) {
			c.setStat("psy.to_neuter", 0);
			c.setStat("psy.to_male", 0);
			c.setStat("psy.to_female", 50);
			c.setStat("psy.to_futa", 50);


			if (Calc.chance(10)) // bi
			{
				c.addEffect("psy.to_male", "bisexual", 50);
				c.addEffect("psy.to_neuter", "bisexual", 50);
			} else if (Calc.chance(10)) //homo
			{
				c.addEffect("psy.to_male", "homosexual", 50);
				c.addEffect("psy.to_neuter", "homosexual", 50);
				c.addEffect("psy.to_female", "homosexual", -50);
				c.addEffect("psy.to_futa", "homosexual", -50);
			}
		}

		if (c.isMale()) {
			c.setStat("psy.to_neuter", 0);
			c.setStat("psy.to_male", 0);
			c.setStat("psy.to_female", 50);
			c.setStat("psy.to_futa", 40);


			if (Calc.chance(10)) // bi
			{
				c.addEffect("psy.to_male", "bisexual", 50);
				c.addEffect("psy.to_neuter", "bisexual", 50);
				c.addEffect("psy.to_futa", "bisexual", 20);
			} else if (Calc.chance(10)) //homo
			{
				c.addEffect("psy.to_male", "homosexual", 50);
				c.addEffect("psy.to_neuter", "homosexual", 30);
				c.addEffect("psy.to_female", "homosexual", -50);
				c.addEffect("psy.to_futa", "homosexual", -20);
			}
		}

		if (c.isFemale()) {
			c.setStat("psy.to_neuter", 30);
			c.setStat("psy.to_male", 50);
			c.setStat("psy.to_female", 10);
			c.setStat("psy.to_futa", 30);


			if (Calc.chance(10)) // bi
			{
				c.addEffect("psy.to_female", "bisexual", 50);
				c.addEffect("psy.to_futa", "bisexual", 30);
			} else if (Calc.chance(10)) //homo
			{
				c.addEffect("psy.to_male", "homosexual", -50);
				c.addEffect("psy.to_neuter", "homosexual", -30);
				c.addEffect("psy.to_female", "homosexual", 50);
				c.addEffect("psy.to_futa", "homosexual", -10);
			}
		}

		if (c.isFuta()) {
			c.setStat("psy.to_neuter", 30);
			c.setStat("psy.to_male", 50);
			c.setStat("psy.to_female", 50);
			c.setStat("psy.to_futa", 60);


			if (Calc.chance(10)) // female prefference
			{
				c.addEffect("psy.to_female", "female_pref", 20);
			} else if (Calc.chance(10)) //male prefference
			{
				c.addEffect("psy.to_male", "male_pref", 20);
			} else if (Calc.chance(10)) //futa pref;
			{
				c.addEffect("psy.to_futa", "futa_pref", 20);
			}
		}
	}

	public static void injectDNAGeneFromFile(Creature c, String filename) {
		DNAGene d = FileCache.getXMLObject(Path.of(filename));
		if (d == null) {
			return;
		}

		DNAGenePool dna = c.getDNA();
		GeneProcessor.MutateGene(d);
		dna.addGene(d);
	}

	public static void injectRNAGeneFromFile(Creature c, String filename) {
		DNAGene d = FileCache.getXMLObject(Path.of(filename));
		if (d == null) {
			return;
		}

		RNAGene r = GeneProcessor.DNAtoRNA(d);
		RNAGenePool rna = c.getRna();

		rna.addGene(r);
	}

	public static void checkGenericRNA(Creature c, String geneName, String filename) {
		RNAGenePool rna = c.getRna();

		if (rna.hasGene(geneName)) {
			return;
		}

		CreatureProcessor.injectRNAGeneFromFile(c, filename);
	}

	public static Creature CreateFromDNA(String name, DNAGenePool dna) {
		Creature c = new Creature(name);
		DNAGene g;
		int i;
		Organ o = null;
		String s;
		Class cl = null;

		//giving chance to run scripts at least once in the DNA genes even if it's predefined template from disk:
		Foetus ft = new Foetus();
		ft.setDNA(dna);
		ft.setHooked(new Organ(new Creature("tmp")));
		ft.nextHour(new Creature("tmp"));

		//mutate dna
		GeneProcessor.MutateDNA(dna);

		//set DNA in creature.
		c.setGenes(dna);

		//adding organs from DNA
		for (i = 0; i < dna.count(); i++) {
			g = dna.getGene(i);
			if (g.getStatName().equals("exists")) {
				if (g.isActive()) // begining finding suitable organ:
				{
					//legacy mode: look for scripted organs first
					o = loadOrgan(g.getOrganName());

					//Do we have hardcoded organ?
					if (o == null && organsLib.containsKey(g.getOrganName())) {
						s = organsLib.get(g.getOrganName());
						try {
							cl = Class.forName(s); ///////////////////////????????????????????
							//  lol at this comment ^
						} catch (ClassNotFoundException ex) {
							logger.error("Failed to find class with name \"{}\"", s, ex);
						}
						try {
							o = (Organ) cl.getDeclaredConstructor().newInstance();
						} catch (InstantiationException ex) {
							logger.error("Failed to instance class", ex);
						} catch (IllegalAccessException ex) {
							logger.error("Illegal access", ex);
						} catch (InvocationTargetException e) {
							e.printStackTrace();
						} catch (NoSuchMethodException e) {
							e.printStackTrace();
						}
					}

					//If we have any suitable organ, then adding it now.
					if (o != null) {
						c.addOrgan(o);
					}
				}

			}

		}

		c.setRna(GeneProcessor.DNAtoRNA(dna));

		c.nextHour(); // chance for RNA scripts to do initial changes before main growth starts.

		return c;
	}

	public static int agePassHours(Creature c, int hours, boolean ignoreEmergency) {
		if (ignoreEmergency && hours > (24 * 30)) {
			c.addHours(hours);
			return 0;
		}

		for (int i = 0; i < hours; i++) {
			if (c.nextHour(ignoreEmergency) && !ignoreEmergency) {
				return i;
			}

		}

		mainStatsRecheck(c);
		genesRangeCheck(c);

		return 0;
	}

	public static int agePassDays(Creature c, int days, boolean ignoreEmergency) {
		return agePassHours(c, days * 24, ignoreEmergency);
	}

	public static String workerWorkOnShedule(Creature c) {
		if (c.isWorker()) {
			ManagmentTask mt = null;

			if (GameEngine.activeWorld.clock.isMorning()) {
				mt = c.shedule.morning;
			}
			if (GameEngine.activeWorld.clock.isDay()) {
				mt = c.shedule.day;
			}
			if (GameEngine.activeWorld.clock.isEvening()) {
				mt = c.shedule.evening;
			}
			if (GameEngine.activeWorld.clock.isNight()) {
				mt = c.shedule.night;
			}

			if (mt != null) {
				//check if tiredness is too high and cancel setted task;
				if (c.getRNAValue(DNAGene.TIREDNESS) >= c.getRNAValue(DNAGene.MAXTIREDNESS)) {
					mt = ManagementEngine.getDefaultTask();
					c.addHistory("To tired!", c.getName() + " is too tired to do anything.");
				}

				if (!ManagementEngine.isConditionsRight(c, mt)) {
					mt = ManagementEngine.getDefaultTask();
				}
				ScheduleTaskResult str;
				str = ScriptEngine.processCreatureScript(c, mt.getScript(), mt);
				if (str.text != null) {
					if (!str.text.equals("")) {
						c.addHistory(mt.getName(), str.text, str.img);
					}
					return str.text;
				}
			}

		}
		return "";
	}

	public static Creature getCreature(String name) {
		for(Creature creature : GameEngine.activeWorld.getCreatures()) {
			if (creature.getName().equals(name)) {
				return creature;
			}
		}
		return null;
	}

	public static Creature getCreatureByUID(String UID) {
		for(Creature creature : GameEngine.activeWorld.getCreatures()) {
			if (creature.getUID().equals(UID)) {
				return creature;
			}
		}
		return null;
	}

	public static Creature CreatureFromTDNA(String name, TemplateDNA tdna) {
		DNAGenePool dna;
		int i;

		if (tdna == null) //no template trying to load dna itself.
		{
			return null;
		} else {
			DNAGenePool adna;

			if (tdna == null) {
				return null;
			}

			dna = new DNAGenePool();

			for (i = 0; i < tdna.count(); i++) {
				adna = FileCache.getGamedata("dna/" + tdna.getPool(i) + ".dna");
				if (adna != null) {
					dna.mergeGenes(adna);
				}

			}

		}

		if (dna == null || dna.count() == 0) {
			return null;
		}

		Creature c = CreateFromDNA(name, dna);

		//DNAGene bage = c.getDNAGene("generic.birthage");

		CreatureProcessor.Birth(c);
//        if (bage != null)
//        {
//            
//            for (i = 0; i < bage.getValue(); i++)
//            {
//                c.nextHour(true); //to do with true;
//            }
//        }

		return c;
	}

	public static Creature loadCreature(String name, String filename) {
		//Check if we have template:
		TemplateDNA tdna = FileCache.getGamedata("dna/" + filename + ".tdna");

		DNAGenePool dna;
		int i;

		if (tdna == null) //no template trying to load dna itself.
		{
			dna = FileCache.getGamedata("dna/" + filename + ".dna");
		} else {
			DNAGenePool adna;

			if (tdna == null) {
				return null;
			}

			dna = new DNAGenePool();

			for (i = 0; i < tdna.count(); i++) {
				adna = FileCache.getGamedata("dna/" + tdna.getPool(i) + ".dna");
				if (adna != null) {
					dna.mergeGenes(adna);
				}

			}

		}

		if (dna == null || dna.count() == 0) {
			return null;
		}

		Creature c = CreateFromDNA(name, dna);

		//DNAGene bage = c.getDNAGene("generic.birthage");

		CreatureProcessor.Birth(c);
//        if (bage != null)
//        {
//            
//            for (i = 0; i < bage.getValue(); i++)
//            {
//                c.nextHour(true); //to do with true;
//            }
//        }

		return c;
	}

	public static Creature loadCreatureInBG(String name, String filename) {
		TemplateDNA tdna = FileCache.getGamedata("dna/" + filename + ".tdna");

		DNAGenePool dna;
		int i;

		if (tdna == null) //no template trying to load dna itself.
		{
			dna = FileCache.getGamedata("dna/" + filename + ".dna");
		} else {
			DNAGenePool adna;

			if (tdna == null) {
				return null;
			}

			dna = new DNAGenePool();

			for (i = 0; i < tdna.count(); i++) {
				adna = FileCache.getGamedata("dna/" + tdna.getPool(i) + ".dna");
				if (adna != null) {
					dna.mergeGenes(adna);
				}

			}

		}

		if (dna == null || dna.count() == 0) {
			return null;
		}

		Creature c = CreateFromDNA(name, dna);

		BackgroundsManager.addBackgroundTask(new GrowInBG(c));

		return c;
	}

	public static void mainStatsRecheck(Creature c) //every hour
	{
		//regen recalc
		c.updateEffect("generic.regen_rate", "endurance", c.getRNAValue(DNAGene.END) / 100);

		//health rechek
		c.setStat(DNAGene.MAXHEALTH, c.getRNAValue(DNAGene.END) * 1.5);
		c.setStat(DNAGene.MAXTIREDNESS, c.getRNAValue(DNAGene.END) + c.getRNAValue(DNAGene.STR));

		RNAGene h = c.getRNAGene(DNAGene.HEALTH), mh = c.getRNAGene(DNAGene.MAXHEALTH);
		if (h.getCleanValue() > mh.getCleanValue()) {
			h.setValue(mh.getCleanValue());
		}

		//mood rechek
		if (c.getRNAValue(DNAGene.TIREDNESS) > Calc.procent(c.getRNAValue(DNAGene.MAXTIREDNESS), 70)) {
			c.addStat("generic.mood", Calc.PlusMinusXProcent(-5, 50));
		}

		if (c.getRNAValue(DNAGene.TIREDNESS) < Calc.procent(c.getRNAValue(DNAGene.MAXTIREDNESS), 30)) {
			c.addStat("generic.mood", Calc.PlusMinusXProcent(1, 50));
		}

		if (c.getCleanRNAValue("generic.mood") > 100) {
			c.setStat("generic.mood", 100);
		}

		//arousal rechechk
//        if (c.getStat("generic.arousal") < c.getStat("generic.libido") * 3.5)
//        {
//            c.addStat("generic.arousal", c.getStat("generic.libido") / 100);
//        }
		if (c.getCleanRNAValue("generic.arousal") < 0) {
			c.setStat("generic.arousal", 0);
		}
		if (c.getCleanRNAValue("generic.libido") < 0) {
			c.setStat("generic.libido", 0);
		}

		if (c.getCleanRNAValue("generic.arousal") > 100) {
			c.setStat("generic.arousal", 100);
		}
		if (c.getCleanRNAValue("generic.libido") > 100) {
			c.setStat("generic.libido", 100);
		}

		if (c.getCleanRNAValue("generic.tiredness") < 0) {
			c.setStat("generic.tiredness", 0);
		}

		if (c.getCleanRNAValue("generic.health") < -1) {
			c.setStat("generic.tiredness", -1);
		}

		interactionStatsRecheck(c);
	}

	public static void interactionStatsRecheck(Creature c) {
		//melee(basic) attack force
		c.setStat("generic.attack", c.getRNAValue("generic.str") + c.getRNAValue("generic.dex"));
		//defence raiting
		c.setStat("generic.defence", c.getRNAValue("generic.str") + ((c.getRNAValue("generic.dex") / 10) * (c.getRNAValue("generic.spd") / 10)));
	}

	public static void criticalConditionsCheck(Creature c) {
		RNAGene h = c.getRNAGene(DNAGene.HEALTH), mh = c.getRNAGene(DNAGene.MAXHEALTH);

		if (h.getValue() <= 0) {
			if (c.isWorker()) {
				ManagementEngine.sendToHospital(c, 24 + ((int) (4 * Math.abs(h.getValue()))));
			}
		}

	}

	public static void genesRangeCheck(Creature c) {
		RNAGenePool rna = c.getRna();
		RNAGene r;
		int i;
		for (i = 0; i < rna.count(); i++) {
			r = (RNAGene) rna.getGene(i);
			if (r.isCheckRange()) {
				if (r.getCleanValue() < r.getMinValue()) {
					r.setValue(r.getMinValue());
				}

				if (r.getCleanValue() > r.getMaxValue()) {
					r.setValue(r.getMaxValue());
				}
			}
		}

	}

	public static ScriptedOrgan loadOrgan(String name) {
		if (FileCache.gamedataFileExists("organs/" + name + ".organ")) {
			return FileCache.getGamedata("organs/" + name + ".organ");
		} else {
			return null;
		}

	}

	public static void createNewOva(Creature c) {

		Ova ov = new Ova();
		DNAGenePool cdt = c.getDNA();
		DNAGenePool odna = (DNAGenePool) cdt.copy();

		odna.removeGene("genetic.father");
		odna.removeGene("father.name");

		DNAGene gm = new DNAGene("genetic", "mother", 0, true);
		gm.setTextValue(c.getUID());
		odna.addGene(gm);
		gm = new DNAGene("mother", "name", 0, true);
		gm.setTextValue(c.getName());
		odna.addGene(gm);
		ov.setDna(odna);

		ov.setRna((RNAGenePool) c.getRna().copy());

		Organ o = c.getOrgan("uterus");
		if (o == null) {
			return;
		}

		o.addHookedObject(ov);

		logger.debug("Ova created for \"{}\"", c.getName());

//        if (GameEngine.fullDevMode)
//        {
//            DNAGene or, cp;
//            or = c.getDNA().getGene("generic.sex");
//            cp = odna.getGene("generic.sex");
//            cp.setValue(5);
//            System.out.println("Orig:" + or + " Clone:"+ cp.getClass());
//        }

	}

	public static void createNewOva(Creature c, int count) {
		for (int i = 0; i < count; i++) {
			createNewOva(c);
		}
	}

	public static void injectSemen(Creature cm, Creature cf, double volume) {
		if (!cf.hasOrgan("uterus")) {
			return;
		}

		Semen sm = createSemenDose(cm, volume);
		Organ o = cf.getOrgan("uterus");

		o.addHookedObject(sm);
	}

	public static Semen createSemenDose(Creature c, double volume) {

		Semen s = new Semen();

		DNAGenePool sdna = (DNAGenePool) c.getDNA().copy();

		sdna.removeGene("genetic.mother");
		sdna.removeGene("mother.name");

		DNAGene gm = new DNAGene("genetic", "father", 0, true);
		gm.setTextValue(c.getUID());
		sdna.addGene(gm);
		gm = new DNAGene("father", "name", 0, true);
		gm.setTextValue(c.getName());
		sdna.addGene(gm);

		s.setRna((RNAGenePool) c.getRna().copy());

		s.setDna(sdna);
		s.setVolume(volume);

		return s;
	}

	public static boolean Fertilize(Organ uterus) {
		if (uterus == null) {
			return false;
		}

		boolean fert = false;

		Semen sperm;

		Ova ova;

		Foetus ft;

		List<OrganHook> ovas = uterus.selectHooksByName("ova");

		// fertilization tryes for all ova
		for (OrganHook organHook : ovas) {
			ova = (Ova) organHook;
			sperm = SelectSpermInUterus(uterus);
			if (sperm == null) {
				continue;
			}

			if (ova.isSemenUsed(sperm)) {
				logger.debug("This semen already used with this ova ({})", ova.getName());
			} else {
				ft = FertilizeOva(ova, sperm);
				ova.addToSemenList(sperm);
				if (ft != null) {
					uterus.getHost().setFlag("last_fertilization", uterus.getHost().getAge());
					uterus.addHookedObject(ft);
					uterus.getHost().addStat("uterus.embrios", 1);
					fert = true;
					ova.unHook();
					logger.debug("Foetus added to \"{}\"", uterus.getHost().getName());
					int max = 0;
					while (Calc.chance(uterus.getHost().getStat("foetus.split_chance")) && max < 9999) {
						Foetus twin = new Foetus();
						DNAGenePool dna = ft.getDNA();
						dna = (DNAGenePool) dna.copy();
						twin.setDNA(dna);
						twin.setGrowRate(dna.getGene("foetus.grow_rate").getValue());
						twin.setReadyAge(dna.getGene("foetus.grow_time").getValue());
						uterus.addHookedObject(twin);
						uterus.getHost().addStat("uterus.embrios", 1);
						max++;
					}
				}
			}
		}
		return fert;

	}

	public static Foetus FertilizeOva(Ova ova, Semen sperm) {
		if (ova == null || sperm == null) {
			return null;
		}

		DNAGenePool ovadna = ova.getDna();
		DNAGenePool spermdna = sperm.getDna();

		if (ovadna == null || spermdna == null) {
			return null;
		}

		double fert_chance = 0;

		if (ova.getRna() != null && sperm.getRna() != null) {
			fert_chance = GeneProcessor.CalcFertilizationChance(ova.getRna(), sperm.getRna());
		}

		logger.debug("Fertilizing ova \"{}\"...", ova.getName());
		logger.debug("Fertilization chance: {}", fert_chance);

		if (!Calc.chance(fert_chance)) {
			return null;
		}

		logger.debug("Ova fertilized!");

		DNAGenePool childDNA = GeneProcessor.NormalDNACreate(spermdna, ovadna);

		Foetus ft = new Foetus();

		ft.setDNA(childDNA);

		ft.setGrowRate(childDNA.getGene("foetus.grow_rate").getValue());
		ft.setReadyAge(childDNA.getGene("foetus.grow_time").getValue());

		return ft;
	}

	public static Semen SelectSpermInUterus(Organ uterus) {
		OrganHook res;
		Semen smn;
		List<OrganHook> sperms = uterus.selectHooksByName("semen");
		ChanceRoulette spr = new ChanceRoulette();
		StateMap stm;

		for (OrganHook sperm : sperms) {
			smn = (Semen) sperm;

			stm = smn.getState();

			spr.addField(smn, stm.getState("fertility") * (stm.getState("volume") * 0.010 + 1));

		}
		res = (OrganHook) spr.getChance();

		if (res != null) {
			logger.debug("Selected sperm; Fertility: {}; Volume: {}", res.getState().getState("fertility"), res.getState().getState("volume"));
		}

		return (Semen) res;
	}

	public static Creature BirthFoetus(Foetus ft) {
		if (ft == null) {
			return null;
		}
		Creature c;
		DNAGenePool dna = ft.getDNA();

		if (ft.getState().getState("is_live") < 1) {
			ft.unHook();
			return null;
		}

		c = CreateFromDNA(TextProcessor.getRandomName((int) dna.getGene("generic.sex").getValue()), dna);

		Birth(c);

		ft.unHook();

		return c;
	}

	public static List<Creature> BirthAll(Creature c) {
		List<Creature> childs = new ArrayList<Creature>();

		Foetus ft;
		Creature ct;

		Organ uterus = c.getOrgan("uterus");

		if (uterus == null) {
			return childs;
		}

		List<OrganHook> embrio = uterus.selectHooksByName("foetus");

		for (OrganHook organHook : embrio) {
			ft = (Foetus) organHook;
			ct = BirthFoetus(ft);
			c.subStat("uterus.embrios", 1);
			if (ct != null) {
				childs.add(ct);
			}
		}

		//c.doAction("birth");

		return childs;
	}

	public static void Ovulation(Creature c) {
		int ovacount = (int) c.getStat("fertility.ovulation_ova");

		while (ovacount < 999 && Calc.chance(c.getStat("fertility.extra_ova"))) {
			ovacount++;
		}

		createNewOva(c, ovacount);
	}

	public static double EmbriosVolume(Creature c) {
		if (c == null) {
			return 0;
		}

		return EmbriosVolume(c.getOrgan("uterus"));
	}

	public static double EmbriosVolume(Organ o) {
		if (o == null) {
			return 0;
		}

		double res = 0;

		int i;
		OrganHook oh;

		for (i = 0; i < o.hooksCount(); i++) {
			oh = o.getHook(i);
			if (oh.getName().equals("foetus")) {
				res += oh.getVolume();
			}
		}
		return res;
	}

	public static void updateScriptedOrgans(Creature creature) {
		if (creature == null) {
			return;
		}
		var or = creature.getOrgans();
		List<Organ> so = new ArrayList<>();
		int i;
		Organ o, nwo;

		logger.debug("Updating scripted organs for \"{}\"", creature.getName());
		for (i = 0; i < or.size(); i++) {
			o = or.get(i);
			if (o instanceof ScriptedOrgan) {
				so.add(o);
			}
		}
		while (!so.isEmpty())//load new version of organ
		{
			o = so.get(0);

			nwo = loadOrgan(o.getName());
			if (nwo == null) {
				continue;
			}

			logger.debug("Updating \"{}\"'s \"{}\"", creature.getName(), o.getName());

			//hooks and stats...
			nwo.setHooked(o.getHooked());
			nwo.setStats(o.getStats());

			creature.removeOrgan(o.getName());
			creature.addOrgan(nwo);

			so.remove(o);
		}
	}

	public static void updateScriptedOrgans(List<Creature> clist) {
		if (clist == null) {
			return;
		}
		for (Creature creature : clist) {
			updateScriptedOrgans(creature);
		}
	}

	public static void doTask(Creature c, String taskname) {
		ManagmentTask mt = ManagementEngine.getTask(taskname);
		if (mt == null) {
			return;
		}

		String scr, res;

		if (ManagementEngine.isConditionsRight(c, mt)) {
			scr = mt.getScript();
			ScriptEngine.processSheduleConditionScript(c, scr);
		}

	}

	public static void resetFetishes(Creature c) {
		if (c == null) {
			return;
		}

		RNAGenePool rna = c.getRna();
		RNAGene r;
		int i;
		for (i = 0; i < rna.count(); i++) {
			r = (RNAGene) rna.getGene(i);
			if (r.getOrganName().equals("fetish")) {
				r.setActive(false);
			}
		}
	}

	public static void activateFetish(Creature c, String fetish) {
		if (c == null) {
			return;
		}

		RNAGenePool rna = c.getRna();
		RNAGene r;
		int i;
		for (i = 0; i < rna.count(); i++) {
			r = (RNAGene) rna.getGene(i);
			if (r.getOrganName().equals("fetish") && r.getStatName().equals(fetish)) {
				r.setActive(true);
			}
		}
	}

	public static double getFetishMod(Creature c) {
		double res = 0;

		if (c == null) {
			return 0;
		}

		RNAGenePool rna = c.getRna();
		RNAGene r;
		int i;
		for (i = 0; i < rna.count(); i++) {
			r = (RNAGene) rna.getGene(i);
			if (r.getOrganName().equals("fetish") && r.isActive()) {
				res += r.getValue();
			}
		}

		return res;
	}

	public static void injectOVA(Creature donor, Creature recepient) {

		Ova ov = new Ova();
		ov.setDna(donor.getDNA());
		ov.setRna((RNAGenePool) donor.getRna().copy());
		Organ o = recepient.getOrgan("uterus");
		if (o == null) {
			return;
		}

		o.addHookedObject(ov);
		if (GameEngine.devMode) {
			System.out.println(recepient.getName() + " - Ova from " + donor.getName() + " injected");
		}
	}

	public static double getFoetusGeneValue(Creature c, int foetus, String gene) {
		DNAGene g = getFoetusGene(c, foetus, gene);
		if (g == null) {
			return 0;
		}

		return g.getValue();
	}

	public static String getFoetusGeneText(Creature c, int foetus, String gene) {
		DNAGene g = getFoetusGene(c, foetus, gene);
		if (g == null) {
			return "";
		}

		return g.getTextValue();
	}

	public static void setFoetusGeneValue(Creature c, int foetus, String gene, double value) {

		DNAGene g = getFoetusGene(c, foetus, gene);
		if (g == null) {
			return;
		}

		g.setValue(value);
	}

	public static void setFoetusGeneText(Creature c, int foetus, String gene, String text) {

		DNAGene g = getFoetusGene(c, foetus, gene);
		if (g == null) {
			return;
		}

		g.setTextValue(text);
	}

	public static int foetusCount(Creature c) {
		Organ ut = c.getOrgan("uterus");
		if (ut == null) {
			return -1;
		}

		List<OrganHook> af = ut.selectHooksByName("foetus");
		if (af.isEmpty()) {
			return -1;
		}

		return af.size();
	}

	public static DNAGene getFoetusGene(Creature c, int foetus, String gene) {
		Organ ut = c.getOrgan("uterus");
		if (ut == null) {
			return null;
		}

		List<OrganHook> af = ut.selectHooksByName("foetus");
		if (af.size() < foetus) {
			return null;
		}

		Foetus oh = (Foetus) af.get(foetus);
		if (oh == null) {
			return null;
		}

		DNAGenePool fdna = oh.getDNA();
		if (fdna == null) {
			return null;
		}

		DNAGene g = fdna.getGene(gene);

		return g;
	}

	public static void updateScriptedEffects(List<Creature> creatures) {
		RNAGenePool rna;
		RNAGene r;
		int i;
		StatEffect eff;
		for (Creature creature : creatures) {
			rna = creature.getRna();
			for (i = 0; i < rna.count(); i++) {
				r = (RNAGene) rna.getGene(i);
				for (int i1 = 0; i1 < r.effectCount(); i1++) {
					eff = r.getEffect(i1);
					if (eff.getFilename() != null) // reloading scripted effect
					{
						logger.debug("Character: {}; Updating effect: {}", creature.getName(), eff.getFilename());
						StatEffect tmp = FileCache.getGamedata("effects/" + eff.getFilename() + ".effect");
						if (tmp == null) {
							continue;
						}

						eff.setScript(tmp.getScript());

					}

				}
			}
		}

	}

	public static void updateScriptedTasks(List<Creature> creatures) {
		if (creatures == null) {
			return;
		}
		int i;
		for (i = 0; i < creatures.size(); i++) {
			updateScriptedTasks(creatures.get(i));
		}
	}

	private static void updateScriptedTasks(Creature c) {
		if (c == null) {
			return;
		}

		if (c.shedule.morning != null) {
			String t = c.shedule.morning.getName();
			c.shedule.morning = ManagementEngine.getTask(t);
		}
		if (c.shedule.day != null) {
			String t = c.shedule.day.getName();
			c.shedule.day = ManagementEngine.getTask(t);
		}
		if (c.shedule.evening != null) {
			String t = c.shedule.evening.getName();
			c.shedule.evening = ManagementEngine.getTask(t);
		}
		if (c.shedule.night != null) {
			String t = c.shedule.night.getName();
			c.shedule.night = ManagementEngine.getTask(t);
		}
	}
}
