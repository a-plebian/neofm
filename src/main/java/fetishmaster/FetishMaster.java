/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster;

import com.formdev.flatlaf.FlatDarkLaf;
import com.formdev.flatlaf.FlatLightLaf;
import fetishmaster.display.JFrameMasterWindow;
import fetishmaster.engine.GameEngine;
import fetishmaster.engine.scripts.ScriptEngine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Random;

/**
 * @author H.Coder
 */
public class FetishMaster {


	//TODO: Replace XStream so that this insane hack isn't needed anymore
	//This silences the illegal reflective access warnings
	public static void SILENCIO() {
		try {
			// Activate /devilish/ mode
			var unsafeClass = Class.forName("sun.misc.Unsafe");
			var field = unsafeClass.getDeclaredField("theUnsafe");
			field.setAccessible(true);
			Object unsafe = field.get(null);

			Method putObjectVolatile = unsafeClass.getDeclaredMethod("putObjectVolatile", Object.class, long.class, Object.class);
			Method staticFieldOffset = unsafeClass.getDeclaredMethod("staticFieldOffset", Field.class);

			// reflect into the internal logger
			Class loggerClass = Class.forName("jdk.internal.module.IllegalAccessLogger");
			// get the logger field
			Field loggerField = loggerClass.getDeclaredField("logger");
			// evil fuckshit
			// grab memory offset of the static field
			Long offset = (Long) staticFieldOffset.invoke(unsafe, loggerField);
			// force set the logger to null
			putObjectVolatile.invoke(unsafe, loggerClass, offset, null);
		} catch (Exception eated) { }
	}

	/**
	 * @param args the command line arguments
	 */

	public static final String[] strongs = {
			"Back so soon?",
			"This is an intervention, your parents are worried about you",
			"off for another rousing bout of wankie jerkoffs",
			"---ACTIVATING COOM BOOSTERS---",
			"Welcome back weird perverts.",
			"Sorry.",
			"Welcome to Horny Jail",
			"Also try Space Station 13!",
			"Catgirls are diet furries",
			"poo poo fart 69",
			"It's a fucking piece of sheiiit.",
			"Approved by 4 out of 5 coomers",
			"Powered by Hi",
			"Canned log joke",
			"Designed to be played one handed",
			"This game is FUCKED",
			"error 404: funny not found",
			"OH MY GOOOOOOOD!",
			"That's right Jay!",
			"Funniest shit I've ever seen",
			"FACTS and LOGIC",
	};

	public static final Logger logger = LoggerFactory.getLogger(FetishMaster.class);

	public static void main(String[] args) {
		SILENCIO();

		if (args.length > 0) {
			int i = 0;
			for (String arg : args) {
				//TODO: Generic args system
				switch (arg) {
					case "--devmode" -> GameEngine.devMode = true;
					case "--fulledit" -> GameEngine.fullDevMode = true;
					case "-data" -> {
						if (i + 1 < args.length) {
							GameEngine.gameDataPath = args[i + 1];
						} else {
							logger.error("-data passed as last arg (no parameter)");
						}
					}
				}
				i++;
			}
		}

		logger.info("NeoFM Initializing...");
		Random random = new Random();
		logger.info("{}", strongs[random.nextInt(strongs.length)]);
		logger.info("Created by H.Coder");
		logger.info("Version: {}", GameEngine.version);
		logger.info("OS: {}, {}", System.getProperty("os.name"), System.getProperty("os.version"));
		logger.info("Java Version: {}", System.getProperty("java.version"));
		logger.info("Java VM Version: {}", System.getProperty("java.vendor"));



		GameEngine.debugStartApp();
		ScriptEngine.init();

		try {
			UIManager.setLookAndFeel(new FlatDarkLaf());
			JFrame.setDefaultLookAndFeelDecorated(true);
			JDialog.setDefaultLookAndFeelDecorated(true);

		} catch (Exception e) {
			//Exception handle
		}

		java.awt.EventQueue.invokeLater(() -> {
			JFrameMasterWindow mainframe = new JFrameMasterWindow();
			mainframe.setDefaultCloseOperation(JFrameMasterWindow.DISPOSE_ON_CLOSE);
			mainframe.setVisible(true);
			GameEngine.activeMasterWindow = mainframe;
		});

	}


}
