/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.contracts;

import fetishmaster.bio.Creature;
import fetishmaster.engine.scripts.ScriptEngine;

/**
 * @author H.Coder
 */
public class ContractCondition {
	public String name = "Noname";
	public String conditionScript;
	public String brokenScript;
	public String voidText;
	public boolean payfine = false;



	public boolean isBroken(Creature c) {
		return ScriptEngine.processSheduleConditionScript(c, conditionScript);
	}


}
