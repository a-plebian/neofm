/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.contracts;

import fetishmaster.bio.Creature;

/**
 * @author H.Coder
 */
public class ContractConditionBadMood extends ContractCondition {
	public ContractConditionBadMood() {
		this.payfine = true;
		this.voidText = "Work here is too nasty! I'm leaving!";
	}

	@Override
	public boolean isBroken(Creature c) {
		return c.getRNAValue("generic.mood") < 0;
	}

}
