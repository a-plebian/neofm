/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.contracts;

import fetishmaster.bio.Creature;
import fetishmaster.engine.GameEngine;
import fetishmaster.engine.scripts.ScriptEngine;
import fetishmaster.engine.scripts.Status;
import fetishmaster.engine.scripts.VarContext;
import fetishmaster.utils.ICopiable;

import java.util.ArrayList;
import java.util.List;

/**
 * @author H.Coder
 */
public class WorkerContract implements ICopiable<WorkerContract> {
	protected Creature worker;
	protected String desc;
	protected int value;
	protected boolean temp;
	protected int timeleft;
	protected int conage;
	protected List<ContractCondition> conditions = new ArrayList<>();
	private String name;
	private boolean custom = false;

	public WorkerContract() {

	}

	public WorkerContract(Creature c, int value) {

	}

	@Override
	public WorkerContract copy() {
		var out = new WorkerContract();

		out.desc = desc;
		out.value = value;
		out.temp = temp;
		out.timeleft = timeleft;
		out.conage = conage;


		return out;
	}

	/**
	 * @return the worker
	 */
	public Creature getWorker() {
		return worker;
	}

	/**
	 * @param worker the worker to set
	 */
	public void setWorker(Creature worker) {
		this.worker = worker;
		conage = 0;
	}

	/**
	 * @return the desc
	 */
	public String getDesc() {
		if (worker != null) {
			String txt;

			VarContext wcw = new VarContext();
			ScriptEngine.loadVars(wcw, worker, null);
			txt = ScriptEngine.parseForScripts(desc, wcw);

			return txt;
		} else {
			return desc;
		}
		//return desc;
	}

	/**
	 * @param desc the desc to set
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}

	/**
	 * @return the value
	 */
	public int getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(int value) {
		this.value = value;
	}

	/**
	 * @return the temp
	 */
	public boolean isTemp() {
		return temp;
	}

	/**
	 * @param temp the temp to set
	 */
	public void setTemp(boolean temp) {
		this.temp = temp;
	}

	/**
	 * @return the timeleft
	 */
	public int getTimeleft() {
		return timeleft;
	}

	/**
	 * @param timeleft the timeleft to set
	 */
	public void setTimeleft(int timeleft) {
		this.timeleft = timeleft;
	}

	/**
	 * @return the conditions
	 */
	public List<ContractCondition> getConditions() {
		return conditions;
	}

	/**
	 * @param conditions the conditions to set
	 */
	public void setConditions(List<ContractCondition> conditions) {
		this.conditions = conditions;
	}

	public String testConditions() {
		int i;
		ContractCondition cc;
		String res = null;

		for (i = 0; i < conditions.size(); i++) {

			cc = conditions.get(i);
			if (cc.isBroken(worker)) {
				res = cc.voidText;
				if (cc.payfine == true) {
					GameEngine.activeWorld.playerAvatar.inventory.addMoney(this.value / 2);
				}

				if (this.custom) {
					ScriptEngine.processSheduleConditionScript(this.worker, cc.brokenScript);
					break;
				}
			}
		}


		if (Status.isAdult(worker))
			conage++;
		if (!this.custom) {
			worker.updateEffect("generic.mood", "contract_age", -conage / 900, 2);
		}
		//worker.subStat("generic.mood", conage/10000);

		return res;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the scripted
	 */
	public boolean isCustom() {
		return custom;
	}

	/**
	 * @param scripted the scripted to set
	 */
	public void setCustom(boolean scripted) {
		this.custom = scripted;
	}


}
