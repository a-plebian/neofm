/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.contracts;

import fetishmaster.bio.Creature;

/**
 * @author H.Coder
 */
public class ContractConditionHealth extends ContractCondition {

	public ContractConditionHealth() {
		this.payfine = false;
		this.voidText = "The healthcare is terrible here! I'm leaving!";
	}

	@Override
	public boolean isBroken(Creature c) {
		return c.getRNAValue("generic.knockouts") > 3;
	}
}
