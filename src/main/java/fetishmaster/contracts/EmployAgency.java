/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.contracts;

import fetishmaster.bio.Creature;
import fetishmaster.bio.CreatureProcessor;
import fetishmaster.bio.DNAGenePool;
import fetishmaster.engine.GameEngine;
import fetishmaster.engine.TextProcessor;
import fetishmaster.engine.backgrounds.BackgroundsManager;
import fetishmaster.engine.backgrounds.UpdateAgencyScripted;
import fetishmaster.utils.Calc;

import java.util.ArrayList;
import java.util.List;

/**
 * @author H.Coder
 */
public class EmployAgency {
	private final int nextContractor = 0;
	public ContractList contracts = new ContractList();

	public void nextHour() {
		int i = contracts.getContractCount() - 1;
		WorkerContract wc;
		int ttl;
		while (i >= 0) {
			ttl = contracts.getContractTtl(i);
			ttl--;
			contracts.setContractTtl(i, ttl);

			if (ttl <= 0) {
				contracts.removeContract(i);

			}

			i--;
		}

//        nextContractor--;
//        if(nextContractor <= 0)
//        {
//            BackgroundsManager.addBackgroundTask(new UpdateAgency());
//            nextContractor = (int)Calc.random(24)+5;
//        }
		BackgroundsManager.addBackgroundTask(new UpdateAgencyScripted());

	}

	public void createNewContractor(String dnaPath) {
		Creature c = CreatureProcessor.loadCreature("agency", dnaPath);

		if (c == null)
			return;


		String name = TextProcessor.getRandomName((int) c.getRNAValue(("generic.sex")));
//        Creature c = CreatureProcessor.CreateFromDNA(name, dna);
//        CreatureProcessor.Birth(c);
		c.setName(name);

		WorkerContract wc = createContract();

		wc.setWorker(c);

		double val = 0;

		val += c.getStat("generic.str");
		val += c.getStat("generic.dex");
		val += c.getStat("generic.spd");
		val += c.getStat("generic.end");
		val += c.getStat("generic.int");
		val += c.getStat("generic.cha");

		contracts.addContract(wc, (int) val * 3);

	}

	public void createNewContractor(DNAGenePool dna) {
		if (dna == null)
			return;

		String name = TextProcessor.getRandomName((int) dna.getGene("generic.sex").getValue());
		Creature c = CreatureProcessor.CreateFromDNA(name, dna);
		CreatureProcessor.Birth(c);

		WorkerContract wc = createContract();

		wc.setWorker(c);

		contracts.addContract(wc, (int) Calc.random(100) + 24);
	}


	public WorkerContract createContract() {
		WorkerContract wc = new ContractAgencyBasic();

		wc.conditions.add(new ContractConditionHealth());
		wc.conditions.add(new ContractConditionBadMood());

		return wc;
	}

//    public void cleanContracts()
//    {
//        contracts.removeAll();
//    }

	public void acceptContract(int pos) {
		if (pos >= contracts.getContractCount())
			return;

		WorkerContract wc = contracts.getContract(pos);
		GameEngine.acceptContract(wc);
		contracts.removeContract(pos);
	}

	public WorkerContract createLegacyContract() {
		WorkerContract wc = new LegacyContract();

		wc.conditions.add(new ContractConditionHealth());
		wc.conditions.add(new ContractConditionBadMood());

		return wc;
	}

	public void addLegacyWorkers(List<Creature> workers) {
		WorkerContract wc;

		for (Creature worker : workers) {
			wc = createLegacyContract();
			wc.setWorker(worker);
			GameEngine.acceptContract(wc);
		}

	}

	public List<Creature> getCreatures() {
		List<Creature> cl = new ArrayList<>();


		for (int i = 0; i < contracts.getContractCount(); i++) {
			cl.add(contracts.getContract(i).getWorker());
		}

		return cl;
	}
}
