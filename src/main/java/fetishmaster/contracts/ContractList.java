/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.contracts;

import fetishmaster.bio.Creature;

import java.util.ArrayList;
import java.util.List;

/**
 * @author H.Coder
 */
public class ContractList {
	private final List<WorkerContract> contracts = new ArrayList<>();
	private final List<Integer> ttl = new ArrayList<>();

	public synchronized void addContract(WorkerContract wc, int hours) {
		ttl.add(hours);
		contracts.add(wc);
	}

	public synchronized WorkerContract getContract(int pos) {
		return (WorkerContract) contracts.get(pos);
	}

	public synchronized WorkerContract getContract(Creature c) {
		int i;
		WorkerContract wc = null;

		for (i = 0; i < contracts.size(); i++) {
			wc = contracts.get(i);
			if (wc.getWorker() == c)
				return wc;
		}

		return null;
	}

	public synchronized int getContractNumber(Creature c) {
		int i;
		WorkerContract wc = null;

		for (i = 0; i < contracts.size(); i++) {
			wc = contracts.get(i);
			if (wc.getWorker() == c)
				return i;
		}

		return -1;
	}

	public synchronized int getContractCount() {
		return contracts.size();
	}

	public synchronized String getContractDesc(int pos) {
		return getContract(pos).getDesc();
	}

	public synchronized int getContractTtl(int pos) {
		if (ttl.size() >= pos)
			return ttl.get(pos);
		else
			return -1;
	}

	public synchronized void setContractTtl(int pos, int hours) {
		ttl.set(pos, hours);
	}

	public synchronized void removeContract(int pos) {
		contracts.remove(pos);
		ttl.remove(pos);
	}

	public synchronized void removeContract(Creature c) {
		int pos = getContractNumber(c);
		if (pos == -1)
			return;

		removeContract(pos);
	}

	public synchronized void removeAll() {
		contracts.clear();
		ttl.clear();
	}


}
