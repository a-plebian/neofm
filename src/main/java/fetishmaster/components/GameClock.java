/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.components;

/**
 * @author H.Coder
 */
public class GameClock {
	private int hours;
	private int days;

	public void GameClock() {
		hours = 0;
		days = 0;
	}

	public void addHours(int value) {
		this.hours += value;
		check();
	}

	public void addDays(int value) {
		this.days += value;
		check();
	}

	private void check() {
		int tmp = 0;

		if (this.hours > 23) {
			tmp = this.hours / 24;
			this.days += tmp;
			this.hours -= tmp * 24;
		}

	}

	public int getDays() {
		return this.days;
	}

	public int getHours() {
		return this.hours;
	}

	public int getAHours() {
		return days * 24 + hours;
	}

	public String getTextDate() {
		String ret;

		ret = "Day: " + getDays() + ", " + getHours() + ":00";
		return ret;
	}

	public boolean isMorning() {
		return hours >= 6 && hours < 12;
	}

	public boolean isDay() {
		return hours >= 12 && hours < 18;
	}

	public boolean isEvening() {
		return hours >= 18;
	}

	public boolean isNight() {
		return hours < 6;
	}

	public boolean isHour(int hour) {
		return this.hours == hour;
	}

	public boolean isDay(int day) {
		return this.days == day;
	}

	public boolean isFirstHourOfPeriod() {
		return hours == 0 || hours == 6 || hours == 12 || hours == 18;
	}

	public boolean isLastHourOfPeriod() {
		return hours == 23 || hours == 5 || hours == 11 || hours == 15;
	}

	public boolean isHourInRange(int first, int last) {
		return isHourInRange(this.hours, first, last);
	}

	public boolean isHourInRange(int hour, int first, int last) {
		if (first < last) {
			return hour >= first && hour <= last;
		} else //going over midnight
		{
			return hour >= first || hour <= last;
		}
	}

}
