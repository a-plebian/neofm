/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.components;

import fetishmaster.utils.ICopiable;

import java.util.ArrayList;
import java.util.List;

/**
 * @author H.Coder
 */
public class ValueTextDescriptor implements ICopiable<ValueTextDescriptor> {
	private final List<Double> min = new ArrayList<>();
	private final List<Double> max = new ArrayList<>();
	private final List<String> descr = new ArrayList<>();
	private String name = "";

	public ValueTextDescriptor copy() {
		var out = new ValueTextDescriptor();

		out.name = name;

		out.min.addAll(min);
		out.max.addAll(max);
		out.descr.addAll(descr);


		return out;
	}

	public void addDesc(double min, double max, String descr) {
		this.max.add(max);
		this.min.add(min);
		this.descr.add(descr);
	}

	public String getRandomOnValue(double val) {
		List<String> res = new ArrayList<>();
		double minimum, maximum;


		for (int i = 0; i < descr.size(); i++) {
			minimum = this.min.get(i);
			maximum = this.max.get(i);

			if ((minimum <= val) && (maximum >= val)) {
				res.add(descr.get(i));
//                if (GameEngine.devMode)
//                    System.out.println("Range result: "+descr.get(i));
			}
		}

		if (res.isEmpty())
			return "";

		return res.get((int) (Math.random() * res.size()));
	}

	public void removeDesc(int num) {
		if (num < 0 && num > this.descr.size())
			return;

		this.min.remove(num);
		this.max.remove(num);
		this.descr.remove(num);
	}

	public double getMin(int num) {
		return  min.get(num);
	}

	public double getMax(int num) {
		return max.get(num);
	}

	public String getDescr(int num) {
		return descr.get(num);
	}

	public void setMin(int pos, double min) {
		this.min.set(pos, min);
	}

	public void setMax(int pos, double max) {
		this.max.set(pos, max);
	}

	public void setDescr(int pos, String descr) {
		this.descr.set(pos, descr);
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	public int getSize() {
		return descr.size();
	}
}
