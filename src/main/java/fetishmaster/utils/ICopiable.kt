package fetishmaster.utils

/**
 * like Cloneable but not useless because it has a method
 */
interface ICopiable<K> {
	fun copy(): K
	@Suppress("UNCHECKED_CAST")
	fun copyAsICopiable(): ICopiable<K> = copy() as ICopiable<K>
}
