package fetishmaster.utils;

import fetishmaster.engine.GameEngine;
import org.apache.commons.io.FilenameUtils;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class UIHelpers {
	private static final Map<String, String> lastFiles = new HashMap<>();

	public static File openSaveFileChooser(String defaultRelativePath, String fileType, String typeName) {
		String path = lastFiles.getOrDefault(
				fileType,
				GameEngine.gameDataPath + "/" + defaultRelativePath + "/"
		);

		JFileChooser fileChooser = new JFileChooser() {
			@Override
			public void approveSelection() {
				File file = getSelectedFile();
				if (file.exists()) {
					int result = JOptionPane.showConfirmDialog(this, "The file exists, overwrite?", "Existing File", JOptionPane.YES_NO_CANCEL_OPTION);
					switch (result) {
						case JOptionPane.YES_OPTION:
							super.approveSelection();
							return;
						case JOptionPane.CANCEL_OPTION:
							cancelSelection();
							return;
						case JOptionPane.CLOSED_OPTION:
						case JOptionPane.NO_OPTION:
					}
				}
				super.approveSelection();
			}
		};
		fileChooser.setDialogType(JFileChooser.SAVE_DIALOG);
		fileChooser.setFileFilter(new FileNameExtensionFilter(typeName, fileType));
		fileChooser.setCurrentDirectory(new File(path));
		fileChooser.setApproveButtonText("Save");
		int returnValue = fileChooser.showSaveDialog(null);

		switch (returnValue) {
			case JFileChooser.CANCEL_OPTION:
				return null;
			case JFileChooser.APPROVE_OPTION:
				File file = fileChooser.getSelectedFile();
				if (!FilenameUtils.getExtension(file.getName()).equals(fileType)) {
					file = new File(file.getPath() + "." + fileType);
					lastFiles.put(fileType, file.getPath());
				}
				return file;
		}

		return null;
	}

	public static File openLoadFileChooser(String defaultRelativePath, String fileType, String typeName) {
		String path = lastFiles.getOrDefault(
				fileType,
				GameEngine.gameDataPath + "/" + defaultRelativePath + "/"
		);

		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileFilter(new FileNameExtensionFilter(typeName, fileType));
		fileChooser.setCurrentDirectory(new File(path));
		fileChooser.setApproveButtonText("Load");
		int returnValue = fileChooser.showOpenDialog(null);

		switch (returnValue) {
			case JFileChooser.CANCEL_OPTION:
				return null;
			case JFileChooser.APPROVE_OPTION:
				File file = fileChooser.getSelectedFile();
				lastFiles.put(fileType, file.getPath());
				return file;
		}

		return null;
	}

	public static void resetLastFiles() {
		lastFiles.clear();
	}
}
