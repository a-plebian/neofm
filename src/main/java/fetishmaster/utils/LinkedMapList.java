package fetishmaster.utils;

import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.thoughtworks.xstream.converters.collections.MapConverter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Linked Hash Map addressable by index.
 * @param <K> Key
 * @param <V> Value
 */
@XStreamConverter(MapConverter.class)
public class LinkedMapList<K, V> extends LinkedHashMap<K, V> implements Serializable, ICopiable<LinkedMapList<K, V>> {
	private final List<K> keyList = new ArrayList<>();

	@Override
	public LinkedMapList<K, V> copy() {
		var out = new LinkedMapList<K, V>();

		if (isEmpty()) {
			return out;
		}

		var keyIsCopiable = (getKeyAt(0) instanceof ICopiable);
		var valueIsCopiable = (getAt(0) instanceof ICopiable);

		for (K key : keyList) {
			@SuppressWarnings("unchecked")
			var newKey = keyIsCopiable ? ((ICopiable<K>) key).copy() : key;
			@SuppressWarnings("unchecked")
			var newValue = valueIsCopiable ? ((ICopiable<V>) get(key)).copy() : get(key);
			out.put(newKey, newValue);
		}

		return out;
	}

	@Override
	public V put(K key, V value) {
		if(!containsKey(key)) {
			keyList.add(key);
		}
		return super.put(key, value);
	}

	@Override
	public boolean remove(Object key, Object value) {
		keyList.remove(key);
		return super.remove(key, value);
	}

	@Override
	public V remove(Object key) {
		keyList.remove(key);
		return super.remove(key);
	}

	public V putAt(int index, V object) {
		return put(keyList.get(index), object);
	}

	public K getKeyAt(int index) {
		return keyList.get(index);
	}

	public V getAt(int index) {
		return get(keyList.get(index));
	}

	public V removeAt(int index) {
		var key = keyList.get(index);
		keyList.remove(index);
		return remove(key);
	}
}
