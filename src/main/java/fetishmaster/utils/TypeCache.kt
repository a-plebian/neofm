package fetishmaster.utils


class TypeCache(
	val cache: MutableMap<String, ICopiable<*>> = mutableMapOf()
) {
	fun <T : ICopiable<T>> get(string: String): T? {
		return cache[string]?.copy() as T?
	}

	fun <T : ICopiable<T>> put(key: String, t: ICopiable<T>) {
		cache[key] = t.copyAsICopiable()
	}
}
