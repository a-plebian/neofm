@file:JvmName("Profiler")
package fetishmaster.utils

import mu.KotlinLogging

private val logger = KotlinLogging.logger {}

private val profilesTable = mutableMapOf<String, Long>()

fun startProfiling(key: String) {
	if (profilesTable.containsKey(key)) {
		logger.error { "Attempted to start profiling \"$key\" while already profiling it" }
		return
	}

	profilesTable[key] = System.currentTimeMillis()
	logger.debug { "Started profiling \"$key\"" }
}

fun stopProfiling(key: String) {
	if (!profilesTable.containsKey(key)) {
		logger.error { "Attempted to start profiling \"$key\" while not yet profiling it" }
		return
	}

	val timeTook = System.currentTimeMillis() - profilesTable[key]!!
	profilesTable.remove(key)

	logger.debug { "\"$key\" profiling finished: ${timeTook / 1000.0}s ($timeTook ms)" }
}
