/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.utils;

/**
 * @author H.Coder
 */
public class TextNumber {
	private final String data;

	public TextNumber() {
		data = "";
	}

	public TextNumber(String text) {
		data = text;
	}

	public TextNumber(double number) {
		data = Double.toString(number);
	}

	public TextNumber(int number) {
		data = Integer.toString(number);
	}

	public String get() {
		return data;
	}

	public String asText() {
		return data;
	}

	public double asDouble() {
		double ret;
		try {
			ret = Double.parseDouble(data);
		} catch (Exception e) {
			ret = 0;
		}

		return ret;

	}

	public double asInt() {
		int ret;
		try {
			ret = (int) Double.parseDouble(data);
		} catch (Exception e) {
			ret = 0;
		}

		return ret;

	}

	public boolean asBool() {
		return !data.isEmpty();
	}


}

