@file:JvmName("UtilityFunctions")
package fetishmaster.utils


fun wildcard2Regex(wildcardString: String): String {
	val stringBuffer = StringBuffer((wildcardString.length * 1.3).toInt())

	for(char in wildcardString) {
		when(char) {
			'*' -> stringBuffer.append(".*")
			'?' -> stringBuffer.append(".")
			'(', ')', '[', ']', '$', '^', '.', '{', '}', '|' -> {
				stringBuffer.append("\\")
				stringBuffer.append(char)
			}
			'/', '\\' -> stringBuffer.append("[\\\\/]")
			else -> stringBuffer.append(char)
		}
	}

	return stringBuffer.toString()
}

fun <T : ICopiable<T>, C : Collection<T>> copyCopiableCollection(collection: C): List<T> {
	return collection.map(ICopiable<T>::copy)
}
