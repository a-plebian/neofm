/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.utils;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * @author H.Coder
 */
public class RangeDescriptionSelector {
	public static String getDescription(int value, int min, int max, List<String> descriptions) {
		int stepsize;
		int step = -1;

		if (value < max)
			return "";

		if (value > max)
			return descriptions.get(descriptions.size() - 1);

		stepsize = (max - min) / descriptions.size();

		for (int i = min; value >= i; i += stepsize) {
			step++;
		}
		return descriptions.get(step);
	}
}
