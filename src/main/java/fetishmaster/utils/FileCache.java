package fetishmaster.utils;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import com.thoughtworks.xstream.security.AnyTypePermission;
import fetishmaster.bio.DNAGenePool;
import fetishmaster.engine.GameEngine;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileFilter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public final class FileCache {
    private static Logger logger = LoggerFactory.getLogger(FileCache.class);

	private FileCache() {}

	private static final Map<Path, String> fileCache = new HashMap<>();
	private static final Map<String, List<Path>> pathCache = new HashMap<>();
	private static final TypeCache objectCache = new TypeCache();

	public static String getFile(Path path) {
		if (!fileCache.containsKey(path)) {
			if (Files.exists(path)) {
				try {
					fileCache.put(path, Files.readString(path));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		logger.trace("Loading file: {}", path);
		return fileCache.getOrDefault(path, null);
	}

	public static String getFile(String path) {
		return getFile(Paths.get(path));
	}

	public static <T> T getXMLObject(Path path) {
		//TODO: see if I can replace this with jackson in the long run
		// Initial tests implied jackson's serialization is different so some research is needed
		//StaxDriver because it's fast
		XStream xStream = new XStream(new StaxDriver());
		xStream.addPermission(new AnyTypePermission());
		try {
			//XML deserialization is not cached because deserialization creates a deep copy
			//Not a fan of the unchecked cast, but needed with XStream
			@SuppressWarnings("unchecked")
			T t = (T) xStream.fromXML(getFile(path));
			return t;
		} catch (Exception e) {
			logger.error("Failed to load \"{}\"", path, e);
			return null;
		}
	}

	public static <T> T getXMLObject(File file) {
		if (file == null) {
			return null;
		}
		return getXMLObject(file.toPath());
	}

	public static List<Path> getPathsInDir(Path path, String mask) {
		if(!Files.isDirectory(path)) {
			logger.error("Attempted to get the paths in dir of a file \"{}\"", path);
			return new LinkedList<>();
		}
		List<Path> paths = new ArrayList<>();
		var pathKey = path + "|" + ((mask == null) ? "null" : mask);
		if (pathCache.containsKey(pathKey)) {
			paths = pathCache.get(pathKey);
		} else {
			try (var stream = Files.walk(path)){
				String preprocessed = "";
				if (mask != null) {
					preprocessed = mask.replace('\\', '/');
					if (!mask.startsWith("*")) {
						preprocessed = "*" + preprocessed;
					}
				}
				var regex = Pattern.compile(UtilityFunctions.wildcard2Regex(preprocessed));
				paths = stream
						.filter(intPath -> !intPath.toFile().isDirectory())
						.filter(inPath -> {
							if (mask != null) {
								return regex.matcher(inPath.toString()).find();
							} else {
								return true;
							}
						})
						.toList();
			} catch (Exception e) {
				logger.error("Failed to load \"{}\"", path, e);
			}
			pathCache.put(pathKey, paths);
		}

		return paths;
	}

	public static List<Path> getPathsInDir(Path path) {
		return getPathsInDir(path, null);
	}

	public static List<Path> getPathsInDir(String path, String mask) {
		return getPathsInDir(Path.of(path), mask);
	}

	public static List<Path> getPathsInDir(String path) {
		return getPathsInDir(Path.of(path), null);
	}

	@SuppressWarnings("unchecked")
	public static <T> List<T> getDirXMLObjects(String path, String mask) {
		List<Path> paths = getPathsInDir(path, mask);

		return paths.stream().map(
				inPath -> (T) FileCache.getXMLObject(inPath.toAbsolutePath())
		).toList();

	}

	public static <T> List<T> getDirXMLObjects(String path) {
		return getDirXMLObjects(path, null);
	}

	public static <T extends ICopiable<T>> T getGamedata(String relativePath) {
		var path = Path.of(GameEngine.gameDataPath + "/" + relativePath).toAbsolutePath();
		T cachedObject = objectCache.get(path.toString());
		if (cachedObject == null) {
			cachedObject = getXMLObject(path);
			objectCache.put(path.toString(), cachedObject);
		}
		return cachedObject;
	}

	public static <T extends ICopiable<T>> List<T> getGamedataDir(String relativePath, String mask) {
		var pathsInDir = getPathsInDir(GameEngine.gameDataPath + "/" + relativePath, mask);
		return pathsInDir.stream()
				.map(path -> {
					T cachedObject = objectCache.get(path.toAbsolutePath().toString());
					if (cachedObject == null) {
						cachedObject = getXMLObject(path);
						objectCache.put(path.toString(), cachedObject);
					}
					return cachedObject;
				}).toList();
	}

	public static <T extends ICopiable<T>> List<T> getGamedataDir(String relativePath) {
		return getGamedataDir(relativePath, null);
	}

	public static boolean gamedataFileExists(String path) {
		return new File(GameEngine.gameDataPath + "/" + path).exists();
	}

	public static <T> T getSimpleGamedata(String relativePath) {
		return getXMLObject(Path.of(GameEngine.gameDataPath + "/" + relativePath));
	}

	public static void writeXMLObject(String path, Object object, boolean force) {
		//DomDriver because it outputs nicely
		XStream xStream = new XStream(new DomDriver());
		xStream.autodetectAnnotations(true);
		xStream.addPermission(new AnyTypePermission());
		String serialized = xStream.toXML(object);

		Path pathObject = Paths.get(path).toAbsolutePath();

		if (!force && Files.exists(pathObject)) {
			return;
		}

		try {
			FileUtils.writeStringToFile(pathObject.toFile(), serialized);
			fileCache.put(pathObject, serialized);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void writeXMLObject(String path, Object object) {
		writeXMLObject(path, object, false);
	}

	public static void writeXMLObject(File file, Object object, boolean force) {
		writeXMLObject(file.getPath(), object, force);
	}

	public static void writeXMLObject(File file, Object object) {
		if (file == null) return;
		writeXMLObject(file.getPath(), object);
	}

	public static int cacheLength() {
		return fileCache.size();
	}

	public static void clearCache() {
		fileCache.clear();
	}
}
