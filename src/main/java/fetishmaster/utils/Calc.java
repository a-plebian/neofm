/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.utils;

import fetishmaster.bio.Creature;

/**
 * @author H.Coder
 */
public class Calc {

	public static double PlusMinusXProcent(double value, double procent) {
		double tmp = value / 100 * procent;
		double x = Math.random() * tmp * 2;

		return value - tmp + x;
	}

	public static double debugPlusMinusXProcent(double value, double procent) {
		System.out.println("value: " + value + "  procent: " + procent);
		double tmp = value / 100 * procent;
		System.out.println("tmp: " + tmp);

		double x = Math.random() * tmp * 2;
		System.out.println("x: " + x);

		return value - tmp + x;
	}

	public static double MinusXProcent(double value, double procent) {
		double tmp = value / 100 * procent;
		return value - tmp;
	}

	public static double PlusXProcent(double value, double procent) {
		double tmp = value / 100 * procent;
		return value + tmp;
	}

	public static double procent(double value, double procent) {
		double tmp = value / 100 * procent;
		return tmp;
	}

	public static double percent(double value, double percent) {
		double tmp = value / 100 * percent;
		return tmp;
	}

	public static boolean chance(int procent) {
		double n = Math.random() * 100;

		return procent > n;
	}

	public static double getProcent(double max, double current) {
		double one = max / 100;
		double res = current / one;
		return res;
	}

//    public static int getProcent(double max, double current)
//    {
//        double one = max / 100;
//        double res = current / one;
//        return (int)res;
//    }

	public static double getPercent(double max, double current) {
		return getProcent(max, current);
	}

	public static boolean chance(double procent) {
		double n = Math.random() * 100;

		return procent > n;
	}

	public static double random(double x) {
		return Math.random() * x;
	}

	public static int randomInt(double x) {
		double d = Math.random() * x;
		int res = (int) Math.round(d);
		return res;
	}

	public static double BMI(Creature c) {
		double res = 0, height, weight, extra = 0;

		if (c != null) {
			if (c.getRna().hasGene("breasts.milk_volume"))
				extra += c.getStat("breasts.milk_volume");
			if (c.getRna().hasGene("udder.milk"))
				extra += c.getStat("udder.milk");
			if (c.getRna().hasGene("uterus.weight"))
				extra += c.getStat("uterus.weight");
			height = c.getStat("generic.height") / 100;
			weight = (c.getStat("generic.weight") - extra) / 1000;
			res = weight / (height * height);
		}
		//Debug.print(c.getName()+" BMI: "+ res);
		return res;
	}

	public static double WHR(Creature c) {
		double whr;
		double w, h, uv, fv, erd;
		uv = c.getStat("uterus.volume");
		fv = c.getStat("abdomen.food");
		erd = GeometryCalc.VolumeToRadius(uv + fv);
		w = c.getStat("generic.waist") - 2 * Math.PI * erd;
		h = c.getStat("generic.hips");
		whr = w / h;

		return whr;
	}

	public static double setMax(double val, double max) {
		if (val > max)
			val = max;
		return val;
	}

	public static double setMin(double val, double min) {
		if (val < min)
			val = min;
		return val;
	}

	public static double setInRange(double val, double min, double max) {
		val = setMax(val, max);
		val = setMin(val, min);
		return val;
	}

	public static double VaginalStretch(Creature c, double width) {
		double res, cw = c.getStat("vagina.width");

		res = (width - cw) / 10;
		if (res < 0) res = 0;

		return res;
	}

	public static double VaginalStretch(Creature receiver, Creature giver) {
		return VaginalStretch(receiver, giver.getStat("penis.width"));
	}

}
