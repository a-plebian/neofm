/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.utils;

import fetishmaster.bio.*;
import fetishmaster.components.StatEffect;
import fetishmaster.engine.GameEngine;
import fetishmaster.engine.scripts.ScriptVarsIntegrator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;
import java.util.Map;

/**
 * @author H.Coder
 */
public class Debug {
	private static final Logger logger = LoggerFactory.getLogger(Debug.class);

	public static String dislplayPersonalFlags(Creature c) {
		StringBuilder res = new StringBuilder();
		res.append("==========================================\n");
		res.append("Character: ").append(c.getName()).append("\n");
		res.append("============= Personal flags =============\n");
		for(Map.Entry<String, Integer> entry : c.flags.entrySet()) {
			res.append("Flag: ").append(entry.getKey()).append(", ").append(entry.getValue()).append("\n");
		}

		String out = res.toString();
		logger.debug(out);
		return out;
	}

	public static String displayGlobalFlags() {
		StringBuilder res = new StringBuilder();
		Iterator it = GameEngine.activeWorld.flags.keySet().iterator();
		String flag;
		int val;
		res.append("============= Global flags =============\n");
		while (it.hasNext()) {
			flag = (String) it.next();
			val = ScriptVarsIntegrator.GetFlag(flag);
			res.append("Flag: ").append(flag).append(", ").append(val).append("\n");
		}

		res.append("============= Global text flags =============\n");
		it = GameEngine.activeWorld.textFlags.keySet().iterator();
		String tval;
		while (it.hasNext()) {
			flag = (String) it.next();
			tval = ScriptVarsIntegrator.GetTextFlag(flag);
			res.append("Flag: ").append(flag).append(", ").append(tval).append("\n");
		}

		String out = res.toString();
		logger.debug(out);
		return out;
	}


	public static Creature CreateChildFrom(Creature p1, Creature p2) {
		Creature child;
		DNAGenePool child_dna = GeneProcessor.NormalDNACreate(p1.getDNA(), p2.getDNA());
		child = CreatureProcessor.CreateFromDNA("child", child_dna);
		CreatureProcessor.Birth(child);
		return child;
	}

	public static String PrintAllEffects(Creature c) {
		if (c == null)
			return "";

		StringBuilder res = new StringBuilder();

		RNAGenePool rna = c.getRna();
		RNAGene r;
		StatEffect ef;

		res.append("==========================" + "\n");
		res.append("Creature: ").append(c.getName()).append("\n");

		for (int i = 0; i < rna.count(); i++) {
			r = (RNAGene) rna.getGene(i);

			if (r.effectCount() > 0) {
				res.append("-------" + "\n");
				res.append("Gene: ").append(r.getFCName()).append(",  value: ").append(r.getValue()).append("\n");
			}

			for (int j = 0; j < r.effectCount(); j++) {
				ef = r.getEffect(j);
				res.append("Effect: ").append(ef.getName()).append("  val: ").append(ef.getValue()).append("\n");
			}
		}

		String out = res.toString();
		logger.debug(out);
		return out;
	}

	public static void scriptDebug(int code, Object obj) {

		if (code == 0 && obj instanceof Creature) {
			Creature c = (Creature) obj;
			if ("Asami".equals(c.getName()))
				logger.debug("{}", obj);

		}

		if (code == 1 && obj instanceof String) {
			String s = (String) obj;
			logger.debug(s);
			if (s.equals("uterus_birth_offscreen")) {
				logger.debug("{}", obj);
			}
		}

		if (code == 3 && obj != null) {
			logger.debug("{}", obj);
		}

	}

	public static void log(String string) {
		logger.debug(string);
	}

	public static void info(String string) {
		logger.info(string);
	}

	public static void error(String string) {
		logger.error(string);
	}

}
