/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.utils;

import java.util.ArrayList;

/**
 * @author H.Coder
 */
public class ChanceRoulette {
	ArrayList fields = new ArrayList();

	public void addField(Object key, double size) {
		fields.add(new field(key, size));
	}

	public Object getChance() {
		double fullsize = 0, seed;
		field f, pf;

		Object res = null;

		for (Object fl : fields) {
			f = (field) fl;
			fullsize += f.size;
		}

		seed = Math.random() * fullsize;

		fullsize = 0;

		if (fields.isEmpty())
			return null;
		pf = (field) fields.get(0);
		for (Object fl : fields) {
			f = (field) fl;
			fullsize += f.size;

			if (fullsize > seed) {
				res = pf.key;
				break;
			}
			pf = f;

		}
		return res;
	}

	public int fieldsCount() {
		return fields.size();
	}

	class field {
		protected double size;
		Object key;

		private field(Object key, double size) {
			this.key = key;
			this.size = size;
		}
	}
}
