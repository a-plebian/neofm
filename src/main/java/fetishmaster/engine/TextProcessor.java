/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.engine;

import fetishmaster.bio.ConstSex;
import fetishmaster.bio.Creature;
import fetishmaster.bio.DNAGene;
import fetishmaster.bio.RNAGene;
import fetishmaster.components.ValueTextDescriptor;
import fetishmaster.engine.backgrounds.BackgroundsManager;
import fetishmaster.engine.scripts.ScriptEngine;
import fetishmaster.utils.FileCache;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * @author H.Coder
 */
public class TextProcessor {

	private static final Map<String, ValueTextDescriptor> texts = new HashMap<>();
	private static final HashMap templates = new HashMap();
	private static List<String> mnames = new ArrayList<>();
	private static List<String> fnames = new ArrayList<>();

	public static void init() {
		List<ValueTextDescriptor> descriptionList = FileCache.getGamedataDir("descriptions", "*.descr");
		String subname;

		for (ValueTextDescriptor vtd : descriptionList) {
			texts.put(vtd.getName(), vtd);
		}

//        l = fileXML.LoadXMLsFromMask("templates", "*.tpl");
//        
//        for (i = 0; i < l.size(); i+=2)
//        {
//            subname = (String) l.get(i);
//            subname = subname.replace(".tpl", "");
//            
//            templates.put(subname, l.get(i+1));
//        }

		fnames = FileCache.getSimpleGamedata("texts/fnames.xml");
		mnames = FileCache.getSimpleGamedata("texts/mnames.xml");
	}

	public static String getRandomName(int sex) {
		List<String> namespool;

		if (sex == ConstSex.NONE || sex == ConstSex.MALE) {
			namespool = mnames;
		} else {
			namespool = fnames;
		}

		int i = (int) (namespool.size() * Math.random());

		return namespool.get(i);
	}

	public static TextTemplate getTemplate(String name) {
		TextTemplate t = FileCache.getGamedata("templates/" + name + ".tpl");
		if (t == null) {
			return new TextTemplate("");
		}

		return t;
	}

	//    public static TextTemplate getRandomTemplate(String name)
//    {
//        ArrayList names = new ArrayList();
//        Collection col = templates.keySet();
//        Iterator it = col.iterator();
//        String key;
//        int rand;
//                
//        while (it.hasNext())
//        {
//            key = (String) it.next();
//            if(key.startsWith(name))
//            {
//                names.add(key);
//            }
//           
//        }
//        
//        if (names.isEmpty())
//            return new TextTemplate("");
//        
//        rand = (int) (Math.random()*names.size());
//        
//        return (TextTemplate) templates.get(names.get(rand));
//    }

	public static TextTemplate getRandomTemplate(String name, WalkFrame frame) {
		var tpls = getAllTemplates(name, frame);

		if (tpls.isEmpty()) {
			return new TextTemplate("");
		}

		var rand = new Random();
		return tpls.get(rand.nextInt(tpls.size()));

	}

	public static List<TextTemplate> getAllTemplates(String name, WalkFrame frame) {
		List<TextTemplate> tpls = FileCache.getGamedataDir("templates", name + "*.tpl");

		tpls = selectGoodTemplates(tpls, frame);
		tpls = onlyMaxPriorityTemplates(tpls);

		return tpls;
	}

	public static List<TextTemplate> selectGoodTemplates(List<TextTemplate> tpls, WalkFrame frame) {
		//performance short circuit
		if(tpls.isEmpty()) {
			return tpls;
		}

		return tpls
				.stream()
				.filter(template -> ScriptEngine.processConditionsScript(template.getConditions(), frame.getVarsContext()))
				.collect(Collectors.toList());
	}

	public static List<TextTemplate> onlyMaxPriorityTemplates(List<TextTemplate> tpls) {
		//performance short circuit
		if (tpls.size() <= 1) {
			return tpls;
		}

		AtomicInteger highest = new AtomicInteger();
		return tpls.stream()
				.sorted(Comparator.comparing(TextTemplate::getPriority))
				.takeWhile(template -> {
					if (template.getPriority() < highest.get()) {
						return false;
					} else {
						highest.set(template.getPriority());
						return true;
					}
				})
				.collect(Collectors.toList());
	}

	//    public static String GetCreatureDescription(Creature c)
//    {
//        String res = "";
//        
//        if (c == null)
//            return res;
//        
//        TextTemplate tp = getTemplate("visuals/full_description");
//        res = ScriptEngine.processCreatureScript(c, tp.getText());
//        
//        return res;
//    }
	public static String getDescr(String tag, double value) {
		ValueTextDescriptor vtd = texts.get(tag);

		if (vtd == null) {
			return "";
		}

		return vtd.getRandomOnValue(value);
	}

	public static String getRangeDesc(String template, double value) {
		ValueTextDescriptor vtd = FileCache.getGamedata("descriptions/" + template + ".descr");

		if (vtd == null) {
			return "";
		}

//        if (GameEngine.devMode)
//            System.out.println("Loaded range description template: "+ vtd.getName());

		return vtd.getRandomOnValue(value);
	}

	public static String HimHer(Creature c) {
		if (c == null) {
			return "";
		}


		RNAGene s = c.getRNAGene(DNAGene.SEX);
		switch ((int) s.getValue()) {
			case Creature.NONE: return "them";
			case Creature.MALE: return "him";
			case Creature.FEMALE:
			case Creature.FUTA: return "her";
		}
		return "";
	}

	public static String HisHer(Creature c) {
		String res = "";

		if (c == null) {
			return res;
		}

		RNAGene s = c.getRNAGene(DNAGene.SEX);

		if (s.getValue() == Creature.NONE) {
			res = "his";
		}
		if (s.getValue() == Creature.MALE) {
			res = "his";
		}
		if (s.getValue() == Creature.FEMALE) {
			res = "her";
		}
		if (s.getValue() == Creature.FUTA) {
			res = "her";
		}

		return res;
	}

	public static String HisHers(Creature c) {
		String res = "";

		if (c == null) {
			return res;
		}

		RNAGene s = c.getRNAGene(DNAGene.SEX);

		if (s.getValue() == Creature.NONE) {
			res = "his";
		}
		if (s.getValue() == Creature.MALE) {
			res = "his";
		}
		if (s.getValue() == Creature.FEMALE) {
			res = "hers";
		}
		if (s.getValue() == Creature.FUTA) {
			res = "hers";
		}

		return res;
	}

	public static String HeShe(Creature c) {
		String res = "";

		if (c == null) {
			return res;
		}

		RNAGene s = c.getRNAGene(DNAGene.SEX);

		if (s.getValue() == Creature.NONE) {
			res = "he";
		}
		if (s.getValue() == Creature.MALE) {
			res = "he";
		}
		if (s.getValue() == Creature.FEMALE) {
			res = "she";
		}
		if (s.getValue() == Creature.FUTA) {
			res = "she";
		}

		return res;
	}

	public static String CapitalizeFirst(String text) {
		String res;
		String first;
		String other = "";

		if (text.length() < 1) {
			return text;
		}

		first = text.substring(0, 1);

		if (text.length() > 1) {
			other = text.substring(1);
		}

		first = first.toUpperCase();

		res = first + other;

		return res;
	}

	public static String GenerateCharacterDescription(Creature c, String template) {
		WalkFrame wf = new WalkFrame();
		ScriptEngine.loadVars(wf.getVarsContext(), c, null);
		String s;
		//TextTemplate t = TextProcessor.getRandomTemplate(template, wf);
		synchronized (BackgroundsManager.notifyer) {
			WalkEngine.setSpecialIncludeFrame(wf);
			s = WalkEngine.processInclude(template, wf);//= ScriptEngine.parseForScripts(t.getText(), wf.getVarsContext());
			WalkEngine.clearSpecialIncludeFrame();
		}

		return s;
	}
}
