/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.engine;

import fetishmaster.bio.Creature;
import fetishmaster.bio.CreatureProcessor;
import fetishmaster.bio.DNAGene;
import fetishmaster.bio.RNAGene;
import fetishmaster.display.JFrameMasterWindow;
import fetishmaster.display.WalkButton;
import fetishmaster.display.gamewindow.JDialogItemShop;
import fetishmaster.engine.backgrounds.BackgroundsManager;
import fetishmaster.engine.scripts.ScriptEngine;
import fetishmaster.items.Item;
import fetishmaster.items.ItemProcessor;
import fetishmaster.items.ItemShop;
import fetishmaster.utils.Debug;
import fetishmaster.utils.FileCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.*;

/**
 * @author H.Coder
 */
//Full static class for the walking.
public class WalkEngine {

	private static final Logger logger = LoggerFactory.getLogger(WalkEngine.class);

	//private static LinkedBlockingDeque<WalkFrame> framesStack = new LinkedBlockingDeque<WalkFrame>();
	//public static String savedText = null;
	//public static int ReturnPointsCount = 0;
	private static final int nextHourPoints = 15;
	//private static String forcedEvent = null;
	public static Map<Long, WalkFrame> frameDB = Collections.synchronizedMap(new HashMap<Long, WalkFrame>());
	//private static String currentIState;
	//public static Map interactionStates = Collections.synchronizedMap(new HashMap());
	public static ItemShop itShop = null;
	private static WalkFrame currentFrame = null;
	private static WalkFrame specialIncludeFrame = null;


	public static void clearSpecialIncludeFrame() {
		frameDB.remove(Thread.currentThread().getId());
		specialIncludeFrame = null;
	}

	public static void setSpecialIncludeFrame(WalkFrame frame) {
		frameDB.put(Thread.currentThread().getId(), frame);
		//specialIncludeFrame = frame;
	}

	public static void initFirstTurn(String location) {
		NewEvent(location, null);
	}

	public static void forceEvent(String location) {
		WalkFrame fr = currentFrame;
		if (fr == null)
			fr = new WalkFrame();

		fr.setForcedEvent(location);
	}

	public static void NewEvent(String newLocation, WalkFrame oldFrame) {
		WalkEvent nev;
		WalkFrame frame;
		if (oldFrame == null) {
			//framesStack.clear();
			frame = new WalkFrame();
			frame.setActor(GameEngine.activeWorld.activePartner);
			//VarContext vars = new VarContext();
			ScriptEngine.loadVars(frame);
			//frame.setVarsContext(vars);
		} else {
			frame = oldFrame.clone();
			ScriptEngine.loadVars(frame);
		}

		nev = loadEvent(newLocation, frame);
		DoTurn(nev, oldFrame);
	}

	public static void DoTurn(WalkEvent newEvent, WalkFrame oldFrame) {
		WalkFrame frame = null;
		if (oldFrame == null || oldFrame != currentFrame) {
			//framesStack.clear();
			frame = new WalkFrame();
			frame.setActor(GameEngine.activeWorld.activePartner);
			//VarContext vars = new VarContext();
			ScriptEngine.loadVars(frame);
			//frame.setVarsContext(vars);
		} else if (oldFrame.isProcessed()) {
			frame = oldFrame.clone();
			ScriptEngine.loadVars(frame);
		}

		if (frame == null) {
			logger.error("Frame initialization failed. This is an engine error!");
			frame = new WalkFrame();
			frame.setActor(GameEngine.activeWorld.activePartner);
			ScriptEngine.loadVars(frame);
		}

		frame.setForcedEvent(null);
		frame.setEvent(newEvent);
		frame.setProcessed(false);

		frame = processFrame(frame);
		if (frame.getForcedEvent() != null) {
			NewEvent(frame.getForcedEvent(), frame);
			return; // displayFrame will be called after returning from recursion.
		}
		displayFrame(frame);
	}

	public static void displayFrame(WalkFrame frame) {
		JFrameMasterWindow w = GameEngine.activeMasterWindow;
		if (w == null) {
			logger.error("Can't access main window");
			return;
		}

		clearPersonageImage();
		clearWalkImage();
		clearWalkText();
		clearWalkButtons();

		if (frame.isInteraction() && frame.getTmpImagePatch() != null) {
			displayWalkImage(GameEngine.gameDataPath + "/img/" + frame.getTmpImagePatch());
		} else {
			displayWalkImage(GameEngine.gameDataPath + "/img/" + frame.getImagePath());
		}
		displayPersonageWalkImage(GameEngine.gameDataPath + "/img/" + frame.getCharImgPath());
		displayWalkText(frame);
		displayFrameButtons(frame);
		displayShopIfReady(frame);

		if (frame.getScriptWindow() != null) {
			frame.getScriptWindow().display(w);
			frame.setScriptWindow(null);
		}
	}

	public static void addWalkButton(String text, String value) {
		addWalkButton(text, value, null, currentFrame);
	}

	public static void addWalkButton(String text, String value, Object scriptObject, WalkFrame frame) {
		WalkEvent ev = loadEvent(value, frame);
		if (ev == null)
			return;
		final WalkButton but = new WalkButton(text, ev);
		but.setScriptObject(scriptObject);
		frame.getButtons().add(but);
	}

	public static void addButtonFromScript(String text, String value) {
		addButtonFromScript(text, value, null);
//        if (!currentFrame.getEvent().hasChoice(text))
//        {
//            currentFrame.getEvent().addChoice(new WalkChoice(text, value, null));
//        }
	}

	public static void addButtonFromScript(String caption, String event, Object scriptObject) {
		WalkEvent ev = loadEvent(event, currentFrame);
		if (ev == null)
			return;
		final WalkButton but = new WalkButton(caption, ev);
		but.setScriptObject(scriptObject);
		currentFrame.getScriptButtons().add(but);
//        if (!currentFrame.getEvent().hasChoice(caption))
//        {
//            currentFrame.getEvent().addChoice(new WalkChoice(caption, event, scriptObject));
//        }
	}

	public static void clearWalkButtons() {
		GameEngine.activeMasterWindow.walkControls.removeAll();
		GameEngine.activeMasterWindow.walkControls.revalidate();
		GameEngine.activeMasterWindow.walkControls.repaint();
	}

	public static void displayWalkText(WalkFrame frame) {
		GameEngine.activeMasterWindow.setWalkText(frame.getEventFinalText());
		GameEngine.activeMasterWindow.getWalkTextPane().moveCaretPosition(0);
		GameEngine.activeMasterWindow.getWalkTextPane().select(0, 0);
		GameEngine.activeMasterWindow.refreshWindow();
	}

	public static void clearWalkText() {
		GameEngine.activeMasterWindow.setWalkText("");
	}

	public static void addWalkText(String text) {
		GameEngine.activeMasterWindow.setWalkText(GameEngine.activeMasterWindow.getWalkText() + text);
	}

	public static void walkButtonPressed(WalkButton button) {
		//loadEvent(button.value, button.getScriptObject());
		logger.debug("Selected event: {}", button.getEvent().getName());
		WalkFrame f = currentFrame;
		f.setPressedObject(button.getScriptObject());
		DoTurn(button.getEvent(), f);
	}

	public static void displayWalkImage(String img_path) {
		File f = new File(img_path);
		if (!f.isFile()) {
			f = new File(GameEngine.gameDataPath + "/img/empty_background.jpg");
		}

		if (!f.isFile()) {
			return;
		}

		GameEngine.activeMasterWindow.walkImage.loadImage(f);

	}

	public static void displayPersonageWalkImage(String img_path) {

		if (img_path == null) {
			return;
		}


		File f = new File(img_path);

		if (!f.isFile()) {
			return;
		}

		GameEngine.activeMasterWindow.walkImage.setPersonage(f);
		GameEngine.activeMasterWindow.walkImage.resizeToCurrent();
		GameEngine.activeMasterWindow.walkImage.repaint();
	}

	public static void clearWalkImage() {
		GameEngine.activeMasterWindow.walkImage.cleanImages();
		//GameEngine.activeMasterWindow.walkImage.removeAll();
		//GameEngine.activeMasterWindow.walkImage.revalidate();
		//GameEngine.activeMasterWindow.walkImage.repaint();
	}

	public static void clearPersonageImage() {
		GameEngine.activeMasterWindow.walkImage.cleanImages();
	}

	public static void setState(String state, WalkFrame frame) {
		if (!GameEngine.activeWorld.inInteractionMode) {
			return;
		}

		logger.debug("Setting walkengine state to \"{}\"", state);
		if (frame.getInteractionStates().containsKey(state)) {
			frame.setForcedEvent((String) frame.getInteractionStates().get(state));
			frame.setCurrentIState(frame.getForcedEvent());
			frame.getEvent().getChoices().clear();
			logger.debug("Forced event (from interaction state) is \"{}\"", frame.getForcedEvent());
		}
	}

	public static void changeState(String state, WalkFrame frame) {
		if (!GameEngine.activeWorld.inInteractionMode) {
			return;
		}
		logger.debug("Interaction mode is changing state: {}", state);

		if (frame.getInteractionStates().containsKey(state)) {
			frame.setCurrentIState((String) frame.getInteractionStates().get(state));
			logger.debug("State event changed to: {}", frame.getCurrentIState());
		}
	}

	public static WalkEvent loadEvent(String name, WalkFrame frame) {
		if (frame == null) {
			frame = new WalkFrame();
		}

		WalkEvent we;
		List<WalkEvent> l, n;

		synchronized (BackgroundsManager.notifyer) {
			//String oldnext = null;

			int i;

			l = FileCache.getGamedataDir("events", name + "*.walk");
			// for now just hang, but this need to be resolved
			if ((l == null) || l.isEmpty()) {
				return null;
			}

			l = SelectGoodConditionsEvents(l, frame);
		}

		l = GetMaxPriorityEventsFromList(l);

		// for now just hang, but this need to be resolved
		if ((l == null) || l.isEmpty()) {
			return null;
		}

		we = finalEventSelection(l);

		return we;

	}

	public static WalkFrame processFrame(WalkFrame frame) {
//        synchronized (BackgroundsManager.notifyer)
//        {
		//ScriptEngine.newEventVars();
		WalkEvent event = frame.getEvent();
		if (event == null) {
			logger.error("No event in frame! Critical error!");
			return frame;
		}

		if (!frame.isInteraction()) // loading new variables.
		{
			ScriptEngine.loadVars(frame);
		}
		CreatureProcessor.resetFetishes(frame.getActor());

		if (frame.isInteraction()) {
			CreatureProcessor.interactionStatsRecheck(frame.getActor());
			CreatureProcessor.interactionStatsRecheck(frame.getInteractionActor());
		}


		if (!frame.isInteraction()) {
			try {
				if (currentFrame != null) {
					frame.getFramesStack().put(currentFrame); //current frame go in the stack if it not interactive.
				}
			} catch (InterruptedException ex) {
				logger.error("Syncrhonized frame stack interrupted", ex);
			}
		}
		currentFrame = frame; //we setting this frame as current

		//temp memory, if images changed by scripts
		String timg = frame.getImagePath(), tcimg = frame.getCharImgPath();
		frame.setCharImgChanged(false);
		frame.setImgChanged(false);
		frame.setTmpImagePatch(null);

		//Scripts executed here!
		event.parsedDesc = ScriptEngine.parseForScripts(event.getDescr(), frame.getVarsContext());
		frame.addText(event.parsedDesc);
		GameEngine.activeWorld.addHours(event.getTime());

		//images processing init
		if (!frame.isInteraction() && !frame.isImgChanged() && (timg != null && timg.equals(frame.getImagePath()))) {
			frame.setImagePath(null);
		}

		if (!frame.isInteraction() && (!frame.isCharImgChanged())) {
			frame.setCharImgPath(null);
		}


		//changin image if it not match previous in interaction mode.
//        if (frame.isInteraction() && timg != null && !timg.equals("") && !timg.equals(event.getPicturePath()))
//        {
//            frame.setImagePath(event.getPicturePath());
//        }

		//if image is clean - use event image.
		if (event.getPicturePath() != null && !event.getPicturePath().equals("") && frame.getImagePath() == null) {
			frame.setImagePath(event.getPicturePath());
		}

		if (frame.isInteraction() && event.getPicturePath() != null && !event.getPicturePath().equals("")) {
			frame.setTmpImagePatch(event.getPicturePath());
		}

		if (event.isReturnPoint()) {
			//ReturnPointsCount++; //now it's done by cloning frames
			frame.setLastReturnPointName(event.getName());
		}
		if (frame.getTimeFromReturnPoint() > nextHourPoints) {
			if (!GameEngine.InInteractionMode()) {
				GameEngine.activeWorld.nextHour(false);
				Creature c = frame.getActor();
				if (c != null) {
					c.addStat("generic.tiredness", 5);
				}
				GameEngine.activeMasterWindow.nextHourUpdate();
				frame.setTimeFromReturnPoint(0);
			}
		}

		if (frame.getActor() != null && !event.isBypassInternalChecks()) {
			frame.getActor().addStat(DNAGene.TIREDNESS, event.getTime());
		}

		if (event.isReturnPoint()) {
			frame.setLastReturnPointName(event.getName());
		}

		doCriticalChecks(frame);
		createWalkButtons(frame);

		//check if image still not present
		if (frame.getImagePath() == null) {
			frame.setImagePath("");
		}

		frame.setProcessed(true);
		//}

		if (currentFrame != frame) {
			logger.debug("Event: {}; Overridden by: {}", frame.getEvent().getName(), currentFrame.getEvent().getName());
			return currentFrame;
		}

		return frame;
	}

//    public static void displayEvent(WalkFrame frame)
//    {
//        clearWalkButtons();
//
//        if (frame.getButtons().isEmpty())
//        {
//            if (GameEngine.activeWorld.lastEvent != null && GameEngine.activeWorld.inInteractionMode == false)
//            {
//                addWalkButton("Continue", GameEngine.activeWorld.lastEvent);
//            } else
//            {
//                addWalkButton("Continue", frame.getCurrentIState());
//            }
//        }
//
//        displayFrameButtons(frame);
//
//        displayPersonageWalkImage(frame.getCharImgPath());
//
//        displayWalkImage(GameEngine.gameDataPath + "/img/" + frame.getImagePath());

//        if (savedText == null)
//        {
//            addWalkText(we.parsedDesc);
//        } else
//        {
//            addWalkText(we.parsedDesc + savedText);
//            savedText = null;
//        }
//
//        displayWalkText(frame);
//
//
//        if (frame.getForcedEvent() != null)
//        {
//            loadEvent(frame.getForcedEvent(), null);
//            //return;
//        }

//        if (!GameEngine.activeWorld.inInteractionMode)
//        {
//            if (we.isBypassInternalChecks() == false)
//            {
//                checkTiredness();
//                checkHealth();
//                checkMobility();
//            }
//        }


	//    }
	private static void doCriticalChecks(WalkFrame wf) {
		if (wf.getActor() == null)
			return;
		if (wf.getEvent().isBypassInternalChecks())
			return;

		WalkFrame tf = new WalkFrame();
		ScriptEngine.loadVars(tf);
		ScriptEngine.loadVars(tf.getVarsContext(), wf.getActor(), null);
		tf.setActor(wf.getActor());
		String res = processInclude("system/proxy_walking_check", tf);

		if (res != null && !res.isEmpty()) {
			ArrayList<WalkButton> a;
			a = (ArrayList<WalkButton>) tf.getButtons();
			a.addAll(tf.getScriptButtons());
			wf.setButtons(tf.getScriptButtons());
		}

		return;
	}

	private static void checkTiredness() {
		if (GameEngine.activeWorld.activePartner == null) {
			return;
		}

		Creature c = GameEngine.activeWorld.activePartner;

		if (c.getRNAValue(DNAGene.TIREDNESS) >= c.getRNAValue(DNAGene.MAXTIREDNESS)) {
			c.setStat(DNAGene.TIREDNESS, c.getRNAValue(DNAGene.MAXTIREDNESS) - 1);
			clearWalkButtons();
			addWalkButton("Continue", "system_events/maxed_tiredness");
		}
	}

	private static void checkHealth() {
		if (GameEngine.activeWorld.activePartner == null) {
			return;
		}

		Creature c = GameEngine.activeWorld.activePartner;

		if (c.getRNAValue(DNAGene.HEALTH) <= 0) {
			c.setStat(DNAGene.HEALTH, c.getRNAValue(DNAGene.MAXHEALTH));
			clearWalkButtons();
			addWalkButton("Continue", "system_events/life_zero");
		}
	}

	public static List<WalkEvent> SelectGoodConditionsEvents(List<WalkEvent> events, WalkFrame frame) {
		if (events == null) {
			return null;
		}
		if (events.isEmpty()) {
			return null;
		}

		ArrayList selected = new ArrayList();
		WalkEvent e;
		int i;
		String res;


		for (i = 0; i < events.size(); i++) {
			e = events.get(i);

			if (isConditionGood(e, frame)) {
				selected.add(e);
			}
		}

		if (selected.isEmpty()) {
			return null;
		}

		return selected;
	}

	public static boolean isConditionGood(WalkEvent we, WalkFrame frame) {
		if (we.pasedConditions == WalkEvent.COND_BAD) {
			return false;
		}
		if (we.pasedConditions == WalkEvent.COND_GOOD) {
			return true;
		}

		boolean res;

		res = ScriptEngine.processConditionsScript(we.getConditions(), frame.getVarsContext());
		if (res) {
			we.pasedConditions = WalkEvent.COND_GOOD;
			return true;
		}

		we.pasedConditions = WalkEvent.COND_BAD;
		return false;
	}

	public static ArrayList SortEventsOnPriority(ArrayList events) {
		if ((events == null) || events.isEmpty()) {
			return null;
		}

		ArrayList sorted = new ArrayList();
		WalkEvent e;
		int maxprio;
		int mppos;
		int i;

		while (!events.isEmpty()) {
			maxprio = 0;
			mppos = 0;

			for (i = 0; i < events.size(); i++) {
				e = (WalkEvent) events.get(i);
				if (maxprio < e.getPriority()) {
					maxprio = e.getPriority();
					mppos = i;
				}

			}

			e = (WalkEvent) events.get(mppos);
			sorted.add(e);
			events.remove(mppos);
		}

		return sorted;
	}

	/**
	 * Retun array with walkevent with max priority
	 *
	 * @param events
	 * @return
	 */
	public static List<WalkEvent> GetMaxPriorityEventsFromList(List<WalkEvent> events) {
		if ((events == null) || events.isEmpty()) {
			return null;
		}

		ArrayList selected = new ArrayList();
		WalkEvent e;
		int maxprio = 0;
		int mppos = 0;
		int i;

		for (i = 0; i < events.size(); i++) {
			e = events.get(i);
			if (maxprio < e.getPriority()) {
				maxprio = e.getPriority();
			}

		}

		for (i = 0; i < events.size(); i++) {
			e = events.get(i);
			if (maxprio == e.getPriority()) {
				selected.add(e);
			}
		}

		return selected;
	}

	public static WalkEvent finalEventSelection(List<WalkEvent> events) {
		if ((events == null) || events.isEmpty()) {
			return null;
		}

		ArrayList<WalkEvent> merged = new ArrayList<WalkEvent>();

		WalkEvent e;
		int i;
		for (i = 0; i < events.size(); i++) {
			e = events.get(i);
			if (e.isMergeDown())
				merged.add(e);
		}

		events.removeAll(merged);

		//if no normal events for the base, but merged exists, we take first merged as base.
		if (events.isEmpty() && !merged.isEmpty()) {
			events.add(merged.get(0));
			merged.remove(0);
		}


		int num;

		num = (int) (Math.random() * events.size());

		e = events.get(num);

		for (i = 0; i < merged.size(); i++) {
			e.mergeEvent(merged.get(i));
		}

		return e;
	}

	public static void createWalkButtons(WalkFrame frame) {
		if (frame == null) {
			return;
		}
		WalkEvent event = frame.getEvent();

		ArrayList choices = new ArrayList();
		List<WalkEvent> events = null;
		WalkChoice wc;
		boolean eventsGood;
		int i, i1;

		if (event.getChoicesCount() == 0 && frame.getScriptButtons().isEmpty()) {
			if (frame.isInteraction()) {
				event.addChoice(new WalkChoice("Continue", frame.getCurrentIState(), null));
			} else {
				event.addChoice(new WalkChoice("Continue", frame.getLastReturnPointName(), null));
			}
		}

		for (i = 0; i < event.getChoicesCount(); i++) {
			wc = event.getChoice(i);

			events = FileCache.getGamedataDir("events", wc.getValue() + "*.walk");
			if (events.isEmpty()) {
				logger.debug("Did not find an event matching UID \"{}\"", wc.getValue());
				continue;
			}
			events = SelectGoodConditionsEvents(events, frame);

			if ((events != null) && (events.size() > 0)) {
				addWalkButton(wc.getName(), wc.getValue(), wc.getScriptObject(), frame);
			}
		}

		// Will repair situation if all possible events has bad conditions. Just returning to last return point.
		if (frame.getButtons().isEmpty() && frame.getScriptButtons().isEmpty()) {
			addWalkButton("Continue", frame.getLastReturnPointName());
		}

	}

	private static void checkMobility() {
		if (GameEngine.activeWorld.activePartner == null) {
			return;
		}

		Creature c = GameEngine.activeWorld.activePartner;

		double strength = c.getRNAValue(DNAGene.STR) * 1.5 * 1000;
		double weight = c.getRNAValue("generic.weight");
		double cleanWeight = c.getCleanRNAValue("generic.weight");

		if ((strength + cleanWeight) > weight) {
			logger.debug("\"{}\" lost mobility; current weight: {}; max body weight: {}", c.getName(), weight, (strength + cleanWeight));

			clearWalkButtons();
			addWalkButton("Continue", "system_events/lost_mobility");
		}
	}

	//    public static void dispalyShopWindow()
//    {
//        if (itShop == null)
//        {
//            return;
//        }
//
//        JDialogItemShop jis = new JDialogItemShop(GameEngine.activeMasterWindow, true);
//        jis.jPanelItemShop.setShop(itShop);
//        jis.AlignCenter(GameEngine.activeMasterWindow);
//        jis.setVisible(true);
//    }
	public static void pushEvent() {
		pushEvent(currentFrame);
	}

	public static void pushEvent(WalkFrame e) {
		GameEngine.activeWorld.eventsOnHold.addEvent(e.getEvent());
	}

	public static WalkEvent popEvent() {
		return GameEngine.activeWorld.eventsOnHold.getEvent();
	}

	public static String processInclude(String template, WalkFrame frame) {
		TextTemplate tt = TextProcessor.getRandomTemplate(template, frame);

		synchronized (BackgroundsManager.notifyer) {

			if (tt.getPicturePath() != null) {
				if (frame.isInteraction() && tt.getPicturePath() != null && !tt.getPicturePath().equals("")) {
					frame.setTmpImagePatch(tt.getPicturePath());
				} else if (!tt.getPicturePath().equals("")) {
					frame.setImagePath(tt.getPicturePath());
				}
			}

			if (tt.isOverrideChoices()) {
				frame.getButtons().clear();
				//frame.getScriptButtons().clear();
				frame.getEvent().getChoices().clear();
			}

			int i;
			WalkChoice wc;
			for (i = 0; i < tt.getChoicesCount(); i++) {
				wc = tt.getChoice(i);
				addButtonFromScript(wc.getName(), wc.getValue());
			}
		}
		return ScriptEngine.parseForScripts(tt.getText(), frame.getVarsContext());
	}

	public static String processIncludeAll(String template, WalkFrame frame) {
		List<TextTemplate> ttl = TextProcessor.getAllTemplates(template, frame);
		TextTemplate tt;
		Iterator it = ttl.iterator();
		StringBuilder text = new StringBuilder();

		while (it.hasNext()) {

			tt = (TextTemplate) it.next();

			if (tt.getPicturePath() != null) {
				if (!tt.getPicturePath().equals("")) {
					frame.setImagePath(tt.getPicturePath());

				}
			}

			if (tt.isOverrideChoices()) {
				frame.getButtons().clear();
			}

			int i;
			WalkChoice wc;
			for (i = 0; i < tt.getChoicesCount(); i++) {
				wc = tt.getChoice(i);
				addButtonFromScript(wc.getName(), wc.getValue());
			}

			text.append(ScriptEngine.parseForScripts(tt.getText(), frame.getVarsContext()));

		}

		return text.toString();
	}

	public static WalkFrame getCurrentFrame() {
		if (specialIncludeFrame != null)
			return specialIncludeFrame;

		return currentFrame;
	}

	private static void displayFrameButtons(WalkFrame frame) {

		List<WalkButton> bts = frame.getButtons();

		int i;
		for (i = 0; i < bts.size(); i++) {
			WalkButton b = bts.get(i);
			GameEngine.activeMasterWindow.walkControls.add(b);
			b.setVisible(true);
		}

		bts = frame.getScriptButtons();

		for (i = 0; i < bts.size(); i++) {
			WalkButton b = bts.get(i);
			GameEngine.activeMasterWindow.walkControls.add(b);
			b.setVisible(true);
		}

		GameEngine.activeMasterWindow.walkControls.revalidate();
	}

	private static void displayShopIfReady(WalkFrame frame) {
		if (itShop == null)
			return;
		JDialogItemShop shop = new JDialogItemShop(GameEngine.activeMasterWindow, true);
		shop.AlignCenter(GameEngine.activeMasterWindow);
		shop.jPanelItemShop.setShop(itShop);
		shop.setVisible(true);
		itShop = null;
	}

	public static void createItemShop(Creature customer) {
		ItemShop is = new ItemShop();

		is.setCustomer(customer);
		WalkEngine.itShop = is;
	}

	public static void addItemToShop(Item it) {
		if (WalkEngine.itShop == null)
			return;

		if (it == null)
			return;

		WalkEngine.itShop.addToSellList(it);
	}

	public static void addItemToShop(String itemName) {
		if (WalkEngine.itShop == null)
			return;

		Item it;

		it = ItemProcessor.loadItem(itemName);

		if (it == null)
			return;

		WalkEngine.itShop.addToSellList(it);
	}

	public static void toInteractionMode(String event, WalkFrame frame) {
		frame.getInteractionStates().clear();
		GameEngine.activeWorld.inInteractionMode = true;
		GameEngine.activeMasterWindow.jButton_disconnectLink.setEnabled(false);
		GameEngine.activeMasterWindow.jButton_inventory.setEnabled(false);
		//WalkEngine.pauseEvent = true;
		//WalkEngine.forcedEvent = event;
		frame.setInteraction(true);
		frame.setCurrentIState(event);
	}

	public static void fromInteractionMode(WalkFrame frame) {
		GameEngine.activeWorld.inInteractionMode = false;
		GameEngine.activeMasterWindow.jButton_disconnectLink.setEnabled(true);
		GameEngine.activeMasterWindow.jButton_inventory.setEnabled(true);
		GameEngine.activeWorld.interactionCreature = null;
		GameEngine.activeWorld.currentEnemy = null;
		frame.setInteractionActor(null);
		frame.setInteraction(false);
		frame.getInteractionStates().clear();
		//WalkEngine.resumeEvent = true;
	}
}
