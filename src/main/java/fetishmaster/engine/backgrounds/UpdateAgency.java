/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.engine.backgrounds;

import fetishmaster.engine.GameEngine;
import fetishmaster.utils.Calc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author H.Coder
 */
public class UpdateAgency implements Runnable {
	private static final Logger logger = LoggerFactory.getLogger(UpdateAgency.class);

	@Override
	public void run() {
		if (Calc.chance(10))
			GameEngine.activeWorld.agency.createNewContractor("human_futa_agency_worker");
		else
			GameEngine.activeWorld.agency.createNewContractor("human_female_agency_worker");
	}

}
