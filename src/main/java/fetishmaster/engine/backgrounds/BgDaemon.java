/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.engine.backgrounds;

import fetishmaster.engine.GameEngine;
import fetishmaster.utils.Profiler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author H.Coder
 */
public class BgDaemon implements Runnable {
	private static final Logger logger = LoggerFactory.getLogger(BgDaemon.class);

	final Object sync;

	public BgDaemon(Object sync) {
		this.sync = sync;
	}

	@Override
	public void run() {
		while (true) {
			runNext();
			timeout();
		}

	}

	private void timeout() {
		if (BackgroundsManager.getBgCount() == 0) {
			synchronized (sync) {
				try {
					logger.debug("Finished available work, sleeping");
					Profiler.stopProfiling("Background Daemon");
					sync.wait(0);
					logger.debug("Woke up!");
					Profiler.startProfiling("Background Daemon");
				} catch (InterruptedException ex) {
					logger.error("Interrupted", ex);
				}
			}

		}
	}

	private synchronized void runNext() {
		if (BackgroundsManager.getBgCount() == 0)
			return;

		synchronized (sync) {
			BgTask t = BackgroundsManager.getNextTask();
			Profiler.startProfiling(t.getClass().getName());
			t.execute();
			Profiler.stopProfiling(t.getClass().getName());
			sync.notify();
		}
	}

}
