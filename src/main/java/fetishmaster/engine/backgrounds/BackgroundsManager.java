/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.engine.backgrounds;

import fetishmaster.engine.GameEngine;
import fetishmaster.utils.Debug;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * @author H.Coder
 */
public class BackgroundsManager {
	final public static Object notifyer = new Object();
	final public static Object cpl_notifyer = new Object();
	//private static LinkedBlockingDeque <BgTask>bgTasks = new LinkedBlockingDeque<BgTask>();
	private static final java.util.concurrent.ConcurrentLinkedQueue<BgTask> bgTasks = new ConcurrentLinkedQueue<BgTask>();
	public static volatile boolean bgBusy = false;
	public static volatile boolean cplBusy = false;
	private static Thread daemon;

	private static Logger logger = LoggerFactory.getLogger(BackgroundsManager.class);

	public static void init() {
		daemon = new Thread(new BgDaemon(notifyer));
		daemon.setDaemon(true);
		daemon.start();

	}

	public static void waitForNoBackgrounds() // cause deadlock
	{
		GameEngine.activeMasterWindow.setTitle("Fetish Master (please, wait...)");
		while (!bgTasks.isEmpty()) {
			try {
				Thread.sleep(1000);
				logger.trace("Waiting for no background...");
				//BackgroundsManager.getDaemonCurBg().wait(1000);
			} catch (InterruptedException ex) {
				logger.debug("Wait Interrupted", ex);
			}

		}

		GameEngine.activeMasterWindow.setTitle("Fetish Master");
	}

	public static void addBackgroundTask(BgTask t) {
		synchronized (notifyer) {
			bgTasks.add(t);

			//Debug.print("bg tasks left: " + bgTasks.size());


			notifyer.notify();
		}

	}

	public static BgTask getNextTask() {

		BgTask t = null;
		if (!bgTasks.isEmpty())
			t = bgTasks.poll();
		return t;
	}

	public static void debugState() {
		logger.debug("Background tasks left: {}", bgTasks.size());
	}

	static synchronized int getBgCount() {
		return bgTasks.size();
	}


}
