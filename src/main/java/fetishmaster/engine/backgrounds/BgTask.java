/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.engine.backgrounds;

/**
 * @author H.Coder
 */
public abstract class BgTask {
	protected Object fastReturn;

	public BgTask() {
		fastReturn = null;
	}

	/**
	 * @return the fastReturn
	 */
	public Object getFastReturn() {
		return fastReturn;
	}

	abstract public void execute();

}
