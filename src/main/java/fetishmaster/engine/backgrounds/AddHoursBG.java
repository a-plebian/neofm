/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.engine.backgrounds;

import fetishmaster.bio.Creature;

/**
 * @author H.Coder
 */
public class AddHoursBG extends BgTask {
	Creature c;
	int hours;

	public AddHoursBG(Creature c, int hours) {
		this.c = c;
		this.hours = hours;
	}

	@Override
	public void execute() {
		this.c.addHours(hours);
	}

}
