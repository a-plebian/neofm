/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.engine.backgrounds;

import fetishmaster.engine.GameEngine;

/**
 * @author H.Coder
 */
public class AddWorldHoursBG extends BgTask {

	int hours;

	public AddWorldHoursBG(int hours) {
		this.hours = hours;
	}

	public void execute() {
		GameEngine.activeWorld.addHours(hours);
	}

}
