/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.engine.backgrounds;

import fetishmaster.display.gamewindow.JDialogAlert;
import fetishmaster.engine.GameEngine;

/**
 * @author H.Coder
 */
public class AlertWindow extends BgTask {

	String text = "Not initialized";

	public AlertWindow(String text) {
		this.text = text;
	}

	@Override
	public void execute() {
		JDialogAlert jda = new JDialogAlert(text);
		jda.AlignCenter(GameEngine.activeMasterWindow);
		jda.setVisible(true);
	}
}
