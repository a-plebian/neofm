/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.engine;

import com.thoughtworks.xstream.annotations.XStreamOmitField;
import fetishmaster.utils.ICopiable;
import fetishmaster.utils.UtilityFunctions;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author H.Coder
 */
public class WalkEvent implements ICopiable<WalkEvent> {
	public final static int COND_NOT_CHECKED = 0;
	public final static int COND_GOOD = 2;
	public final static int COND_BAD = 1;
	private List<WalkChoice> choices = new ArrayList<>();
	public String parsedDesc = "";
	@XStreamOmitField
	public int pasedConditions = 0;
	private String name = "";
	private String descr = "";
	private String conditions = "1";
	private String picturePath = "";
	private String defaultNext = "";
	private int time = 0;
	private int priority = 0;
	private boolean returnPoint = false;
	private boolean bypassInternalChecks = false;
	private Object scriptObject = new Object();
	private boolean mergeDown = false;

	public WalkEvent(String name, String descr) {
		this.name = name;
		this.descr = descr;
		this.defaultNext = name;
	}

	//workaround for xstream/openjdk compatibility, ignore
	private WalkEvent() {
		this.name = "";
		this.descr = "";
		this.defaultNext = "";
	}

	public WalkEvent copy() {
		var out = new WalkEvent();

		out.parsedDesc = parsedDesc;
		out.pasedConditions = pasedConditions;
		out.name = name;
		out.descr = descr;
		out.conditions = conditions;
		out.picturePath = picturePath;
		out.defaultNext = defaultNext;
		out.time = time;
		out.priority = priority;
		out.returnPoint = returnPoint;
		out.bypassInternalChecks = bypassInternalChecks;
		out.scriptObject = scriptObject;
		out.mergeDown = mergeDown;

		out.choices = UtilityFunctions.copyCopiableCollection(choices);

		return out;
	}

	public void addChoice(WalkChoice choise) {
		choices.add(choise);
	}

	public void setDefault(String defaultNext) {
		this.defaultNext = defaultNext;
	}

	public WalkChoice getChoice(int number) {
		return choices.get(number);
	}

	public void removeChoice(WalkChoice choice) {
		choices.remove(choice);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescr() {
		return descr;
	}

	/**
	 * @param descr the descr to set
	 */
	public void setDescr(String descr) {
		this.descr = descr;
	}

	public String getDefaultNext() {
		return this.defaultNext;
	}

	public int getChoicesCount() {
		return choices.size();
	}

	/**
	 * @return the picturePath
	 */
	public String getPicturePath() {
		return picturePath;
	}

	/**
	 * @param picturePath the picturePath to set
	 */
	public void setPicturePath(String picturePath) {
		this.picturePath = picturePath;
	}

	/**
	 * @return the time
	 */
	public int getTime() {
		return time;
	}

	/**
	 * @param time the time to set
	 */
	public void setTime(int time) {
		this.time = time;
	}

	/**
	 * @return the conditions
	 */
	public String getConditions() {
		return conditions;
	}

	/**
	 * @param conditions the conditions to set
	 */
	public void setConditions(String conditions) {
		this.conditions = conditions;
	}

	/**
	 * @return the priority
	 */
	public int getPriority() {
		return priority;
	}

	/**
	 * @param priority the priority to set
	 */
	public void setPriority(int priority) {
		this.priority = priority;
	}

	public void moveChoiceUp(int pos) {
		if (pos > choices.size() || pos < 1)
			return;

		WalkChoice wc = choices.get(pos);

		choices.remove(pos);

		choices.add(pos - 1, wc);

	}

	public void moveChoiceDown(int pos) {
		if (pos >= choices.size() - 1 || pos < 0)
			return;

		WalkChoice wc = choices.get(pos);

		choices.remove(pos);

		choices.add(pos + 1, wc);

	}

	/**
	 * @return the returnPoint
	 */
	public boolean isReturnPoint() {
		return returnPoint;
	}

	/**
	 * @param returnPoint the returnPoint to set
	 */
	public void setReturnPoint(boolean returnPoint) {
		this.returnPoint = returnPoint;
	}

	/**
	 * @return the bypassInternalChecks
	 */
	public boolean isBypassInternalChecks() {
		return bypassInternalChecks;
	}

	/**
	 * @param bypassInternalChecks the bypassInternalChecks to set
	 */
	public void setBypassInternalChecks(boolean bypassInternalChecks) {
		this.bypassInternalChecks = bypassInternalChecks;
	}

	public boolean hasChoice(String name) {
		int i;
		WalkChoice wc;
		for (i = 0; i < getChoicesCount(); i++) {
			wc = getChoice(i);
			if (name.equals(wc.getName()))
				return true;
		}

		return false;
	}

	/**
	 * @return the choices
	 */
	public List<WalkChoice> getChoices() {
		return choices;
	}

	/**
	 * @return the scriptObject
	 */
	public Object getScriptObject() {
		return scriptObject;
	}

	/**
	 * @param scriptObject the scriptObject to set
	 */
	public void setScriptObject(Object scriptObject) {
		this.scriptObject = scriptObject;
	}

	/**
	 * @return the mergeDown
	 */
	public boolean isMergeDown() {
		return mergeDown;
	}

	/**
	 * @param mergeDown the mergeDown to set
	 */
	public void setMergeDown(boolean mergeDown) {
		this.mergeDown = mergeDown;
	}

	public void mergeEvent(WalkEvent we) {
		choices.addAll(we.getChoices());
		if (!we.getPicturePath().equals(""))
			this.picturePath = we.getPicturePath();
		this.descr += we.getDescr();
		this.time += we.getTime();
		if (we.isReturnPoint())
			this.returnPoint = true;
		if (we.scriptObject != null)
			this.scriptObject = we.getScriptObject();
	}
}
