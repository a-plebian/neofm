/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.engine;

import com.formdev.flatlaf.FlatDarkLaf;
import com.formdev.flatlaf.FlatLightLaf;
import fetishmaster.World;
import fetishmaster.bio.*;
import fetishmaster.contracts.WorkerContract;
import fetishmaster.display.JFrameMasterWindow;
import fetishmaster.display.gamewindow.JDialogAlert;
import fetishmaster.display.gamewindow.JDialogInventory;
import fetishmaster.display.gamewindow.ScriptWindow;
import fetishmaster.engine.backgrounds.BackgroundsManager;
import fetishmaster.engine.scripts.ScriptCache;
import fetishmaster.engine.scripts.ScriptEngine;
import fetishmaster.items.Item;
import fetishmaster.utils.Debug;
import fetishmaster.utils.FileCache;
import fetishmaster.utils.UIHelpers;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.mvel2.optimizers.OptimizerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;
import java.util.zip.Adler32;

/**
 * @author H.Coder
 */

//This is full static class.
public class GameEngine {

	public static final String compileDate = "69";
	public static final String compileVersion = "0.98";
	public static String version = compileVersion + "." + compileDate;
	public static HashMap sysVars = new HashMap();
	public static String gameDataPath = "gamedata";
	public static World activeWorld = null;
	public static JFrameMasterWindow activeMasterWindow;
	public static JDialogInventory inventoryWindow = null;
	public static boolean devMode = false;
	public static boolean gameStarted = false;
	public static boolean fullDevMode = false;
	public static Logger logger = LoggerFactory.getLogger(GameEngine.class);

	//public volatile static Thread partialBgTask = new Thread();
	//public volatile static List bgTasks = Collections.synchronizedList(new ArrayList());

	public static List<ManagmentTask> tasksSet = new ArrayList<>();

	public static boolean nowWalking = false;


	public static void initNewGame() {
		long t1 = 0, t2;
		if (GameEngine.devMode)
			t1 = System.currentTimeMillis();

		activeWorld = new World();

		initGameEngine();

		//toWalking("system_events/start_game");

		activeWorld.clock.addHours(7 + 24);

		// debug
		//debugZoneStartGame();

		Creature c;
		Item it;
		WorkerContract w;

		//waitForBackground();
		//BackgroundsManager.waitForNoBackgrounds();
		//activeWorld.agency.acceptContract(0);
		WalkFrame wf = new WalkFrame();
		ScriptEngine.loadVars(wf.getVarsContext(), GameEngine.activeWorld.playerAvatar, null);
		WalkEngine.processInclude("system/on_game_start", wf);
		GameEngine.activeMasterWindow.jPanelInventoryBase1.initStorage();

		toWalking("system_events/start_game");

		t2 = System.currentTimeMillis() - t1;
		logger.debug("Time spent starting: {}ms", t2);

		gameStarted = true;

	}

	public static void debugCachesState() {
		logger.debug("++++++++++++++++++++++++++++");
		ScriptCache.debugState();
		logger.debug("-----------------------------");
		BackgroundsManager.debugState();
		logger.debug("++++++++++++++++++++++++++++");
	}

	public static void debugStartApp() {

		logger.debug("Relative game resource path: {}", GameEngine.gameDataPath);
		logger.debug("Absolute game resource path: {}", getAbsoluteGameDataPath());

//        Item it1, it2;
//        it1 = ItemProcessor.loadItem("items/coin.item");
//        it2 = it1.clone();     

//        ArrayList a = new ArrayList();
//        int i;
//        for (i = 0; i<100; i++)
//            a.add("");
//        fileXML.SaveXML(a, gameDataPath+"/texts/fnames.xml");
//        fileXML.SaveXML("Splash text", gameDataPath+"/texts/splash.xml");

	}

	public static void debugZoneStartGame() {

		Creature c;
		Item it;
		WorkerContract w;
//        c = loadNewCharacter("melly.char");
//        CreatureProcessor.Birth(c);
//        activeWorld.creatures.add(c);
//        
//        WorkerContract w = new ContractFreeChar();
//        //w.setWorker(c);
//        //acceptContract(w);
//        
//        Item it;
//        it = ItemProcessor.loadItem("items/coin.item");
//        c.inventory.addItem(it);
//        it = ItemProcessor.loadItem("items/coin.item");
//        c.inventory.addItem(it);
//        it = ItemProcessor.loadItem("items/coin.item");
//        c.inventory.addItem(it);
//        it = ItemProcessor.loadItem("items/lust_pill.item");
//        c.inventory.addItem(it);
//        it = ItemProcessor.loadItem("items/hormone_shot.item");
//        c.inventory.addItem(it);
//          it = ItemProcessor.loadItem("items/lactaids.item");
//          c.inventory.addItem(it);
//                       
		long t1 = 0, t2;
		if (GameEngine.devMode)
			t1 = System.currentTimeMillis();

//        c = CreatureProcessor.CreateFromDNA(TextProcessor.getRandomName(2), (DNAGenePool)fileXML.LoadXML(gameDataPath+"/dna/human_female.dna"));
//        //GameEngine.activeWorld.addHours(1);
//        CreatureProcessor.Birth(c);
//        CreatureProcessor.agePassDays(c, 365*22, true);
//        
//        w = new ContractFreeChar();
//        w.setWorker(c);
//        acceptContract(w);

		//c = CreatureProcessor.CreateFromDNA(TextProcessor.getRandomName(2), (DNAGenePool)fileXML.LoadXML(gameDataPath+"/dna/human_female.dna"));
		//GameEngine.activeWorld.addHours(1);
		//CreatureProcessor.Birth(c);
		//CreatureProcessor.agePassDays(c, 365*24, true);

		//w = new ContractFreeChar();
		//w.setWorker(c);
		//acceptContract(w);

		//ItemProcessor.injectItemsFromFile(c.inventory, "old_battery", 13);

//        it = ItemProcessor.loadItem("lactaids");
//        c.inventory.addItem(it);
//        
//        it = ItemProcessor.loadItem("items/hormone_shot.item");
//        c.inventory.addItem(it);
//        it = ItemProcessor.loadItem("items/old_battery.item");
//        c.inventory.addItem(it);
//        it = ItemProcessor.loadItem("items/old_battery.item");
//        c.inventory.addItem(it);
//        it = ItemProcessor.loadItem("items/old_battery.item");
//        c.inventory.addItem(it);
//        it = ItemProcessor.loadItem("items/old_battery.item");
//        c.inventory.addItem(it);
//        it = ItemProcessor.loadItem("items/old_battery.item");
//        c.inventory.addItem(it);
//        it = ItemProcessor.loadItem("items/old_battery.item");
//        c.inventory.addItem(it);
//        

		activeWorld.agency.acceptContract(0);

		//activeWorld.playerAvatar.inventory.addMoney(10000);
	}

	public static boolean checkResources() {
		WalkEvent we = FileCache.getGamedata("events/system_events/start_game.walk");

		return we != null;
	}

	public static void initGameEngine() {
		sysVars = FileCache.getSimpleGamedata("texts/system.xml");

		BackgroundsManager.init();
		loadAllTasks();
		TextProcessor.init();
		CreatureProcessor.init();

		GameEngine.activeMasterWindow.initVisuals();
		GameEngine.activeMasterWindow.refreshWindow();
		GameEngine.activeMasterWindow.jTabbedPane_main.setSelectedIndex(0);
		GameEngine.activeMasterWindow.jTabbedPane_main.setEnabledAt(2, false);
		GameEngine.activeMasterWindow.jPanelInventoryBase1.initStorage();
		activeWorld.agency.nextHour();
		BackgroundsManager.waitForNoBackgrounds();

	}

	public static void saveGame() {
		System.gc();
		FileCache.writeXMLObject(UIHelpers.openSaveFileChooser("saves", "world", "FetishMaster worlds"), activeWorld);
	}

	public static void loadGame() {
		World newworld;
		newworld = FileCache.getXMLObject(UIHelpers.openLoadFileChooser("saves", "world", "FetishMaster worlds"));
		if (newworld != null) {
			if (newworld.objects == null)
				newworld.objects = Collections.synchronizedMap(new HashMap());
			GameEngine.activeWorld = newworld;
			if (activeWorld.activePartner != null)
				WalkEngine.loadEvent(GameEngine.activeWorld.lastEvent, null);
			gameStarted = true;
			initGameEngine();
			GameEngine.activeMasterWindow.jTabbedPane_main.setEnabledAt(2, true);
			GameEngine.activeMasterWindow.jTabbedPane_main.setEnabledAt(1, false);
			CreatureProcessor.updateScriptedOrgans(GameEngine.activeWorld.getCreatures());
			CreatureProcessor.updateScriptedOrgans(GameEngine.activeWorld.agency.getCreatures());
			CreatureProcessor.updateScriptedEffects(GameEngine.activeWorld.getCreatures());
			CreatureProcessor.updateScriptedTasks(GameEngine.activeWorld.getCreatures());
			if (newworld.notes != null)
				activeMasterWindow.jEditorPane_notes.setText(newworld.notes);
			WalkFrame wf = new WalkFrame();
			ScriptEngine.loadVars(wf.getVarsContext(), GameEngine.activeWorld.playerAvatar, null);
			WalkEngine.processInclude("system/on_game_load", wf);
		}
		System.gc();

	}

	public static void loadAllTasks() {
		tasksSet.clear();

		tasksSet.addAll(FileCache.getGamedataDir("tasks/", "*.task"));
	}

	public static String getFullPath(String filename) {
		return gameDataPath + "/" + filename;
	}

	public static Creature loadNewCharacter(String template) {
		Creature c;
		c = FileCache.getGamedata("chars/" + template);
		GeneProcessor.calculateRNAOnlyStats(c);

		if (c.shedule == null)
			c.shedule = new CreatureShedule(); //don't auto initialize on load;

		return c;
	}

	public static int addHours(int hours) {
		return activeWorld.addHours(hours);
	}

	public static void addHoursBG(int hours) {
		GameEngine.activeWorld.addHoursBG(hours);
	}

	public static void toManagment() {
		GameEngine.activeMasterWindow.jButton_disconnectLink.setEnabled(true);
		activeMasterWindow.jTabbedPane_main.setEnabledAt(0, true);
		activeMasterWindow.jTabbedPane_main.setEnabledAt(2, true);
		activeMasterWindow.jTabbedPane_main.setEnabledAt(1, false);
		activeMasterWindow.jTabbedPane_main.setSelectedIndex(0);
		GameEngine.activeMasterWindow.jButton_disconnectLink.setEnabled(true);
		GameEngine.activeMasterWindow.jButton_inventory.setEnabled(true);
		//if (activeMasterWindow.jTabbedPane_main.getModel().getSelectedIndex() > 0 )
		//activeMasterWindow.refreshWindow(); //Really not needed now??? Wow. Then I really done it!.

		nowWalking = false;

		activeWorld.activePartner = null;
		//GameEngine.activeMasterWindow.jPanel_system.setVisible(true);
	}

	public static void toWalking(String startEvent) {
		if (startEvent == null)
			return;

		activeMasterWindow.jTabbedPane_main.setEnabledAt(1, true);
		activeMasterWindow.jTabbedPane_main.setEnabledAt(0, false);
		activeMasterWindow.jTabbedPane_main.setEnabledAt(2, false);
		activeMasterWindow.jTabbedPane_main.setSelectedIndex(1);
		activeMasterWindow.refreshWindow();
		nowWalking = true;

		WalkEngine.initFirstTurn(startEvent);

		GameEngine.activeMasterWindow.jButton_disconnectLink.setEnabled(true);

	}

	public static void voidContract(WorkerContract wc) {
		activeWorld.contracts.removeContract(wc.getWorker());
		wc.getWorker().removeFromWorkers();
		activeWorld.removeCreature(wc.getWorker());
		activeWorld.removeReturner(wc.getWorker());
		activeMasterWindow.initVisuals();
		activeWorld.activePartner = null;
		activeWorld.selectedCreature = null;

	}

	public static void acceptContract(WorkerContract wc) {
		Creature c = wc.getWorker();
		if (c == null)
			return;

		c.setStat(DNAGene.HEALTH, c.getRNAValue(DNAGene.MAXHEALTH));

		activeWorld.addCreature(c);
		activeWorld.contracts.addContract(wc, 0);
		c.addToWorkers();
		activeMasterWindow.initVisuals();
	}

	public static void alertWindow(String text) {
//        BackgroundsManager.addBackgroundTask(new AlertWindow(text));
		if (!GameEngine.nowWalking) {
//            BackgroundsManager.addBackgroundTask(new AlertWindow(text));
//            synchronized (BackgroundsManager.notifyer){
//                ScriptWindow sw = new JDialogAlert(text);
//                sw.display(activeMasterWindow);     
//                BackgroundsManager.notifyer.notify();
//            }
			activeMasterWindow.addAlert(text);
		} else {
			ScriptWindow sw = new JDialogAlert(text);
			WalkEngine.getCurrentFrame().setScriptWindow(sw);
		}

	}

	public static boolean InInteractionMode() {
		return GameEngine.activeWorld.inInteractionMode;
	}


	public static void CreateReleaseCRCList() {
		if (GameEngine.fullDevMode == false)
			return;
		String absgp = getAbsoluteGameDataPath();

		if (absgp == null)
			return;

		var cmap = calcFilesCRC(createFileList(absgp));

		try {
			FileCache.writeXMLObject(GameEngine.gameDataPath + "/texts/release_crc.xml", cmap, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void CreateCummulativePatch() {
		LinkedHashMap cmap = FileCache.getGamedata("texts/release_crc.xml");
		if (cmap == null)
			return;

		File f = new File(GameEngine.gameDataPath);
		String absgp = getAbsoluteGameDataPath();


		if (absgp == null)
			return;

		String key;

		List<String> fl = createFileList(absgp);
		var nmap = calcFilesCRC(fl);

		try {
			FileCache.writeXMLObject(GameEngine.gameDataPath + "/../patch/gamedata/texts/release_crc.xml", nmap, true);
		} catch (Exception e) {
			e.printStackTrace();
		}

		Iterator it = cmap.keySet().iterator();

		while (it.hasNext()) {
			key = (String) it.next();
			if (nmap.containsKey(key) && nmap.get(key).equals(cmap.get(key)))
				nmap.remove(key);

		}
		logger.info("Cumulative patch: {} files", nmap.size());

		it = nmap.keySet().iterator();
		String oldf, newf, fn;
		while (it.hasNext()) {
			fn = (String) it.next();
			oldf = gameDataPath + fn;
			newf = gameDataPath + "/../patch/gamedata" + fn;
			logger.debug("Source file: {}; to: {}", oldf, newf);
			try {
				File of = new File(oldf), nf = new File(newf);
				if (of.isFile())
					FileUtils.copyFile(of, nf);
			} catch (IOException ex) {
				logger.error("Failed to copy \"{}\" to \"{}\" for cumulative patch: {}", oldf, newf, ex);
			}

		}
	}

	public static void setDarkTheme() {
		try {
			UIManager.setLookAndFeel(new FlatDarkLaf());
			SwingUtilities.updateComponentTreeUI(activeMasterWindow);
		} catch (Exception e) {
			//Exception handle
		}
	}

	public static void setLightTheme() {
		try {
			UIManager.setLookAndFeel(new FlatLightLaf());
			SwingUtilities.updateComponentTreeUI(activeMasterWindow);
		} catch (Exception e) {
			//Exception handle
		}
	}

	public static String getAbsoluteGameDataPath() {
		return Paths.get(gameDataPath).toAbsolutePath().toString();
	}

	public static String crc(String filename) {
		var file = new File(getAbsoluteGameDataPath() + filename);

		byte[] bytes = null;

		if (!file.exists()) {
			return null;
		}

		if (file.isDirectory()) {
			return null;
		}

		try {
			bytes = FileUtils.readFileToByteArray(file);
		} catch (IOException ex) {
			logger.error("Failed to load \"{}\" as bytes: {}", filename, ex);
		}

		var ad = new Adler32();

		ad.update(bytes);

		return String.valueOf(ad.getValue());
	}

	public static List<String> createFileList(String absPatch) {
		var files = new ArrayList<String>();

		File f;
		String relp;

		Iterator<File> it = FileUtils.iterateFiles(new File(absPatch), null, true);

		while (it.hasNext()) {
			f = it.next();

			if (f.isFile()) {
				String fn = f.getAbsolutePath();

				relp = fn.substring(getAbsoluteGameDataPath().length());

				if (relp.contains("/.svn/")) {
					continue;
				}
				if (relp.contains("/.git/")) {
					continue;
				}
				if (relp.contains("\\.git\\")) {
					continue;
				}
				if (relp.contains("/.gitignore")) {
					continue;
				}
				if (relp.contains("\\.gitignore")) {
					continue;
				}
				if (relp.contains("\\.svn\\")) {
					continue;
				}
				if (relp.contains("/saves/")) {
					continue;
				}
				if (relp.contains("\\saves\\")) {
					continue;
				}
				if (relp.contains("/texts/")) {
					continue;
				}
				if (relp.contains("\\texts\\")) {
					continue;
				}

				relp = relp.replace('\\', '/');

				files.add(relp);
			}

		}

		return files;
	}

	public static Map<String, String> calcFilesCRC(List<String> files) {
		Map<String, String> crcList = new LinkedHashMap<>(3000);
		Iterator<String> it = files.iterator();
		String fs;
		String crc;

		while (it.hasNext()) {
			fs = it.next();
			crc = crc(fs);

			crcList.put(fs, crc);
		}

		return crcList;
	}
}
