/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.engine;

/**
 * @author H.Coder
 */
public class HistoryEvent {
	private String name = "";
	private String descr = "";
	private String image = "";
	private int hours = 0;
	private int days = 0;

	public HistoryEvent() {
		if (GameEngine.gameStarted) {
			this.hours = GameEngine.activeWorld.clock.getHours();
			this.days = GameEngine.activeWorld.clock.getDays();
		}
	}

	public HistoryEvent(String name) {
		this.name = name;
		if (GameEngine.gameStarted) {
			this.hours = GameEngine.activeWorld.clock.getHours();
			this.days = GameEngine.activeWorld.clock.getDays();
		}

	}

	public HistoryEvent(String name, String descr) {
		this.name = name;
		this.descr = descr;

		if (GameEngine.gameStarted) {
			this.hours = GameEngine.activeWorld.clock.getHours();
			this.days = GameEngine.activeWorld.clock.getDays();
		}

	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the descr
	 */
	public String getDescr() {
		return descr;
	}

	/**
	 * @param descr the descr to set
	 */
	public void setDescr(String descr) {
		this.descr = descr;
	}

	@Override
	public String toString() {
		String res = "Day " + days + ", " + hours + ":00 - " + name;
		return res;
	}

	/**
	 * @return the hours
	 */
	public int getHours() {
		return hours;
	}

	/**
	 * @param hours the hours to set
	 */
	public void setHours(int hours) {
		this.hours = hours;
	}

	/**
	 * @return the days
	 */
	public int getDays() {
		return days;
	}

	/**
	 * @param days the days to set
	 */
	public void setDays(int days) {
		this.days = days;
	}

	/**
	 * @return the image
	 */
	public String getImage() {
		return image;
	}

	/**
	 * @param image the image to set
	 */
	public void setImage(String image) {
		this.image = image;
	}
}
