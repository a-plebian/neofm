/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.engine;

import fetishmaster.bio.Creature;
import fetishmaster.bio.DNAGene;
import fetishmaster.bio.RNAGene;
import fetishmaster.engine.scripts.ScriptEngine;
import fetishmaster.utils.Calc;

import java.util.ArrayList;
import java.util.List;

/**
 * @author H.Coder
 */
public class ManagementEngine {

	public static boolean isConditionsRight(Creature c, ManagmentTask t) {
//        GameEngine.activeWorld.scriptSelfCreature = c;

		return ScriptEngine.processSheduleConditionScript(c, t.getConditions());

	}

	public static List<ManagmentTask> testConditionsAgainst(Creature c) {
		List<ManagmentTask> al = new ArrayList<>();
		List<ManagmentTask> tasks = GameEngine.tasksSet;
		int i;

		for (i = 0; i < tasks.size(); i++) {
			if (isConditionsRight(c, tasks.get(i)))
				al.add(tasks.get(i));
		}

		return al;
	}

	public static ManagmentTask getDefaultTask() {
		List<ManagmentTask> tasks = GameEngine.tasksSet;
		ManagmentTask mt;
		int i;

		for (i = 0; i < tasks.size(); i++) {
			mt = tasks.get(i);
			if (mt.getName().equals("Rest"))
				return mt;
		}

		return null;
	}

	public static void moveWorkerToReturnList(Creature worker, int hoursToReturn) {
		if (!worker.isWorker())
			return;

		GameEngine.activeWorld.removeWorker(worker);
		GameEngine.activeWorld.addReturner(worker);
		worker.addEffect(DNAGene.HEALTH, "time_to_return", 0, hoursToReturn); //we using effect mechanic to check time;
	}

	public static void sendToHospital(Creature worker, int hoursToReturn) {
		if (!worker.isWorker())
			return;

		worker.addHistory("Health low!", worker.getName() + " health is at critical level, so " + worker.get2ndPerson() + " sent to a hospital");
		GameEngine.alertWindow("Health low! " + worker.getName() + " health is at critical level, so " + worker.get2ndPerson() + " sent to a hospital");
		moveWorkerToReturnList(worker, hoursToReturn);
		worker.addStat(DNAGene.HEALTH, Calc.procent(worker.getRNAValue(DNAGene.MAXHEALTH), 10));
		worker.loadEffect("generic.health", "hospital_regen");
	}

	public static ManagmentTask getTask(String taskname) {
		return GameEngine.tasksSet.stream()
				.filter(task -> task.getName().equals(taskname))
				.findFirst()
				.orElse(null);
	}
}
