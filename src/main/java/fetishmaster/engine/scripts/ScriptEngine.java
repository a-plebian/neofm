/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.engine.scripts;

import fetishmaster.bio.Creature;
import fetishmaster.bio.CreatureProcessor;
import fetishmaster.bio.GeneProcessor;
import fetishmaster.bio.organs.OrganResponse;
import fetishmaster.engine.*;
import fetishmaster.engine.backgrounds.BackgroundsManager;
import fetishmaster.interaction.InteractionCalc;
import fetishmaster.items.ItemProcessor;
import fetishmaster.utils.*;
import org.mvel2.MVEL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author H.Coder
 */
public class ScriptEngine {
	private static final Logger logger = LoggerFactory.getLogger(ScriptEngine.class);

	private static final Map incScripts = new LinkedHashMap();
	public static Creature consumer;
	public static LinkedMapList<String, String> simpleTags = new LinkedMapList<>();

	public static void init() {
		LinkedMapList<String, String> l = FileCache.getSimpleGamedata("texts/simple_tags.xml");
		if (l != null) {
			simpleTags = l;
		}
	}

	private static Object processScript(String script, VarContext vars) {
		Object res;

		//synchronized(BackgroundsManager.notifyer){

		// ParserContext ctx = new ParserContext();
		// ctx.addImport(Creature.class);

		if (ScriptCache.hasScript(script)) {
			res = runCompiledScript(script, vars);
		} else {
//            compileScript(script);
//            res = interpretScript(script, vars);
			compileScriptSingleThread(script);
			res = runCompiledScript(script, vars);
		}

//        BackgroundsManager.notifyer.notify();
//        }

		return res;
	}

	private static Object runCompiledScript(String script, VarContext vars) {
		Object res = "";
		Serializable cScript;
		cScript = ScriptCache.getScript(script);

//        if (cScript == null)
//        {
//            res = interpretScript(script, vars);
//            return res;
//        }
		vars.callCount++;

		if (GameEngine.fullDevMode) {
			res = MVEL.executeExpression(cScript, ScriptVarsIntegrator.class, vars);

//            try {
//            res = MVEL.executeExpression(cScript, ScriptVarsIntegrator.class, vars);
//            }
//            catch (Exception e)
//            {
//                System.out.println("===============================================");
//                System.out.println(e.fillInStackTrace());
//                res = "";
//            }


		} else {
			try {
				res = MVEL.executeExpression(cScript, ScriptVarsIntegrator.class, vars);
			} catch (ConcurrentModificationException ex) {

				StringBuffer et = new StringBuffer();
				et.append("===============================================\n");
				et.append("Concurrent modification exception, in most cases this is game engine bug\n");
				et.append("Please, if you report this be ready to give author full error log.\n\n");
				et.append("Look in script: \n").append(script);
				et.append("\n-----------------------------------------------\n");
				et.append("Current variables context: ").append(vars.uuid).append("  Used: ").append(vars.callCount).append(" time(s).\n");
				et.append("-----------------------------------------------\n");
				et.append(vars);
				et.append("\n===============================================\n\n");
				et.append(ex);
				logger.error(et.toString());
				res = "";
				BackgroundsManager.notifyer.notify();
			} catch (Exception e) {
				StringBuffer et = new StringBuffer();
				et.append("===============================================\n");
				et.append("Compiled script error! Look in script: \n").append(script);
				et.append("\n-----------------------------------------------\n");
				et.append(e.getLocalizedMessage());
				et.append("\n-----------------------------------------------\n");
				et.append("Current variables context: ").append(vars.uuid).append("  Used: ").append(vars.callCount).append(" time(s).\n");
				et.append("-----------------------------------------------\n");
				et.append(vars);
				et.append("\n===============================================\n\n");
				logger.error(et.toString());
				res = "";
				BackgroundsManager.notifyer.notify();
			}
		}

		return res;
	}

	public static ScheduleTaskResult processCreatureScript(Creature c, String script, Object s_this) {

		if (c == null) {
			return null;
		}
		ScheduleTaskResult str = new ScheduleTaskResult();
		String res;

		VarContext vars = new VarContext();
		loadSDK(vars, s_this);
		vars.put("task_image", str.img);
		vars.put("self", c);
		WalkFrame wf;
		wf = new WalkFrame();
		wf.setVarsContext(vars);

//        synchronized (BackgroundsManager.notifyer)                
//        {
		WalkEngine.setSpecialIncludeFrame(wf);
		str.text = String.valueOf(processScript(script, wf.getVarsContext()));
		WalkEngine.clearSpecialIncludeFrame();
//            BackgroundsManager.notifyer.notify();
//        }

		if (s_this.getClass() == ManagmentTask.class) {
			//Debug.print(vars);
			str.img = (String) vars.get("task_image");
		}
		return str;
	}

	public static String processEventScript(String script, VarContext vars) {
		String res;

		res = String.valueOf(processScript(script, vars));

		return res;
	}

	public static boolean processConditionsScript(String script, VarContext vars) {

		String res = processEventScript(script, vars);

		if (res != null) {
			if (res.equals("false")) {
				return false;
			}

			if (res.equals("0")) {
				return false;
			}

			if (res.equals("null")) {
				return false;
			}

			return !res.equals("");
		}
		return true;
	}

	public static boolean processSheduleConditionScript(Creature c, String script) {
		String res = null;
		VarContext vars = new VarContext();

		vars.put("self", c);
		loadSDK(vars, null);

		try {
			//res = (String) MVEL.evalToString(script, ScriptVarsIntegrator.class, vars);
			res = String.valueOf(processScript(script, vars));
		} catch (Exception e) {
			logger.error("Script error in schedule script: \"{}\"", script, e);
			BackgroundsManager.notifyer.notify();
		}

		if (res != null) {
			if (res.equals("false")) {
				return false;
			}

			if (res.equals("0")) {
				return false;
			}

			if (res.equals("null")) {
				return false;
			}

			return !res.equals("");
		}

		return true;
	}

	public static OrganResponse processOrganDoActionScript(Creature c, String script, Object s_this, Map args) {

		if (c == null) {
			return null;
		}

		OrganResponse res;

		VarContext vars = new VarContext();
		vars.put("result", new OrganResponse());
		vars.put("self", c);
		vars.put("args", args);
		loadSDK(vars, s_this);

		if (GameEngine.fullDevMode) {
			res = (OrganResponse) processScript(script, vars);
		} else {
			try {
				//res = (OrganResponse) MVEL.eval(script, ScriptVarsIntegrator.class, vars);
				res = (OrganResponse) processScript(script, vars);
			} catch (Exception e) {
				StringBuffer et = new StringBuffer();
				et.append("===============================================\n");
				et.append("Script error! \n" + "Creature: ").append(c.getName()).append("\n" + "Look in Organ doAction script: \n").append(script);
				et.append("\n-----------------------------------------------\n");
				et.append(e);
				et.append("\n===============================================\n\n");

				logger.error(et.toString());

				res = new OrganResponse();
				BackgroundsManager.notifyer.notify();
			}
		}

		return res;
	}

	public static void loadVars(VarContext vars, Creature self, Object scriptObject) {
//        synchronized (BackgroundsManager.notifyer)
//        {
		vars.put("activePartner", GameEngine.activeWorld.activePartner);
		vars.put("proxy", GameEngine.activeWorld.activePartner);
		vars.put("enemy", GameEngine.activeWorld.currentEnemy);
		vars.put("player", GameEngine.activeWorld.playerAvatar);
		vars.put("selectedWorker", GameEngine.activeWorld.selectedCreature);
		//eventVars.put("self", GameEngine.activeWorld.scriptSelfCreature);
		vars.put("self", self);
		vars.put("iTarget", GameEngine.activeWorld.interactionCreature);
		vars.put("consumer", consumer);
		if (scriptObject != null) {
			vars.put("buttonObject", scriptObject);
		}
//        }
		loadSDK(vars, null);
	}

	public static void loadVars(WalkFrame frame) {

		VarContext vars = frame.getVarsContext();

		vars.put("activePartner", frame.getActor());
		vars.put("proxy", frame.getActor());
		vars.put("enemy", frame.getInteractionActor());
		vars.put("player", GameEngine.activeWorld.playerAvatar);
		vars.put("selectedWorker", GameEngine.activeWorld.selectedCreature);
		//eventVars.put("self", GameEngine.activeWorld.scriptSelfCreature);
		vars.put("self", frame.getActor());
		vars.put("iTarget", frame.getInteractionActor());
		vars.put("consumer", consumer);
		if (frame.getPressedObject() != null) {
			vars.put("buttonObject", frame.getPressedObject());
		}

		synchronized (BackgroundsManager.notifyer) {
			loadSDK(vars, null);
			BackgroundsManager.notifyer.notify();
		}

	}

	public static void loadSDK(VarContext vars, Object s_this) {
		vars.put("list", ArrayList.class);
		//vars.put("ArrayList", ArrayList.class);
		vars.put("map", HashMap.class);
		vars.put("InternalClock", InternalClock.class);
		//vars.put("HashMap", HashMap.class);
		vars.put("game", GameEngine.class);
		vars.put("creature", Creature.class);
		vars.put("Creature", Creature.class);
		vars.put("calc", Calc.class);
		vars.put("Calc", Calc.class);
		vars.put("geometry", GeometryCalc.class);
		vars.put("Geometry", GeometryCalc.class);
		vars.put("GeometryCalc", GeometryCalc.class);
		vars.put("item", ItemProcessor.class);
		vars.put("interCalc", InteractionCalc.class);
		vars.put("icalc", InteractionCalc.class);
		vars.put("status", Status.class);
		vars.put("clock", GameEngine.activeWorld.clock);
		vars.put("player", GameEngine.activeWorld.playerAvatar);
		vars.put("debug", Debug.class);
		vars.put("This", s_this);
		vars.put("its", s_this);
		vars.put("workers", GameEngine.activeWorld.getWorkers());
		vars.put("characters", GameEngine.activeWorld.getCreatures());
		vars.put("GeneProcessor", GeneProcessor.class);
		vars.put("CreatureProcessor", CreatureProcessor.class);
		vars.put("world", WorldScripts.class);
		vars.put("agency", Agency.class);
		vars.put("Agency", Agency.class);
	}

	//    public static void setConsumer(Creature c)
//    {
//        consumer = c;
//    }
	public static String processItemScript(Creature c, String useScript, Object s_this) {
		if (c == null) {
			return null;
		}


		String res;

		VarContext vars = new VarContext();
		loadSDK(vars, s_this);
		vars.put("self", c);
		vars.put("consumer", c);

		res = String.valueOf(processScript(useScript, vars));

		return res;
	}

	//    public static String processEventScript(String script, ScriptFrame sframe)
//    {
//        String res;
//        
//        res = String.valueOf(processScript(script, sframe.getVars()));
//
//        return res;
//    }
	public static String parseSimpleTags(String s) {
		for (Map.Entry<String, String> entry : simpleTags.entrySet()) {
			s = s.replace(entry.getKey(), entry.getValue());
		}
		return s;
	}

	public static String parseForScripts(String text, VarContext vars) {
		text = parseSimpleTags(text);

		String res = text;
		String tmp1, tmp2;

		Pattern p = Pattern.compile("/\\*.+?\\*/", Pattern.DOTALL);

		Matcher m = p.matcher(res);

		while (m.find()) {
			tmp1 = m.group();

			//tmp2 = tmp1.substring(2, tmp1.length()-2);

			tmp2 = "";

			res = res.replace(tmp1, tmp2);
		}

//        p = Pattern.compile("<%@.+?%>",Pattern.DOTALL);
//        
//        m = p.matcher(res);
//        
//        while (m.find())
//        {
//            tmp1 = m.group();
//                           
//            tmp2 = tmp1.substring(3, tmp1.length()-3);
//                                  
//            tmp2 = ScriptEngine.processEventScript(tmp2);
//            
//            tmp2 = TextProcessor.CapitalizeFirst(tmp2);
//           
//            res = m.replaceFirst(tmp2);
//            
//            m = p.matcher(res);
//        }

		p = Pattern.compile("<%@?.+?%>", Pattern.DOTALL);

		m = p.matcher(res);

		while (m.find()) {

			tmp1 = m.group();

			if (tmp1.charAt(2) == '@') {
				tmp2 = tmp1.substring(3, tmp1.length() - 3);
				tmp2 = ScriptEngine.processEventScript(tmp2, vars);
				tmp2 = TextProcessor.CapitalizeFirst(tmp2);
			} else {
				tmp2 = tmp1.substring(2, tmp1.length() - 2);
				tmp2 = ScriptEngine.processEventScript(tmp2, vars);

			}

			res = m.replaceFirst(tmp2);

			m = p.matcher(res);
		}

		return res;
	}

	public static boolean inCompQueue(Object script) {
		return incScripts.containsKey(script);
	}

	public static void scriptCompiled(Object script) {
		incScripts.remove(script);
	}

	public static void compileScriptSingleThread(String script) {
		Serializable compiled = null;

		Map impt = new HashMap();
		impt.put("ScriptVarsIntegrator", ScriptVarsIntegrator.class);
		impt.put("Creature", Creature.class);
		try {
			impt.put("Character", ScriptVarsIntegrator.class.getMethod("Character", String.class, String.class));
		} catch (NoSuchMethodException ex) {
			logger.error("No such method", ex);
		} catch (SecurityException ex) {
			logger.error("Security fail", ex);
		}

		try {
			if (impt == null) {
				compiled = MVEL.compileExpression(script);
			} else {
				compiled = MVEL.compileExpression(script, impt);
			}
		} catch (Exception e) {
			System.out.println("====================================================");
			System.out.println("Compilation not possible. \n"
					+ "This is not fatal error, backing up to interpretation mode.\n"
					+ "Script: \n" + script);
			System.out.println("----------------------------------------------------");
			System.out.println(e);
			System.out.println("====================================================\n\n");
		}

		ScriptCache.addScript(script, compiled);
		ScriptEngine.scriptCompiled(script);

	}

	static int cplQueueSize() {
		return incScripts.size();
	}

	public static Object runScript(String script, VarContext vars) {
		return processScript(script, vars);
	}
}
