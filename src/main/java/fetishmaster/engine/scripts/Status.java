/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.engine.scripts;

import fetishmaster.bio.Creature;
import fetishmaster.bio.Foetus;
import fetishmaster.bio.organs.Organ;
import fetishmaster.engine.GameEngine;
import fetishmaster.utils.Calc;
import fetishmaster.utils.Debug;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;
import java.util.List;

/**
 * @author H.Coder
 */
public class Status {
	private static final Logger logger = LoggerFactory.getLogger(Status.class);

	public static boolean isPregnant(Creature c) {
		if (c.hasOrgan("uterus")) {
			Organ o = c.getOrgan("uterus");
			List l = o.selectHooksByName("foetus");
			return !l.isEmpty();
		}
		return false;
	}

	public static boolean isLookedPregnant(Creature c) {
		return c.getStat("generic.abdomen") > 3;
	}

	public static boolean isVisiblePregnant(Creature c) {
		return c.getStat("generic.abdomen") > 3 && isPregnant(c);
	}

	public static boolean isLactating(Creature c) {
		return c.isRNAactive("breasts.lact_rate");
	}

	public static boolean isBreastsFull(Creature c) {
		return c.hasEffect("breasts.size", "engorged");
	}

	public static boolean isLaborReady(Creature c) {
		Organ o = c.getOrgan("uterus");
		List l = o.selectHooksByName("foetus");

		for (Iterator it = l.iterator(); it.hasNext(); ) {
			Foetus ft = (Foetus) it.next();
			if (ft.getState().getState("is_ready") == 1)
				return true;

		}

		return false;
	}

	public static boolean inWorkersList(Creature c) {
		return c.isWorker();
	}

	public static boolean inHome(Creature c) {
		if (!c.isWorker())
			return false;
		return !inReturnersList(c);
	}

	public static boolean inReturnersList(Creature c) {
		if (GameEngine.activeWorld == null) {
			return false;
		}

//        if (GameEngine.activeWorld.workers == null)
//        {
//            return false;
//        }

		Creature ct;

		int i;

		for (i = 0; i < GameEngine.activeWorld.getReturners().size(); i++) {
			ct = GameEngine.activeWorld.getReturners().get(i);
			if (ct == c) {
				return true;
			}
		}

		return false;
	}

	public static boolean hasContract(Creature c) {

		return GameEngine.activeWorld.contracts.getContract(c) != null;
	}

	public static boolean isMature(Creature c) {
		return c.getAge() > c.getStat("psy.child");
	}

	public static boolean isAdult(Creature c) {
		return c.getAge() > c.getStat("psy.child");
	}

	public static boolean isBaby(Creature c) {
		return c.getAge() < c.getStat("psy.baby");
	}

	public static boolean isChild(Creature c) {
		return c.getAge() < c.getStat("psy.child") && c.getAge() > c.getStat("psy.baby");
	}

	public static boolean isVirgin(Creature c) {
		return c.getStat("vagina.virgin") == 1 && c.isRNAactive("vagina.exists");
	}

	public static boolean VaginalIsPossible(Creature c, double width) {
		if (c == null) return false;
		if (c.hasOrgan("vagina") == false) return false;

		double cw = c.getStat("vagina.width") / 2;

		double r = width / 2;
		r = Math.PI * r * r;
		cw = Math.PI * cw * cw;
		cw = cw + Calc.percent(cw, 50);

		logger.debug("Vagina can take: {}; insertion: {}", cw, r);
		if (cw <= r) {
			logger.debug("Insertion not possible");
			return false;
		}

		return true;
	}

	public static boolean VaginalIsPossible(Creature receiver, Creature giver) {
		return VaginalIsPossible(receiver, giver.getStat("penis.width"));
	}

}
