/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.engine.scripts;

import fetishmaster.utils.Calc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * @author H.Coder
 */
public class InternalClock {
	private final List times = Collections.synchronizedList(new LinkedList());
	private final List locations = Collections.synchronizedList(new LinkedList());
	private final List percents = Collections.synchronizedList(new LinkedList());
	private String location = "";

	public synchronized void clearAll() {
		this.times.clear();
		this.locations.clear();
		this.percents.clear();
		location = "";
	}

	public synchronized void addLocation(int hour, String locName, double percent) {
		times.add(hour);
		locations.add(locName);
		percents.add(percent);
	}

	public synchronized void setHour(int hour) {
		int i, fin, idx;
		ArrayList index = new ArrayList();
		for (i = 0; i < times.size(); i++) {
			if ((times.get(i).equals(hour) || (Integer) times.get(i) < 0) && Calc.chance((Double) percents.get(i)))
				index.add(i);
		}

		if (!index.isEmpty()) {
			idx = Calc.randomInt(index.size() - 1);
			fin = (Integer) index.get(idx);
			location = (String) locations.get(fin);
		}
	}

	public synchronized String getLocation() {
		return this.location;
	}

	public synchronized void removeLocation(String locName) {
		int i;
		for (i = locations.size(); i >= 0; i--) {
			if (locations.get(i).equals(locName)) {
				locations.remove(i);
				times.remove(i);
				percents.remove(i);
				i--;
			}
		}
	}

	public synchronized void removeHour(int hour) {
		int i;
		for (i = times.size(); i >= 0; i--) {
			if (times.get(i).equals(hour)) {
				locations.remove(i);
				times.remove(i);
				percents.remove(i);
				i--;
			}
		}
	}

	public synchronized int size() {
		return locations.size();
	}

}
