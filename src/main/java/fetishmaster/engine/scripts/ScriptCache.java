/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.engine.scripts;

import fetishmaster.engine.GameEngine;
import fetishmaster.engine.backgrounds.BackgroundsManager;

import java.io.Serializable;
import java.util.*;

/**
 * @author H.Coder
 */
public class ScriptCache {
	private static final Map cScripts = Collections.synchronizedMap(new LinkedHashMap());
	//private static List cTimes = Collections.synchronizedList(new ArrayList());
	//private static LinkedHashMap cScripts = new LinkedHashMap();

	//private static LinkedMapList cScripts = new LinkedMapList();
	//private static List cTimes = new ArrayList();
	private static final int maxsize = 10000;

	public static void addScript(Object key, Object script) {

		if (hasScript(key)) {
			if (GameEngine.fullDevMode) {
				System.out.println("Compiled scripts: \n===\n" + key + "\n===\nalready exist");
			}
			//return;
		}
		synchronized (BackgroundsManager.cpl_notifyer) {
			cScripts.put(key, script);
			removeOld();
		}
		if (GameEngine.fullDevMode) {
			System.out.println("Compiled scripts: " + cScripts.size());
		}

	}

	public static boolean hasScript(Object script) {
		return cScripts.containsKey(script);
	}

	public static Serializable getScript(String script) {
		Object o;

		o = cScripts.get(script);

		synchronized (BackgroundsManager.cpl_notifyer) {
			upQueue(script);
		}

		return (Serializable) o;
	}

	private static void upQueue(Object o) {
		Object t = cScripts.get(o);
		cScripts.remove(o);
		cScripts.put(o, t);
	}

	public static void clearCache() {

		cScripts.clear();

	}

	private static void removeOld() {

		if (cScripts.size() < maxsize) {
			return;
		}

//        if (GameEngine.devMode)
//        {
//            System.out.println("Removing old compiled script...");
//        }
		synchronized (BackgroundsManager.cpl_notifyer) {
			Set keys = cScripts.keySet();
			Iterator it = keys.iterator();
			while ((cScripts.size() > maxsize) && it.hasNext()) {
				cScripts.remove(it.next());
			}
		}
	}

	public static void debugState() {
		System.out.println("Script cache status.\n"
				+ "Compiled scripts:    " + cScripts.size() + "\n" +
				"Compilation queue: " + ScriptEngine.cplQueueSize() + "\n");
		//  + "Timestamps:          "+cTimes.size());
	}

}
