/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.engine.scripts;

import fetishmaster.bio.Creature;
import fetishmaster.contracts.ContractAgencyBasic;
import fetishmaster.contracts.WorkerContract;
import fetishmaster.engine.GameEngine;

/**
 * @author H.Coder
 */
public class Agency {
	// adding new contract, must be with worker.
	public static void addContract(WorkerContract wc, int ttl) {
		if (wc == null)
			return;
		GameEngine.activeWorld.agency.contracts.addContract(wc, ttl);
	}

	//removing existing contract.
	public static void removeContract(WorkerContract wc) {
		if (wc == null)
			return;
		GameEngine.activeWorld.agency.contracts.removeContract(wc.getWorker());
	}

	//removing existing contract, selection by character.
	public static void removeContract(Creature c) {
		if (c == null)
			return;
		GameEngine.activeWorld.agency.contracts.removeContract(c);
	}

	//contracts count;
	public static int contractCount() {
		return GameEngine.activeWorld.agency.contracts.getContractCount();
	}

	public static WorkerContract newStandardContract() {
		WorkerContract wc = new ContractAgencyBasic();
		return wc;
	}

	public static WorkerContract newStandardContract(Creature c) {
		WorkerContract wc = new ContractAgencyBasic();
		wc.setWorker(c);
		return wc;
	}

	public static WorkerContract newStandardContract(Creature c, int price) {
		WorkerContract wc = new ContractAgencyBasic(price);
		wc.setWorker(c);
		return wc;
	}
}
