/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fetishmaster.engine;

import java.util.ArrayList;
import java.util.List;

/**
 * @author H.Coder
 */
public class EventSelector {
	List events = new ArrayList();

	public void addEvent(Object event) {
		events.add(event);

	}

	public void removeEvent(Object event) {
		events.remove(event);
	}

	public void removeEventAt(int pos) {
		events.remove(pos);
	}

	public Object getEventAt(int pos) {
		return events.get(pos);
	}

	public int getEventsCount() {
		return events.size();
	}

}
